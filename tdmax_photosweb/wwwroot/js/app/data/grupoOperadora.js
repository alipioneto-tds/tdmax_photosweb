﻿var module = angular.module('WebFotos.Data');

module.service('GrupoOperadoraApi', function (BaseApi, $q) {

    this.getGruposOperadoras = function () {

        var promise = BaseApi.getAction('GrupoOperadora')
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    this.getGruposOperadorasFiltro = function (busca) {

        var promise = BaseApi.getApiController('GrupoOperadora', 'GetGrupoOperadoraFiltro', {
            busca: busca
        })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }
        
    this.salvarGrupoOperadora = function (model) {
            var promise = BaseApi.postAction('GrupoOperadora', model)
                .then(function (response) {
                    return response.data;
                });
            return promise;
    }

    this.desativarGrupoOperadora = function (grupoOperadoraID) {
        var promise = BaseApi.postApiController('GrupoOperadora', 'DesativarGrupoOperadora', {
                grupoOperadoraID: grupoOperadoraID
            })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});