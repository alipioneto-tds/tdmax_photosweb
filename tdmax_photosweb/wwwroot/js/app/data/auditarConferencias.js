﻿var module = angular.module('WebFotos.Data');

module.service('AuditarConferenciasApi', function (BaseApi, $q) {
    this.getCadastros = function (buscaCad, buscaNumSerie) {
        var promise = BaseApi.getAction('AuditarConferencias', {
            buscaCad: buscaCad,
            buscaNumSerie: buscaNumSerie
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getAuditoriaCadastro = function (cadastroId) {
        var promise = BaseApi.getApiController('AuditarConferencias', 'GetAuditoriaCadastro', {
            cadastroId: cadastroId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getTiposReclassificacaoCadastro = function (cadastroId) {
        var promise = BaseApi.getApiController('AuditarConferencias', 'GetTiposReclassificacaoCadastro', {
            cadastroId: cadastroId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getTransacoesReclassificacao = function (cadastroId, statusReconhecimentoID, cameraTransIdOffset) {
        var promise = BaseApi.getApiController('AuditarConferencias', 'GetTransacoesReclassificacao', {
            cadastroId: cadastroId,
            statusReconhecimentoID: statusReconhecimentoID,
            cameraTransIdOffset: cameraTransIdOffset
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setDesbloquearCadastro = function (cadastroId) {
        var promise = BaseApi.postApiController('AuditarConferencias', 'SetDesbloquearCadastro', {
            cadastroId: cadastroId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setStatusReclassificacao = function (cameraTransacaoId, status, fotoId) {
        var promise = BaseApi.postAction('AuditarConferencias', {
            transacaoId: cameraTransacaoId,
            status: status,
            fotoId: fotoId
        }).then(function (response) {
            return cameraTransacaoId;
        });
        return promise;
    }
});

