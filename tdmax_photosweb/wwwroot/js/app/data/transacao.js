﻿var module = angular.module('WebFotos.Data');

module.service('TransacaoApi', function (BaseApi) {
    /* Consulta detalhes sobre uma transação específica. */
    this.getDetails = function (transacaoId) {
        var promise = BaseApi.getAction('Transacao', {
            transacaoId: transacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});