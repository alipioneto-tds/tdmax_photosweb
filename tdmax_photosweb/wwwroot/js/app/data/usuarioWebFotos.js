﻿var module = angular.module('WebFotos.Data');

module.service('UsuarioWebFotosApi', function (BaseApi) {

    this.getUsuario = function (usuarioId) {
        var promise = BaseApi.getAction('UsuarioWebFotos', {
            usuarioId: usuarioId
        })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    // Traz todos os usuários associados com perfis
    this.getUsuariosPerfis = function () {

        var promise = BaseApi.getApiController('UsuarioWebFotos', 'GetUsuariosPerfis')
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }


    // Traz todos os usuários associados com perfis
    this.getUsuariosPerfisFiltro = function (nome, perfilId) {

        var promise = BaseApi.getApiController('UsuarioWebFotos', 'GetUsuariosPerfisFiltro', { busca: nome, perfil: perfilId })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    this.GetUsuariosSemGrupo = function (usuarioID) {
        var promise = BaseApi.getApiController('UsuarioWebFotos', 'GetUsuariosSemGrupo', {
            usuarioID: usuarioID
        })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    //Traz todos os usuários que não tem Perfil de Admin de Empresa Operadora
    this.getUsuariosSemAdmin = function () {

        var promise = BaseApi.getApiController('UsuarioWebFotos', 'getUsuariosSemAdmin')
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    this.salvarUsuarioPerfil = function (usuarioID, perfilID, grupoOperadoraID) {
        var promise = BaseApi.postApiController('UsuarioWebFotos', 'InserirUsuarioWebFotos', {
            usuarioID: usuarioID,
            perfilID: perfilID,
            grupoOperadoraID: grupoOperadoraID
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.desativarUsuario = function (usuarioID) {
        var promise = BaseApi.postApiController('UsuarioWebFotos', 'DesativarUsuario', {
            usuarioID: usuarioID
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});