﻿var module = angular.module('WebFotos.Data');

module.service('SubTiposApi', function (BaseApi) {
    this.getSubTipos = function (cadastroTipoId, todos) {
        var promise = BaseApi.getAction('SubTipos', {
            cadastroTipoId: cadastroTipoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getSubTiposPenalizacao = function (cadastroTipoId, penalizacaoId) {
        var promise = BaseApi.getApiController('SubTipos', 'GetDisponiveisPenalizacao', {
            cadastroTipoId: cadastroTipoId,
            penalizacaoId: penalizacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});