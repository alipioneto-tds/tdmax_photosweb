﻿var module = angular.module('WebFotos.Data');

module.service('AdvertenciaPessoalApi', function (BaseApi, $q) {
    this.getAdvPessoais = function (pageId, busca, status) {
        var promise = BaseApi.getAction('AdvertenciaPessoal', {
            pageId: pageId,
            busca: busca,
            status: status
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getImpressaoAdvPessoal = function (cameraTransacaoId) {
        var promise = BaseApi.getApiController('AdvertenciaPessoal', 'GetImpressaoAdvPessoal', {
            cameraTransacaoId: cameraTransacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setAdvPessoal = function (cameraTransacaoId, ligacao, advertencia, desconsiderar) {
        var promise = BaseApi.postApiController('AdvertenciaPessoal', 'SetAdvertenciaPessoal', {
            cameraTransacaoId: cameraTransacaoId,
            ligacao: ligacao,
            advertencia: advertencia,
            desconsiderar: desconsiderar
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});