﻿var module = angular.module('WebFotos.Data');

module.service('ServerInfoApi', function (BaseApi) {
    /* consulta informações do servidor */
    this.getServerInfo = function () {
        var promise = BaseApi.getController('Server', '', '')
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});