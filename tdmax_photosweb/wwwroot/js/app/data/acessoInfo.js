﻿var module = angular.module('WebFotos.Data');

module.service('AcessoInfoApi', function (BaseApi) {
    /* Consulta informações específicas sobre a transação solicitada. */
    this.getAcessoInfo = function (transacaoId) {
        var promise = BaseApi.getAction('AcessoInfo', {
            transacaoId: transacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});