﻿var module = angular.module('WebFotos.Data');

module.service('PenalizacoesApi', function (BaseApi, $q) {
    this.getPenalizacoes = function (descricao, cadastroTipoId, subTipoId) {
        var promise = BaseApi.getAction('Penalizacoes', {
            descricao: descricao,
            cadastroTipoId: cadastroTipoId,
            subTipoId: subTipoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getPenalizacao = function (penalizacaoId) {
        var promise = BaseApi.getApiController('Penalizacoes', 'GetPenalizacao', {
            penalizacaoId: penalizacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setPenalizacoes = function (model) {
        var promise = BaseApi.postAction('Penalizacoes', model
        )
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setRemoverPenalizacao = function (penalizacaoId) {
        var promise = BaseApi.postApiController('Penalizacoes', 'SetRemoverPenalizacao', {
            penalizacaoId: penalizacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});