﻿var module = angular.module('WebFotos.Data');

module.service('ProblemaCameraApi', function (BaseApi) {
    /* Registra uma observação de problema na câmera, vinculada a uma determina comparação. */
    this.setProblemaCamera = function (transacaoId, observacao) {
        var promise = BaseApi.getAction('ProblemaCamera', {
            transacaoId: transacaoId,
            observacao: observacao
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setProblemaCameraEmbarcado = function (numserie, transacao, observacao) {
        var promise = BaseApi.getAction('ProblemaCamera', {
            numserie: numserie,
            transacao: transacao,
            observacao: observacao
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
})