﻿var module = angular.module('WebFotos.Data');

module.service('CamerasComparacoesApi', function (BaseApi, $q) {
    /* Consulta detalhes sobre uma conferência específica. */
    this.getDetails = function (tipo) {
        var promise = BaseApi.getAction('CamerasComparacoes', { tipo: tipo })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    /* Define o estado da verificação informada. */
    this.setStatus = function (numSerie, transacao, status, fotoId) {
        var deferred = $q.defer();

        BaseApi.postAction('CamerasComparacoes', {
            numSerie: numSerie,
            transacao: transacao,
            status: status,
            fotoId: fotoId
        }).then(
            // Sucesso
            function (response) {
                var sucesso = response.data;
                if (sucesso) {
                    deferred.resolve(numSerie + "," + transacao);
                } else {
                    deferred.reject();
                }
            },
            // Erro
            function () {
                deferred.reject();
            }
        );

        return deferred.promise;

    }

});