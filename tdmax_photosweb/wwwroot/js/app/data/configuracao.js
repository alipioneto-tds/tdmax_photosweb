﻿var module = angular.module('WebFotos.Data');

module.service('ConfiguracaoApi', function (BaseApi) {
    /* Consulta o valor de uma configuração. */
    this.getConfiguracao = async function ( args ) {
        var promise = await BaseApi.getApiController( "Settings", "Process", args.processo
            , "Item", args.item, "Index", args.indice );

        return promise;
    };
});