﻿var module = angular.module('WebFotos.Data');

module.service('AplicarPenalizacaoApi', function (BaseApi, $q) {
    this.getTransacoes = function () {
        var promise = BaseApi.getAction('AplicarPenalizacao')
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getTransacoesCadastro = function (cadastroId, cameraTransacaoId) {
        var promise = BaseApi.getApiController('AplicarPenalizacao', 'GetTransacoesCadastro', {
            cadastroId: cadastroId,
            cameraTransacaoId: cameraTransacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getCadastroInfo = function (cadastroId) {
        var promise = BaseApi.getApiController('AplicarPenalizacao', 'GetCadastroInfo', {
            cadastroId: cadastroId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.getPenalizacaoAplicada = function (cameraTransacaoId) {
        var promise = BaseApi.getApiController('AplicarPenalizacao', 'GetPenalizacaoAplicada', {
            cameraTransacaoId: cameraTransacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    this.setAplicarPenalizacao = function (cameraTransacaoId, status, fotoId) {
        var promise = BaseApi.postAction('AplicarPenalizacao', {
            transacaoId: cameraTransacaoId,
            status: status,
            fotoId: fotoId
        }).then(function (response) {
                return cameraTransacaoId;
            });
        return promise;
    }
});

