﻿var module = angular.module('WebFotos.Data');

module.service('EmpresaApi', function (BaseApi) {


    this.getTodasEmpresas = function () {

        var promise = BaseApi.getApiController('Empresa', 'GetEmpresas')
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    this.getEmpresasGrupoOperadora = function (grupoOperadoraID) {

        var promise = BaseApi.getApiController('Empresa', 'GetEmpresasGrupoOperadora', {
            grupoOperadoraID: grupoOperadoraID
        })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

});