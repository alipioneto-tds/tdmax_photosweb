﻿var module = angular.module('WebFotos.Data');

module.service('TransacoesApi', function (BaseApi) {
    /* Obtém as próximas transações do lote solicitado. */
    this.getTransacoes = async function ( userId ) {
        return await BaseApi.getApiController( 'Transactions', userId.toString() );
    };

    this.setStatus = async function ( numserie, transaction, status, photoId ) {
        return await BaseApi.postApiController( ['Transactions'], {
            numSerie: numserie,
            transaction: transaction,
            photoId: photoId,
            status: status
        } );
    };
});