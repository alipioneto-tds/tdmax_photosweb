﻿var module = angular.module('WebFotos.Data', []);

module.service('BaseApi', function ($http) {
    var baseApi = function ( base, controller, action, params ) {
        ///<summary>Retorna o endereço completo para uma webapi</summary>
        ///<param name="controller" type="String">Nome do controllerApi que contém a action</param>
        ///<param name="action" type="String">Nome da action a ser executada</param>
        ///<param name="params" type="String">lista de parâmetros que devem ser enviados em formato JSON</param>
        ///<returns type="String">Retorna a URL completa de uma action no formato: http://{url da aplicação}/api/{controllerApi}/{action}?{parametros}</returns>
        var sComplemento = '',
            addComplemento = function ( item, value ) {
                sComplemento += ( ( sComplemento == '' ) ?
                    ( '?' + item + '=' + value ) :
                    ( '&' + item + '=' + value ) );
            };

        if ( params != undefined && params != null ) {
            for ( var item in params ) {
                if ( $.isArray( params[item] ) ) {
                    for ( var index_arr in params[item] ) {
                        if ( params[item][index_arr] != null ) {
                            addComplemento( item, params[item][index_arr] );
                        }
                    }
                } else if ( params[item] != null ) {
                    addComplemento( item, params[item] );
                }
            }
        }

        var prefix = '';
        if ( location.href.indexOf( 'Account' ) != -1 ) {
            prefix = "../";
        }

        return prefix + base + controller + '/' + action + sComplemento;
    };

    var combine = function ( urls ) {

        var url = "http://localhost:55754/";

        for ( var i = 0; i < urls.length; i++ ) {

            var s = urls[i];

            if ( i > 0 && s.startsWith( "/" ) ) {
                s = s.substring( 1 );
            }
            if ( !s.endsWith( "/" ) ) {
                s = s + "/";
            }

            url += s;
        }

        if ( url.endsWith( "/" ) ) {
            url = url.substring( 0, url.length - 1 );
        }

        return url;
    };

    this.getAction = function ( controller, params ) {
        return $http.get( baseApi( 'api/', controller, '', params ) );
    };

    this.postAction = function ( controller, params ) {
        return $http.post( baseApi( 'api/', controller, '' ), params );
    };

    this.getController = function ( controller, action, params ) {
        return $http.get( baseApi( '', controller, action, params ) );
    };

    this.postController = function ( controller, action, params ) {
        return $http.post( baseApi( '', controller, action ), params );
    };

    this.getApiController = async function ( ...urls) {
        return await $http.get( combine( urls ) );
    };

    this.postApiController = async function ( urls, obj ) {
        return await $http.post( combine( urls ), obj );
    };
});