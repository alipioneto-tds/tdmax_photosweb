﻿var module = angular.module('WebFotos.Data');

module.service('LoteApi', function (BaseApi) {
    /* Obtém informações sobre o próximo lote a ser verificado. */
    this.getLote = function (dataIni, dataFim) {

        var args = {
            dataIni: ( dataIni != undefined ) ? dataIni : "",
            dataFim: ( dataFim != undefined ) ? dataFim : ""
        };

        var promise = BaseApi.getAction('Lote', args)
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }

    /* Fecha um determinado lote. */
    this.fecharLote = function (loteId) {
        var promise = BaseApi.getAction('FecharLote', {
            loteId: loteId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    /* Consulta se o cadastroId informado está conferindo um lote. */
    this.estaConferindo = function (usuarioId) {
        var promise = BaseApi.postController('Home', 'EstaConferindo', {
            usuarioId: usuarioId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});