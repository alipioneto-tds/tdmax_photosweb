﻿var module = angular.module('WebFotos.Data');

module.service('AdvertenciasApi', function (BaseApi) {
    /* Consulta as advertências que já foram recebidas pelo usuário de uma determinada transação. */
    this.getAdvertencias = function (transacaoId) {
        var promise = BaseApi.getAction('Advertencias', {
            transacaoId: transacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});