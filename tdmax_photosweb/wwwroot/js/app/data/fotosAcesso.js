﻿var module = angular.module('WebFotos.Data');

module.service('FotosAcessoApi', function (BaseApi, $q) {
    this.getFotosAcesso = function (cameraTransacaoId) {
        var promise = BaseApi.getAction('FotosAcesso', {
            cameraTransacaoId: cameraTransacaoId
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});

