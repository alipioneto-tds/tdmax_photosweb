﻿var module = angular.module('WebFotos.Data');

module.service('CadastrosTiposApi', function (BaseApi) {
    this.getCadastrosTipos = function (todos) {
        var promise = BaseApi.getAction('CadastrosTipos', {
        todos: todos
        })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});