﻿var module = angular.module('WebFotos.Data');

module.service('SgvApi', function ($http) {
    /* verifica se a versão está liberada pelo SGV */
    this.continuaExecucao = function (aes) {
        var promise = $http.post(Server.Action('UserSecurity', 'Continue'))
            .then(function (response) {
                return response.data;
            });
        return promise;
    }
});