﻿var module = angular.module('WebFotos.Data');

module.service('CamerasProdutoApi', function (BaseApi, $q) {
    /* Consulta detalhes sobre uma conferência específica. */
    this.getDetails = function (tipo) {
        var promise = BaseApi.getAction('CamerasProduto', { tipo: tipo })
            .then(function (response) {
                return response.data;
            });
        return promise;
    }

    /* Define o estado da verificação informada. */
    this.setStatus = function (equipamento, sequencia, transacao, status, fotoId) {
        var deferred = $q.defer();

        BaseApi.postAction('CamerasProduto', {
            equipamento: equipamento,
            sequencia: sequencia,
            transacao: transacao,
            status: status,
            fotoId: fotoId
        }).then(
            // Sucesso
            function (response) {
                var sucesso = response.data;
                if (sucesso) {
                    deferred.resolve(equipamento + "," + sequencia + "," + transacao);
                } else {
                    deferred.reject();
                }
            },
            // Erro
            function () {
                deferred.reject();
            }
        );

        return deferred.promise;

    }

});