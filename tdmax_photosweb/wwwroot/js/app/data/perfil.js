﻿var module = angular.module('WebFotos.Data');

module.service('PerfilApi', function (BaseApi) {
    this.getPerfis = function (todos) {
        var promise = BaseApi.getAction('Perfil', {
            todos: todos
        })
             .then(function (response) {
                 return response.data;
             });
        return promise;
    }
});