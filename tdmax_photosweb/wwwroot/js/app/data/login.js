﻿var module = angular.module('WebFotos.Data');

module.service('LoginApi', function (BaseApi) {
    /* define uma chave local */
    this.setKey = function () {
        if ( !Cryptography.ClientKey ) {
            Cryptography.GenerateKey();
        }

        var promise = BaseApi.getController( 'Authentication', 'SetKey', {
            key: Cryptography.ClientKey
        } )
            .then( function ( response ) {
                return response.data;
            } );
        return promise;
    };

    /* consulta se o usuário está autenticado */
    this.getAuthentication = function ( aes ) {
        var promise = BaseApi.postController( 'Authentication', 'GetAuthentication' )
            .then( function ( response ) {
                return response.data;
            } );
        return promise;
    };

    /* valida o nome de usuário e senha */
    this.setAuthentication = function ( aes ) {
        var promise = BaseApi.postController( 'Authentication', 'SetAuthentication', {
            criptoStr: aes
        } )
            .then( function ( response ) {
                return response.data;
            } );
        return promise;
    };

    /* realiza a troca de senha */
    this.changePassword = function ( oldPassword, newPassword, confirmation ) {
        var promise = BaseApi.postController( 'UserSecurity', 'ChangePassword', {
            senhaantiga: oldPassword,
            senha: newPassword,
            confirmacao: confirmation
        } )
            .then( function ( response ) {
                return response.data;
            } );
        return promise;
    };
});