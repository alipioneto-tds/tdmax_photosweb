﻿var module = angular.module( 'WebFotos.Conferencia' );

module.controller( 'ConferenciaCtrl', function (
    $scope, $rootScope, $location, qtdeFotosRequest
    , CommonLib, preloader, TransacoesApi
    , ConferenciaState, DashboardService, dashboardConfig
) {
    /* Declaração das variáveis locais. */
    $scope.transacoes = [];
    $scope.atual = {
        indice: -1,
        data: {}
    };

    $scope.contadores = {
        pendentes: 0
    };

    $scope.imagemAmpliada = {};
    $rootScope.loading = false;
    var lastQtyReturned = 0;
    var unbindWatch = null;

    /* Conta a quantidade de transações pendentes de conferência. */
    var getQtdePendentes = function () {
        var ret = 0;
        for ( var i = 0; i < $scope.transacoes.length; i++ ) {
            var el = $scope.transacoes[i];
            if ( !el.conferencia ) {
                ret += 1;
            }
        }
        return ret;
    };

    /* Carrega o dashboard. */
    DashboardService.getDashboard( dashboardConfig )
        .then( function ( resp ) {
            $scope.dashboard = resp;
        } )
        .catch( function ( textStatus ) {
            $rootScope.showErrorMessage( textStatus, null );
            $location.path( '/' );
            return;
        } );

    /* Monitora alterações no dashboard e as salva imediatamente. */
    $scope.$on( 'adfDashboardChanged', function ( event, name, model ) {
        DashboardService.setDashboard( dashboardConfig.indice, model )
            .catch( function ( textStatus ) {
                $rootScope.showErrorMessage( textStatus, null );
                $location.path( '/' );
                return;
            } );
    } );

    /* Atualiza o estado da conferência para os componentes externos. */
    var updateState = function () {
        $rootScope.navAllowPrevious = ( $scope.atual.indice > 0 );
        $rootScope.navAllowNext = ( $scope.atual.data.conferencia && $scope.atual.data.conferencia.status && $scope.atual.indice < ( $scope.transacoes.length - 1 ) );
        ConferenciaState.updateFromScope( $scope );
    };

    /* Consulta as próximas transações. */
    var getProximasTransacoes = async function () {
        try {
            $scope.contadores.pendentes = getQtdePendentes();
            var resp = await TransacoesApi.getTransacoes( $rootScope.user.id );
            var transacoes = resp.data;
            lastQtyReturned = transacoes.length;

            for ( var i = 0; i < transacoes.length; i++ ) {

                var exists = false;

                /* verifica se a transação recebida ainda não está inserida no escopo */
                for ( var j = 0; j < $scope.transacoes.length; j++ ) {
                    if ( $scope.transacoes[j].access.transaction === transacoes[i].access.transaction &&
                        $scope.transacoes[j].access.numserie === transacoes[i].access.numserie ) {
                        exists = true;
                        break;
                    }
                }

                /* insere a transação no escopo */
                if ( !exists ) {
                    var tran = transacoes[i];

                    /* monta a url da imagem de cadastro */
                    tran.imagens = {};
                    tran.imagens.cadastro = {
                        url: CommonLib.getCadastroFoto( transacoes[i].userId ),
                        loaded: false
                    };

                    /* monta as urls das imagens de acesso */
                    tran.imagens.acesso = [];
                    for ( var f = 0; f < tran.access.picturesIds.length; f++ ) {
                        tran.imagens.acesso.push( {
                            fotoId: tran.access.picturesIds[f],
                            url: CommonLib.getAcessoThumb( tran.access.numSerie, tran.access.transaction, tran.access.picturesIds[f] ),
                            grd: CommonLib.getAcessoFoto( tran.access.numSerie, tran.access.transaction, tran.access.picturesIds[f] ),
                            loaded: false
                        } );
                    }

                    $scope.transacoes.push( transacoes[i] );
                }
            }

            /* atualiza os contadores e as informações dos widgets */
            $scope.contadores.pendentes = getQtdePendentes();
        } catch ( e ) {
            $rootScope.showErrorMessage( textStatus, null );
            $location.path( '/' );
        }
    };

    /* Carrega os detalhes da transação atual. */
    var loadDetails = function () {
        $rootScope.loading = true;
        updateState();
        $rootScope.loading = false;
    };

    var loadIndice = function ( indice ) {
        $scope.atual.indice = indice;
        $scope.atual.data = $scope.transacoes[$scope.atual.indice];
        $scope.imagemAmpliada = $scope.atual.data.imagens.acesso[0];
        loadDetails();
        preloadImages( $scope.atual.indice );
    };

    var isPreloading = false;

    /* Pré-carrega as imagens. */
    var preloadImages = function ( idx, skip ) {
        if ( isPreloading && !skip ) {
            console.log( "return" );
            return;
        }

        isPreloading = true;

        if ( $scope.transacoes.length > idx ) {
            var proxima = $scope.transacoes[idx];

            var goToNext = function () {
                proxima.imagens.cadastro.loaded = true;

                for ( var i = 0; i < proxima.imagens.acesso.length; i++ ) {
                    proxima.imagens.acesso[i].loaded = true;
                }

                preloadImages( idx + 1, true );
            };

            if ( proxima.imagens.cadastro.loaded == true ) {
                goToNext();
                return;
            }

            var imagens = [];
            imagens.push( proxima.imagens.cadastro.url );

            for ( var i = 0; i < proxima.imagens.acesso.length; i++ ) {
                imagens.push( proxima.imagens.acesso[i].url );
            }

            // Inicia o pré-carregamento.
            preloader.preloadImages( imagens ).then(
                function handleResolve( imageLocations ) {
                    // Terminou de pré-carregar, pode partir para as próximas.
                    goToNext();
                },
                function handleReject( imageLocation ) {
                    // Falhou o carregamento de uma das imagens, não há o que fazer. Pode partir para as próximas.
                    goToNext();
                },
                function handleNotify( event ) {
                    //console.log( event.percent );
                }
            );
        } else {
            isPreloading = false;
        }
    };

    /* Monitora a lista de transações pendentes para antecipar a consulta das próximas. */
    var setupWatch = async function () {
        unbindWatch = $scope.$watch( function ( scope ) {
            return scope.contadores.pendentes;
        }, async function () {
            if ( $scope.transacoes.length > 0 ) {
                /* Não solicitar mais transações se:
                 * Restam mais de 5 transacoes pendentes para comparar
                 * Ultima solicitação de transações ao servidor retornou menos de 5
                 * */
                var half = qtdeFotosRequest / 2;
                var double = qtdeFotosRequest * 2;

                if ( $scope.contadores.pendentes > half || lastQtyReturned <= half ) {
                    return;
                }

                if ( $scope.transacoes.length >= double ) {
                    var diff = $scope.transacoes.length - qtdeFotosRequest;

                    $scope.transacoes.splice( 0, diff );
                    $scope.atual.indice -= diff;
                }
            }

            /* carrega mais transações */
            await getProximasTransacoes();

            /* caso não esteja visualizando nenhuma transação, avança a navegação */
            if ( $scope.atual.indice < 0 ) {
                navTo( 1 );
            }
        } );
    };

    /* Navegação segura no índice das transações. */
    var navTo = async function ( offset ) {
        if ( $rootScope.loading ) {
            console.log( 'RETORNOU! Estava loading...' );
            return;
        }

        var whereto = $scope.atual.indice + offset;

        //console.log( 'whereto: ' + whereto + ' pendentes: ' + $scope.contadores.pendentes + ' Total: ' + $scope.transacoes.length );

        if ( whereto < 0 ) {
            return;
        } else if ( whereto >= $scope.transacoes.length ) {
            await getProximasTransacoes();

            if ( lastQtyReturned === 0 ) {
                JobDone();
            }

            return;
        }

        loadIndice( whereto );
    };

    /* Finaliza a verificação do lote atual. */
    var JobDone = function () {
        if ( unbindWatch ) {
            unbindWatch();
            unbindWatch = null;
        }

        $location.path( '/' );
        swal( "Bom trabalho!", "Comparação finalizada com sucesso!", "success" );

        $rootScope.desabilitaFiltro = false;
    };

    $scope.ampliarImagem = function ( img, event, element ) {
        $scope.imagemAmpliada = img;

        if ( event != undefined ) {
            /* Não propagar o evento click para outras camadas. */
            if ( event.stopPropagation ) {
                event.stopPropagation();   // W3C model
                /* O stopPropagation está bloqueando a abertura do modal. */
                $( '#imagemAmpliada' ).modal();
            } else {
                event.cancelBubble = true; // IE model
            }
        }
    };

    /* Navega para a transação anterior. */
    $scope.$on( 'webFotosConfAnterior', function ( event ) {
        navTo( -1 );

    } );

    /* Navega para a próxima transação. */
    $scope.$on( 'webFotosConfProxima', function ( event ) {
        navTo( 1 );
    } );

    /* Inicializa todo o processo. */
    setupWatch();

    /* Acionar modal de relatar problema na câmera. */
    $rootScope.problemaCamera = function () {
        $rootScope.problemaCameraAtivo = true;
        CommonLib.OpenModal( 'templates/problema-camera.html', 'ProblemaCameraCtrl', { transacaoId: $scope.atual.data.id } );
    };

    $scope.estaVerificando = false;
    /* Define o estado de uma comparação. */
    $rootScope.setVerificacao = async function ( status, foto ) {

        // Se ainda está dentro do timeout da verificação, impede o usuário de realizar outra ação.
        if ( $scope.estaVerificando ) {
            return;
        }
        $scope.estaVerificando = true;

        var recId = 0;
        var statusAnterior = '';

        if ( $scope.atual.data.conferencia ) {
            /* Ja foi conferida antes. Deve guardar o Status Anterior. */
            statusAnterior = $scope.atual.data.conferencia.status;
        }

        $scope.atual.data.conferencia = {
            status: status,
            foto: foto
        };

        /* Define o recID... */
        switch ( status ) {
            case 'confere':
                recId = 2;
                break;

            case 'naoConfere':
                recId = 3;
                break;

            case 'ninguemNoFoco':
                recId = 6;
                break;

            case 'duvida':
                recId = 4;
                break;

            default:
                break;
        }

        if ( recId != 0 ) {

            try {
                await TransacoesApi.setStatus( $scope.atual.data.access.numSerie,
                    $scope.atual.data.access.transaction, recId, foto );

                //console.log( 'sucesso => transacao: ' + $scope.atual.data.access.transaction + 'numserie' + $scope.atual.data.access.numSerie );

                /* Ainda não foi comparada remove do pendente */
                if ( statusAnterior == '' ) {
                    $scope.contadores.pendentes -= 1;
                }

                //Chama a proxima transacao
                navTo( 1 );
            } catch ( textStatus ) {
                $rootScope.showErrorMessage( textStatus, null );
                $location.path( '/' );
                return;
            }

        }

        setTimeout( function () {
            $scope.estaVerificando = false;
        }, 600 );
    };

    // Método utilizado apenas quando o usuário realiza alguma ação pelo teclado, na conferência de fotos; 
    // Nele, apenas são tratadas as teclas das fotos (0 a 9).
    $rootScope.setVerificacaoTeclado = async function ( status, key ) {
        // obs.: se caso for PNE com acompanhante ou algum desses, a transação tem 10 fotos, habilitando a verificação com as teclas de 5 a 9
        if ( ( $scope.atual.data.access.picturesIds.length > 5 && key <= 9 ) ||
            ( $scope.atual.data.access.picturesIds.length <= 5 && key <= 4 ) ) {
            var foto = $scope.atual.data.access.picturesIds[key];
            await $rootScope.setVerificacao( 'confere', foto );
        } else {
            setTimeout( function () { }, 1000 );
        }
    };

    /// váriavel que manipula se alguma tecla do teclado foi pressionada no ultimo periodo de tempo indicado; 
    /// utilizada na logica que ajuda a evitar que o usuario clique repetidas vezes em uma tecla.
    $scope.keyPressed = false;
    $rootScope.keyup = async function ( $event, $root ) {

        // Se a popup para informar um problema na camera estiver na tela, as verificações pelo teclado são bloqueadas.
        if ( $rootScope.problemaCameraAtivo ) {
            return;
        }
        if ( $root.controller == 'ConferenciaCtrl' ) {

            if ( !$scope.keyPressed ) {

                /* keyCodes podem mudar de Browser p/ Browser e de Idioma p/ Idioma, então é melhor realizar a comparação pelo próprio valor da tecla  */
                if ( ( $event.key >= 0 && $event.key <= 9 ) ) {

                    var key = parseInt( $event.key ); //Vai representar um numero de 0 a 9 que será equivalente ao index da foto no array

                    //Passo key -1 pois key é a tecla digitada e o array (a ser utilizado mais para frente) começa do 0
                    await $rootScope.setVerificacaoTeclado( 'confere', key - 1 );

                } else {

                    if ( $event.key == "c" || $event.key == "C" ) { //c / Não Confere
                        await $rootScope.setVerificacao( 'naoConfere', 0 );
                    }
                    else if ( $event.key == "f" || $event.key == "F" ) { //f / Ninguém no Foco
                        await $rootScope.setVerificacao( 'ninguemNoFoco', 0 );
                    }
                    else if ( $event.key == "e" || $event.key == "E" ) { //e / Duvida
                        await $rootScope.setVerificacao( 'duvida', 0 );
                    }
                    else if ( $event.key == "s" || $event.key == "S" ) { //s / Problema na Câmera
                        await $rootScope.problemaCamera();
                    }
                    else if ( $event.key == "ArrowLeft" ) { //seta para esquerda / Transação Anterior
                        if ( $rootScope.navAllowPrevious ) { // só permite navegar para a comparação anterior se o controle estiver habilitado
                            navTo( -1 );
                        }
                    }
                    else if ( $event.key == "ArrowRight" ) { //seta para direita / Próxima Transação
                        if ( $rootScope.navAllowNext ) { // só permite navegar para a próxima comparação se o controle estiver habilitado
                            navTo( 1 );
                        }
                    }
                }
                $scope.keyPressed = true;
            }

            setTimeout( function () {
                $scope.keyPressed = false;
            }, 600 );
        }
    };
} );
