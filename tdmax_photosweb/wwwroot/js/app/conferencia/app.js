﻿var module = angular.module('WebFotos.Conferencia', [
    'ngTable',
    'WebFotos.Common',
    'WebFotos.Data',
    'WebFotos.Dashboard',
    'BenNadel.Preloader',
    'WebFotos.Directives'
]);

/* Constantes. */
module.value('qtdeFotosRequest', 10);
module.value('storageKeyName', 'ContadoresConferencias');

/* Referências das configurações globais utilizadas. */
module.value('biometriaFacialAtivada', {
    processo: "21", indice: "1", item: "10"
} );

/* Referências das configurações globais utilizadas. */
module.value( 'dashboardConfig', {
    processo: "21", item: "1", indice: "15"
} );

var getConfigBioAtiva = async function ( $rootScope, ConfiguracaoApi, biometriaFacialAtivada ) {
    var bio = await ConfiguracaoApi.getConfiguracao( biometriaFacialAtivada );

    $rootScope.config.biometriaFacialAtivada = bio.data;
};

module.run( function ( $rootScope, $location, $routeParams, ConfiguracaoApi, ServerInfoApi, biometriaFacialAtivada  ) {
    /* Inicializa as configurações globais. */
    if (!$rootScope.config) {
        $rootScope.config = {};
    }
    $rootScope.config.biometriaFacialAtivada = false;

    /* Consulta as configurações globais. */
    getConfigBioAtiva( $rootScope, ConfiguracaoApi, biometriaFacialAtivada );

    /* Carrega as configurações do servidor. */
    ServerInfoApi.getServerInfo().then(function (data) {
        $rootScope.path = {
            acesso: data.fotosAcesso,
            cadastro: data.fotosCadastro
        };
    });

    /* Controle da contagem das conferências. */
    $rootScope.contadores = { pendentes: 0, confere: 0, naoConfere: 0, ninguemNoFoco: 0, duvida: 0 };

    /* Váriavel utilizada para controle do modal de Problema de Camera */
    $rootScope.problemaCameraAtivo = false;

    $rootScope.playConferencia = function ( controller ) {
        if ( controller !== 'ConferenciaCtrl' && controller !== 'ReclassificacaoCtrl' ) {
            $location.path( '/conferencia' );
        }
        else {
            if ( $routeParams.cadastroID != undefined ) {
                $location.path( '/exibe-auditar-conferencias/' + $routeParams.cadastroID );
            }
            else {
                $location.path( '/' );
            }
        }
    };
});