﻿var module = angular.module('WebFotos.Conferencia');

module.service('ConferenciaState', function ($rootScope) {
    this.data = {
        advertencias: {},
        register: {},
        access: {},
        penalizacao: {},
        cadastro: {},
        integration: {}
    };

    this.updateFromScope = function ( scope ) {
        /* Informações disponíveis sobre a comparação atual. */
        this.data.register.cadastroId = scope.atual.data.userId;
        this.data.register.cadastroTipo = scope.atual.data.type;
        this.data.register.subTipo = scope.atual.data.subType;
        this.data.register.numSerie = scope.atual.data.access.numSerie;

        /* Informações disponíveis sobre o turno do acesso. */
        this.data.access.equipamento = scope.atual.data.access.equipment;
        this.data.access.sequencia = scope.atual.data.access.sequence;
        this.data.access.turnoId = scope.atual.data.access.shiftId;
        this.data.access.transacao = scope.atual.data.access.transaction;
        this.data.access.dataAcesso = scope.atual.data.access.accessDate;
        //this.data.access.numSerie = scope.atual.data.access.numSerie;
        this.data.access.motorista = scope.atual.data.access.driver;
        this.data.access.cobrador = scope.atual.data.access.ticketCollector;
        this.data.access.linha = scope.atual.data.access.line;
        this.data.access.prefixo = scope.atual.data.access.prefix;

        /* Informações disponíveis sobre o turno da integração. */
        this.data.integration.equipamento = scope.atual.data.integration.equipment;
        this.data.integration.sequencia = scope.atual.data.integration.sequence;
        this.data.integration.turnoId = scope.atual.data.integration.shiftId;
        this.data.integration.transacao = scope.atual.data.integration.transaction;
        this.data.integration.dataAcesso = scope.atual.data.integration.accessDate;
        //this.data.integration.numSerie = scope.atual.data.integration.numSerie;
        this.data.integration.motorista = scope.atual.data.integration.driver;
        this.data.integration.cobrador = scope.atual.data.integration.ticketCollector;
        this.data.integration.linha = scope.atual.data.integration.line;
        this.data.integration.prefixo = scope.atual.data.integration.prefix;
    };

    this.updateFromScopeGratuito = function ( scope ) {

        // Informações disponíveis sobre o acesso.
        this.data.comparacao.numSerie = scope.atual.data.ct;
        this.data.comparacao.transacao = scope.atual.data.trn;
        this.data.comparacao.dataAcesso = scope.atual.data.data;
        this.data.comparacao.cadastroTipo = scope.atual.data.tc;
        this.data.comparacao.subTipo = scope.atual.data.sc;

        // Informações disponíveis sobre o turno. 
        this.data.access.equipamento = scope.atual.data.eq;
        this.data.access.sequencia = scope.atual.data.sq;
        this.data.access.motorista = scope.atual.data.motorista;
        this.data.access.cobrador = scope.atual.data.cobrador;
        this.data.access.linha = scope.atual.data.linha;
        this.data.access.prefixo = scope.atual.data.prefixo;

    };

    this.updateFromScopePenalizacao = function ( scope ) {
        /* Informações disponíveis sobre a comparação atual. */
        this.data.comparacao.cadastroId = scope.atual.data.cid;
        this.data.comparacao.transacaoId = scope.atual.data.id;
        this.data.comparacao.dataAcesso = scope.detalhes.acessoInfo.data;
        this.data.comparacao.cadastroTipo = scope.detalhes.acessoInfo.cadastroTipo;
        this.data.comparacao.subTipo = scope.detalhes.acessoInfo.subTipo;
        this.data.comparacao.acessosMeiaViagem = scope.atual.data.acm == 255 ? "Ilimitada" : scope.atual.data.acm;
        this.data.comparacao.numSerie = scope.detalhes.acessoInfo.cartao;

        /* Informações disponíveis sobre o turno. */
        this.data.access.equipamento = scope.atual.data.eq;
        this.data.access.sequencia = scope.atual.data.sq;
        this.data.access.turnoId = scope.detalhes.acessoInfo.turnoId;
        this.data.access.motorista = scope.detalhes.acessoInfo.motorista;
        this.data.access.cobrador = scope.detalhes.acessoInfo.cobrador;
        this.data.access.linha = scope.detalhes.acessoInfo.linha;
        this.data.access.prefixo = scope.detalhes.acessoInfo.prefixo;

        this.data.cadastro.penalizacaoDesc = scope.detalhes.cadastroInfo.PenalizacaoDesc;
        this.data.penalizacao.divergencia = scope.detalhes.penalizacao.Ordem;
        this.data.penalizacao.penalidade = scope.detalhes.penalizacao.Descricao;

        this.data.cadastro.cartaoAtual = scope.detalhes.cadastroInfo.Cartao;
        this.data.cadastro.status = scope.detalhes.cadastroInfo.Status;
    };

    this.updateFromScopeReclassificacao = function ( scope ) {
        /* Informações disponíveis sobre a comparação atual. */
        this.data.comparacao.cadastroId = scope.atual.data.cid;
        this.data.comparacao.transacaoId = scope.atual.data.id;
        this.data.comparacao.dataAcesso = scope.detalhes.acessoInfo.data;
        this.data.comparacao.cadastroTipo = scope.detalhes.acessoInfo.cadastroTipo;
        this.data.comparacao.subTipo = scope.detalhes.acessoInfo.subTipo;
        this.data.comparacao.acessosMeiaViagem = scope.atual.data.acm == 255 ? "Ilimitada" : scope.atual.data.acm;
        this.data.comparacao.numSerie = scope.detalhes.acessoInfo.cartao;

        /* Informações disponíveis sobre o turno. */
        this.data.access.equipamento = scope.atual.data.eq;
        this.data.access.sequencia = scope.atual.data.sq;
        this.data.access.turnoId = scope.detalhes.acessoInfo.turnoId;
        this.data.access.motorista = scope.detalhes.acessoInfo.motorista;
        this.data.access.cobrador = scope.detalhes.acessoInfo.cobrador;
        this.data.access.linha = scope.detalhes.acessoInfo.linha;
        this.data.access.prefixo = scope.detalhes.acessoInfo.prefixo;
    };
});