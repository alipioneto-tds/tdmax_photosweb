﻿var module = angular.module('WebFotos.Conferencia');

/* module directives */
module.directive('wfConfAnterior', function ($rootScope) {
    return {
        link: function (scope, element) {
            var el = angular.element(element[0]);
            var trigger = function (ev) { $rootScope.$broadcast('webFotosConfAnterior'); }
            el.bind('click', trigger);
            //angular.element(document).bind('keydown', 'left', trigger);
        }
    }
});

module.directive('wfConfProxima', function ($rootScope) {
    return {
        link: function (scope, element) {
            var el = angular.element(element[0]);
            var trigger = function () { $rootScope.$broadcast('webFotosConfProxima'); }
            el.bind('click', trigger);
            //angular.element(document).bind('keydown', 'right', trigger);
        }
    }
});
