﻿var module = angular.module('WebFotos.AuditarConferencias');


module.controller('PesquisaAuditarConfCtrl', function ($scope, AuditarConferenciasApi) {
    var efetuarBusca = undefined;

    $scope.buscaCad = '';
    $scope.buscaNumSerie = '';

    $scope.loaded = true;
    $scope.list = [];

    var carregar = function () {
        AuditarConferenciasApi.getCadastros($scope.buscaCad, $scope.buscaNumSerie).then(function (data) {
            $scope.list = data;
            $scope.loaded = true;
        })
    };

    $scope.buscar = function (timeout) {
        if (efetuarBusca) {
            clearTimeout(efetuarBusca);
            efetuarBusca = undefined;
        }

        $scope.loaded = false;
        efetuarBusca = setTimeout(carregar, timeout);
    };
});


module.controller('ExibeAuditarConfCtrl', function ($scope, $routeParams, $modal, $location, AuditarConferenciasApi) {
    $scope.model = {};
    $scope.loaded = false;

    var carregar = function () {
        AuditarConferenciasApi.getAuditoriaCadastro($routeParams.cadastroID).then(function (data) {
            $scope.model = data;
            $scope.loaded = true;

            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
            }, 500);
        }).catch(function (textStatus) {
            swal("Erro!", textStatus.data.ExceptionMessage, "error");
        });
    };

    $scope.desbloquear = function () {
        swal({
            title: "Deseja realmente desbloquear o cadastro: " + $scope.model.Nome + "?",
            text: "Atenção: Está operação não poderá ser desfeita. Este cadastro irá recuperar o acesso ao sistema.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-lg btn-info",
            confirmButtonText: "Desbloquear",
            cancelButtonText: "Cancelar"
        }).then(function () {
            AuditarConferenciasApi.setDesbloquearCadastro($scope.model.CadastroID).then(function (data) {
                if (data) {
                    swal("Cadastro " + $scope.model.Nome + " desbloqueado com sucesso!", "", "success");
                    carregar();
                }
            }).catch(function (textStatus) {
                swal("Erro ao desbloquear cadastro!", textStatus.data.ExceptionMessage, "error");
            });
        });
    }

    $scope.reclassificar = function () {
        var modal = $modal.open({
            templateUrl: 'Content/templates/auditarConferencias/tipos-reclassificacao.html',
            controller: 'TiposReclassificacaoCtrl',
            resolve: {
                data: function () {
                    return { cadastroId: $routeParams.cadastroID };
                }
            }
        });

        modal.result.then(function (data) {
            if (data != undefined) {
                $location.path('/reclassificacao/' + $routeParams.cadastroID + '/' + data.selecionado.StatusReconhecimentoID);
            }
        });
    }

    carregar();
});


module.controller('TiposReclassificacaoCtrl', function ($scope, $modalInstance, data, AuditarConferenciasApi) {
    $scope.tiposRec = [];

    $scope.selecionado = {};

    AuditarConferenciasApi.getTiposReclassificacaoCadastro(data.cadastroId).then(function (data) {
        if (data.length == 0) {
            swal("Bom trabalho!", "Todas as comparações pendentes foram reclassificadas / zeradas com sucesso.", "success");
            $scope.cancel();
        }

        $scope.tiposRec = data;
    });

    $scope.save = function () {
        $modalInstance.close({
            selecionado: $scope.selecionado
        });
    }

    $scope.cancel = function () {
        $modalInstance.close();
    }

    $scope.selecionarTipo = function (tipo) {
        for (var i = 0; i < $scope.tiposRec.length; i++) {
            $scope.tiposRec[i].selecionado = false;
        }

        tipo.selecionado = true;

        $scope.selecionado = tipo;
    };
});

module.controller('ReclassificacaoCtrl', function ($scope, $rootScope, $q, $location, $routeParams,
    AuditarConferenciasApi, CommonLib, preloader, AcessoInfoApi, ConferenciaState, DashboardApi) {
    /* Declaração das variáveis locais. */
    $scope.transacoes = [];
    $scope.atual = {
        indice: -1,
        data: {}
    };

    $scope.detalhes = {
        acessoInfo: {}
    };

    $scope.TotalConferidas = 0;

    $scope.imagemAmpliada = {};
    $rootScope.loading = false;

    var indiceAnterior = -1;

    /* Carrega o dashboard. */
    DashboardApi.getDashboardReclassificacao().then(function (dashboard) {
        $scope.dashboard = dashboard;
    });

    /* Consulta o próximo lote. */
    var getProximasTransacoes = function (callback) {
        var cameraTransIdOffset = 0;

        if ($scope.transacoes.length > 0 && $routeParams.statusReconhecimentoID > 0)
        {
            cameraTransIdOffset = $scope.transacoes[$scope.transacoes.length - 1].id;
        }

        if ($routeParams.cadastroID != undefined) {
            AuditarConferenciasApi.getTransacoesReclassificacao($routeParams.cadastroID, $routeParams.statusReconhecimentoID, cameraTransIdOffset).then(function (data) {
                if (data && data.transacoes.length == 0) {
                    $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
                    swal("Bom trabalho!", "Todas as comparações pendentes foram reclassificadas / zeradas com sucesso.", "success");
                    return;
                }

                $scope.transacoes = data.transacoes;

                updateState();

                if (callback) {
                    callback();
                }

            }).catch(function () {
                getProximasTransacoes(callback);
            });
        }
    };

    /* Atualiza o estado da conferência para os componentes externos. */
    var updateState = function () {
        $rootScope.navAllowPrevious = $scope.atual.indice > 0;
        $rootScope.navAllowNext = ($scope.atual.data.conferencia && $scope.atual.data.conferencia.status && $scope.atual.indice < ($scope.transacoes.length - 1));
        ConferenciaState.updateFromScopeReclassificacao($scope);
    };

    /* Consulta as próximas transações. */
    var getFotos = function (callback) {
        var transacoes = $scope.transacoes;

        for (var i = 0; i < transacoes.length; i++) {

            /* insere a transação no escopo */
            var tran = transacoes[i];

            tran.imagens = {};
            tran.imagens.cadastro = {
                url: CommonLib.getCadastroFoto(transacoes[i].cid), // baixando sempre a foto de cadastro em tamanho original, pois a miniatura está com problemas de otimização, ficando mais do que 100% maior que a original
                loaded: false
            };

            /* monta as urls das imagens de acesso */
            tran.imagens.acesso = [];
            for (var f = 0; f < tran.fts.length; f++) {
                tran.imagens.acesso.push({
                    fotoId: tran.fts[f],
                    url: CommonLib.getAcessoThumb(tran.eq, tran.sq, tran.fts[f]),
                    grd: CommonLib.getAcessoFoto(tran.eq, tran.sq, tran.fts[f]),
                    loaded: false
                });
            }
        }

        $scope.transacoes = [];
        $scope.transacoes = transacoes;

        updateState();

        if (callback) {
            callback();
        }
    };

    /* Navegação segura no índice das transações. */
    var navTo = function (offset) {
        if ($rootScope.loading) {
            console.log('RETORNOU! Estava loading...');
            return;
        }

        var whereto = $scope.atual.indice + offset;

        if (indiceAnterior != -1) {

            /* Define o whereto para retornar ao indice anterior */
            whereto = indiceAnterior;

            /* Reseta o indiceAnterior... */
            indiceAnterior = -1;

            console.log('<<< Retornando ao indice anterior: ' + whereto);
            loadIndice(whereto);
            return;
        }

        console.log('whereto: ' + whereto + ' Total: ' + $scope.transacoes.length);

        if (whereto < 0) {
            return;
        } else if (whereto >= $scope.transacoes.length) {
            $scope.atual.indice = 0;
            /// verificar se tem mais transacoes para verificar (getProximasTransacoes).
            //se sim, carrega as mesmas. se não, lote finalizado.
            getProximasTransacoes(function () {
                getFotos(function () {
                    navTo(0);
                });
            });

            return;
        }

        loadIndice(whereto);
    };

    var loadIndice = function (indice) {
        console.log('Vai carregar indice: ' + indice + ' transacao: ' + $scope.transacoes[indice].id);
        $scope.atual.indice = indice;
        $scope.atual.data = $scope.transacoes[$scope.atual.indice];

        if ($routeParams.statusReconhecimentoID == 4) {
            $scope.atual.data.conferencia = {
                status: 'duvida',
                foto: undefined
            };
        } else if ($routeParams.statusReconhecimentoID == 6) {
            $scope.atual.data.conferencia = {
                status: 'ninguemNoFoco',
                foto: undefined
            };
        }

        $scope.imagemAmpliada = $scope.atual.data.imagens.acesso[0];
        loadDetails();
        preloadImages($scope.atual.indice);
    };

    /* Carrega os detalhes da transação atual. */
    var loadDetails = function () {
        var cadastroId = $scope.atual.data.cid;
        var cameraTransacaoId = $scope.atual.data.id;

        $rootScope.loading = true;

        $q.all([
            AcessoInfoApi.getAcessoInfo(cameraTransacaoId)
        ])
            .then(function (data) {
                $scope.detalhes.acessoInfo = data[0];
                updateState();
                $rootScope.loading = false;
            });
    };

    var isPreloading = false;
    /* Pré-carrega as imagens. */
    var preloadImages = function (idx, skip) {
        if (isPreloading && !skip) {
            return;
        }

        isPreloading = true;

        if ($scope.transacoes.length > idx) {
            var proxima = $scope.transacoes[idx];

            var imagens = [];
            //imagens.push(proxima.imagens.cadastro.url);

            for (var i = 0; i < proxima.imagens.acesso.length; i++) {
                imagens.push(proxima.imagens.acesso[i].url);
            }

            var goToNext = function () {
                preloadImages(idx + 1, true);
            };

            // Inicia o pré-carregamento.
            preloader.preloadImages(imagens).then(
                function handleResolve(imageLocations) {
                    // Terminou de pré-carregar, pode partir para as próximas.
                    goToNext();
                },
                function handleReject(imageLocation) {
                    // Falhou o carregamento de uma das imagens, não há o que fazer. Pode partir para as próximas.
                    goToNext();
                },
                function handleNotify(event) {
                    //
                }
            );
        } else {
            isPreloading = false;
        }
    };

    $scope.ampliarImagem = function (img, event, element) {
        $scope.imagemAmpliada = img;
        /* Não propagar o evento click para outras camadas. */
        if (event.stopPropagation) {
            event.stopPropagation();   // W3C model
            /* O stopPropagation está bloqueando a abertura do modal. */
            $('#imagemAmpliada').modal();
        } else {
            setVerificacao
            event.cancelBubble = true; // IE model
        }
    };

    /* Navega para a transação anterior. */
    $scope.$on('webFotosConfAnterior', function (event) {
        navTo(-1);

    });

    /* Navega para a próxima transação. */
    $scope.$on('webFotosConfProxima', function (event) {
        navTo(1);
    });

    $scope.estaVerificando = false;

    /* Define o estado de uma comparação. */
    $rootScope.setVerificacao = function (status, foto) {
        var recId = 0;

        // Se ainda está dentro do timeout da verificação, impede o usuário de realizar outra ação.
        if ($scope.estaVerificando) {
            return;
        }

        $scope.estaVerificando = true;

        /* Define o recID... */
        switch (status) {
            case 'confere':
                recId = 2;
                break;

            case 'naoConfere':
                recId = 3;
                break;

            case 'ninguemNoFoco':
                recId = 6;
                break;

            case 'duvida':
                recId = 4;
                break;

            default:
                break;
        }

        $scope.atual.data.conferencia = {
            status: status,
            foto: foto
        };

        if (recId != 0) {

            /* CSTDMAXV2-3536 - Passar para a proxima transacao e contabilizar conferencia apenas se conseguir definir o Status da Verificacao, 
                                para evitar que pule  transacoes e prejudique a finalizacao do Lote. */
            AuditarConferenciasApi.setStatusReclassificacao($scope.atual.data.id, recId, foto).then(
                /* Sucesso */
                function (tran) {
                    console.log('tran: ' + tran + ' transacao: ' + $scope.atual.data.id);

                    if (tran != $scope.atual.data.id) {
                        swal("Erro!", "Transacoes diferentes", "error");

                        if ($routeParams.cadastroID != undefined) {
                            $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
                        }
                        else {
                            $location.path('/');
                        }

                        return;
                    }

                    // Contabiliza a conferencia atual
                    $scope.TotalConferidas += 1;

                    //Chama a proxima transacao
                    navTo(1);
                }
            ).catch(function (textStatus) {
                swal("Erro!", textStatus.data.ExceptionMessage, "error");
                if ($routeParams.cadastroID != undefined) {
                    $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
                }
                else {
                    $location.path('/');
                }
                return;
            });
        }

        setTimeout(function () {
            $scope.estaVerificando = false;
        }, 1000);
    };

    // Método utilizado apenas quando o usuário realiza alguma ação pelo teclado, na conferência de fotos; 
    // Nele, apenas são tratadas as teclas das fotos (0 a 9).
    $rootScope.setVerificacaoTeclado = function (status, key) {
        // obs.: se caso for PNE com acompanhante ou algum desses, a transação tem 10 fotos, habilitando a verificação com as teclas de 5 a 9
        if (($scope.atual.data.fts.length > 5 && key <= 9) || ($scope.atual.data.fts.length <= 5 && key <= 4)) {
            var foto = $scope.atual.data.fts[key];
            $rootScope.setVerificacao('confere', foto);
        } else {
            setTimeout(function () { }, 1000);
        }
    };

    /// váriavel que manipula se alguma tecla do teclado foi pressionada no ultimo periodo de tempo indicado; 
    /// utilizada na logica que ajuda a evitar que o usuario clique repetidas vezes em uma tecla.
    $scope.keyPressed = false;
    $rootScope.keyup = function ($event, $root) {
        if ($root.controller == 'ReclassificacaoCtrl') {

            if (!$scope.keyPressed) {

                /* keyCodes podem mudar de Browser p/ Browser e de Idioma p/ Idioma, então é melhor realizar a comparação pelo próprio valor da tecla  */
                if (($event.key >= 0 && $event.key <= 9)) {

                    var key = parseInt($event.key); //Vai representar um numero de 0 a 9 que será equivalente ao index da foto no array

                    //Passo key -1 pois key é a tecla digitada e o array (a ser utilizado mais para frente) começa do 0
                    $rootScope.setVerificacaoTeclado('confere', key - 1);

                } else {

                    if ($event.key == "c" || $event.key == "C") { //c / Não Confere
                        $rootScope.setVerificacao('naoConfere', 0);
                    }
                    else if ($event.key == "f" || $event.key == "F") { //f / Ninguém no Foco
                        $rootScope.setVerificacao('ninguemNoFoco', 0);
                    }
                    else if ($event.key == "e" || $event.key == "E") { //e / Duvida
                        $rootScope.setVerificacao('duvida', 0);
                    }
                    else if ($event.key == "s" || $event.key == "S") { //s / Problema na Câmera
                        $rootScope.problemaCamera();
                    }
                    else if ($event.key == "ArrowLeft") { //seta para esquerda / Transação Anterior
                        if ($rootScope.navAllowPrevious) { // só permite navegar para a comparação anterior se o controle estiver habilitado
                            navTo(-1);
                        }
                    }
                    else if ($event.key == "ArrowRight") { //seta para direita / Próxima Transação
                        if ($rootScope.navAllowNext) { // só permite navegar para a próxima comparação se o controle estiver habilitado
                            navTo(1);
                        }
                    }
                }
                $scope.keyPressed = true;
            }

            setTimeout(function () {
                $scope.keyPressed = false;
            }, 800);
        }
    };

    getProximasTransacoes(function () {
        getFotos(function () {
            /* caso não esteja visualizando nenhuma transação, avança a navegação */
            if ($scope.atual.indice < 0) {
                navTo(1);
            }
        });
    });
});