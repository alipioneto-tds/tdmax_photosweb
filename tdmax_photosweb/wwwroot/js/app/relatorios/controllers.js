﻿var module = angular.module('WebFotos.Relatorios');

module.directive('clickOutside', function ($document) {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            elem.bind('click', function (e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function () {
                // magic here.
                if (attr.clickOutside === "vm.showFilters=false") {
                    if (scope.$apply("vm.ignoreShowFilters") === true) {
                        scope.$apply("vm.ignoreShowFilters=false");
                        return;
                    }
                    else {
                        scope.$apply(attr.clickOutside);
                    }
                }
                else if (attr.clickOutside === "vm.showPivotEditor=false") {
                    if (scope.$apply("vm.ignorePivotEditor") === true) {
                        scope.$apply("vm.ignorePivotEditor=false");
                        return;
                    }
                    else {
                        scope.$apply(attr.clickOutside);
                    }
                }
                else if (attr.clickOutside === "vm.showPivotChartEditor=false") {
                    if (scope.$apply("vm.ignoreChartEditor") === true) {
                        scope.$apply("vm.ignoreChartEditor=false");
                        return;
                    }
                    else {
                        scope.$apply(attr.clickOutside);
                    }
                }
                else if (attr.clickOutside === "vm.showShareHistory=false") {
                    if (scope.$apply("vm.ignoreShareHistory") === true) {
                        scope.$apply("vm.ignoreShareHistory=false");
                        return;
                    }
                    else {
                        scope.$apply(attr.clickOutside);
                    }
                }
                else if (attr.clickOutside === "vm.showShareForm=false") {
                    if (scope.$apply("vm.ignoreShareHistory") === true) {
                        scope.$apply("vm.ignoreShareHistory=false");
                        return;
                    }
                    else {
                        scope.$apply(attr.clickOutside);
                    }
                }
            })
        }
    }
});

module.controller('PesquisaRelatorioCtrl', function ($scope, $http) {
    $scope.relatorios = {};

    $http({ method: 'GET', url: 'Relatorios/Index' })
        .then(function (res) {
            $scope.relatorios = res.data.relatorios;
        });
});

module.controller('ExibeRelatorioCtrl', function ($scope, $http, $timeout, $routeParams, $location) {
    var self = this
    self.filter = {}
    self.filteredBy = {}
    self.reportData = {}
    self.paginaHistorico = 0
    self.ultimaPaginaHistorico = false
    self.chartSettings = undefined
    self.titulo = ''
    self.ignoreShowFilters = false
    self.ignorePivotEditor = false
    self.ignoreChartEditor = false
    self.ignoreShareHistory = false

    var uri = {
        exibir: 'Relatorios/Exibir/' + $routeParams.relatorioId,
        consultar:'Relatorios/Consultar/' +  $routeParams.relatorioId,
        salvarEditor: 'Relatorios/SalvarFiltros/' + $routeParams.relatorioId,
        compartilharLink: 'Relatorios/CompartilharLink/' + $routeParams.relatorioId,
        renomearLink: 'Relatorios/RenomearLink/' + $routeParams.relatorioId,
        historicoLinks: 'Relatorios/HistoricoLinks/' + $routeParams.relatorioId,
        enviarEmail: 'Relatorios/EnviarEmail/' + $routeParams.relatorioId,
        linkCompartilhado: 'Relatorios/Compartilhar' + $routeParams.link
    }

    self.mensagemSucesso = function (msg) {
        toastr.success(msg)
    }

    self.mensagemErro = function (err) {
        var errmsg

        if (err.data && err.data.Exception) {
            errmsg = err.data.Exception
        } else if (err.statusText) {
            errmsg = err.statusText
        } else if (err) {
            errmsg = err
        } else {
            errmsg = "Erro não tratado!"
        }

        toastr.error(errmsg)
    }

    self.formatarParametros = function () {
        var query = {}

        Object.keys(self.filter).forEach(function (q) {
            var e = self.filter[q]
            if (e && (e.start !== undefined)) {
                if (e.end) {
                    if (e.start.length > 0 && e.end.length > 0) {
                        var a = moment(e.start, 'DD/MM/YYYY').format('YYYY-MM-DD')
                        var b = moment(e.end, 'DD/MM/YYYY').format('YYYY-MM-DD')
                        query[q] = a + '|' + b
                    }
                }
            } else if (e && e.min) {
                if (e.max) {
                    if (e.min.length > 0 && e.max.length > 0) {
                        query[q] = e.min + '|' + e.max
                    }
                }
            } else {
                query[q] = e
            }
        })

        return query
    }

    self.atualizarFiltros = function (novosFiltros) {
        self.filteredBy = {}

        Object.keys(novosFiltros).forEach(function (key) {
            var findcol = self.reportData.columns.filter(function (i) {
                return i.id === key
            })

            if (findcol.length === 1) {
                var column = findcol[0]
                var value = novosFiltros[key]
                var split = value.split('|')

                switch (column.type) {
                    case 'date':
                    case 'datetime':
                        self.filteredBy[column.id] = { start: moment(split[0], 'YYYY-MM-DD').format('DD/MM/YYYY'), end: moment(split[1], 'YYYY-MM-DD').format('DD/MM/YYYY') }
                        break

                    case 'integer':
                    case 'decimal':
                        self.filteredBy[column.id] = { min: split[0], max: split[1] }
                        break

                    default:
                        self.filteredBy[column.id] = value
                        break
                }
            }
        })

        self.filter = JSON.parse(JSON.stringify(self.filteredBy))
    }

    self.contarItens = function () {
        var ret = 0

        if (self.reportData && self.reportData.columns) {
            self.reportData.columns.forEach(function (col) {
                if (self.filtroAtivo(col, self.filteredBy[col.id])) {
                    ret++
                }
            })
        }

        return ret
    }

    self.filtroAtivo = function (col, f) {
        switch (col.type) {
            case 'date':
            case 'datetime':
                if (f && f.start && f.start.length > 0 && f.end && f.end.length > 0) {
                    return true
                }
                break

            case 'integer':
            case 'decimal':
                if (f && f.min && f.min.length > 0 && f.max && f.max.length > 0) {
                    return true
                }
                break

            case 'boolean':
                if (f == 'true' || f == 'false') {
                    return true
                }
                break

            default:
                if (f && f.length > 0) {
                    return true
                }
                break
        }

        return false
    }

    self.verificarLink = function () {
        var filtros = $('#LinkFiltros')
        var config = $('#LinkConfiguracoes')

        if (filtros.length && config.length) {
            var f = {}

            filtros.val().split('&').forEach(function (i) {
                var kv = i.split('=');

                if (kv.length === 2) {
                    var key = kv[0];
                    var value = kv[1];
                    var multiple = value.split('|');

                    if (multiple.length === 2) {
                        if (/[0-9]+\/[0-9]+\/[0-9]+/.exec(multiple[0])) {
                            var a = moment(multiple[0], 'DD/MM/YYYY').format('YYYY-MM-DD');
                            var b = moment(multiple[1], 'DD/MM/YYYY').format('YYYY-MM-DD');
                            value = a + '|' + b
                        }
                    }

                    f[key] = value;
                }
            })

            return {
                filtros: f,
                config: JSON.parse(config.val())
            }
        }

        return undefined
    }

    self.atualizarGrafico = function () {
        var settings = self.reportData.chartSettings
        if (settings && settings.Type && settings.X && settings.Y) {
            if (self.userSettings === null ) {
                self.userSettings = {}
            }

            self.userSettings.groups = [_.find(self.reportData.columns, { id: settings.X })]
            self.userSettings.aggregations = {}

            if (self.reportData.rows.length > 0) {
                self.userSettings.values = _.map(settings.Y, function (i) {
                    self.userSettings.aggregations[i.ColumnName] = i.Operation.toLowerCase()
                    return _.find(self.reportData.columns, { label: i.ColumnName })
                })
            }

            self.viewChart = true
        } else {
            self.viewChart = false
        }
    }

    self.aplicarFiltros = function (settings, cb) {
        var query = self.formatarParametros()
        self.loading = true

        var dadosLink = self.verificarLink()

        if (dadosLink && dadosLink.filtros && dadosLink.config) {
            query = dadosLink.filtros
            settings = dadosLink.config
        }

        if ($routeParams.link !== undefined) {
            $http({ method: 'GET', url: 'Relatorios/Compartilhar/' + $routeParams.link })
                .then(function (res) {
                    self.titulo = res.data.titulo
                    uri.consultar = 'Relatorios/Consultar/' + res.data.id
                    self.consultar(settings, query, cb)

                }, function (err) {
                    self.mensagemErro(err)
                })
        }
        else {
            $http({ method: 'GET', url: uri.exibir, params: query })
                .then(function (res) {
                    self.titulo = res.data.titulo
                    self.consultar(settings, query, cb)

                }, function (err) {
                    self.mensagemErro(err)
                })
        }
    }

    self.consultar = function (settings, query, cb) {
        $http({ method: 'GET', url: uri.consultar, params: query })
            .then(function (res) {
                self.userSettings = settings || JSON.parse(res.data.userSettings)
                delete res.data.userSettings

                self.reportData = res.data
                self.atualizarFiltros(res.data.filters)
                self.atualizarGrafico()

                if (cb) {
                    cb();
                }

            }, function (err) {
                self.mensagemErro(err)
            })
            .finally(function () {
                $timeout(function () {
                    self.loading = false
                }, 500)
            })
    }

    self.removerHashKeys = function (i) {
        delete i.$$hashKey
        return i
    }

    var exibirFoto = function (id) {
        window.open("#/foto-acessso/" + id)
    }


    self.rowCallbackClick = function (id) {
        if (self.reportData.rowCallback.function == 'exibirFoto') {
            exibirFoto(id)
        }
    }

    self.salvarConfiguracoes = function () {
        var filtros = ''

        var config = _.clone(self.userSettings)
        config.groups = _.map(config.groups, self.removerHashKeys)
        config.values = _.map(config.values, self.removerHashKeys)
        config.hide = _.chain(config.columns)
            .filter(function (i) {
                return i.hidden
            })
            .map(function (i) {
                return i.id
            })
            .value()
        delete config.columns

        $http({ method: 'POST', url: uri.salvarEditor, params: { filtros: filtros, config: JSON.stringify(config) } })
            .then(function (res) {
                self.mensagemSucesso('Configurações salvas com sucesso!')
            }, function (err) {
                self.mensagemErro(err)
            })
    }

    self.editarDescricao = function () {
        var tituloRelatorio = $('h1').html()
        var atual = self.linkDescricao || tituloRelatorio
        self.linkDescricao = window.prompt('Digite uma descrição para identificar este link (opcional):', atual)

        if (!self.linkDescricao) {
            self.linkDescricao = tituloRelatorio
        }

        // editar descrição
        $http({ method: 'POST', url: uri.renomearLink, params: { linkId: self.linkId, descricao: self.linkDescricao } })
            .then(function (res) {
                self.linkId = res.data.linkId
                self.linkAtual = self.getFullUrl() + 'compartilhar-relatorio/' + res.data.link
            }, function (err) {
                var errmsg

                if (err.data && err.data.Exception) {
                    errmsg = err.data.Exception
                } else if (err.statusText) {
                    errmsg = err.statusText
                } else if (err) {
                    errmsg = err
                } else {
                    errmsg = "Erro não tratado!"
                }
            })
    }

    self.getFullUrl = function () {
        var url = $location.absUrl();
        var arr = url.split("/");
        return arr[0] + '//' + arr[2] + '/' + arr[3] + '/' + arr[4] + '/'
    }

    self.resetarLink = function () {
        self.linkId = undefined
        self.linkAtual = undefined
        self.linkDescricao = undefined
    }

    self.stringifyFiltros = function (filtros) {
        var f = ''

        Object.keys(filtros).forEach(function (key) {
            var value = filtros[key]

            if (typeof value === 'object') {
                if (value.start && value.end) {
                    value = value.start + '|' + value.end
                } else if (value.min && value.max) {
                    value = value.min + '|' + value.max
                } else {
                    value = undefined
                }
            }

            if (value) {
                if (f.length > 0) {
                    f += '&'
                }

                f += key + '=' + value
            }
        })

        return f
    }

    self.salvarLink = function () {
        self.linkDescricao = $('h1').html()

        var f = self.stringifyFiltros(self.filteredBy);

        var config = _.clone(self.userSettings)
        config.groups = _.map(config.groups, self.removerHashKeys)
        config.values = _.map(config.values, self.removerHashKeys)
        config.hide = _.chain(config.columns)
            .filter(function (i) {
                return i.hidden
            })
            .map(function (i) {
                return i.id
            })
            .value()
        delete config.columns

        $http({ method: 'POST', url: uri.compartilharLink, params: { descricao: self.linkDescricao, filtros: f, config: JSON.stringify(config) } })
            .then(function (res) {
                self.linkId = res.data.linkId
                self.linkAtual = self.getFullUrl() + 'compartilhar-relatorio/' + res.data.link
                self.mensagemSucesso('Link salvo com sucesso!')
            }, function (err) {
                self.mensagemErro(err)
            })
    }

    self.carregarLink = function (link) {
        var filtros = {}
        var config = JSON.parse(link.Configuracoes)

        link.Filtros.split('&').forEach(function (i) {
            var kv = i.split('=')

            if (kv.length === 2) {
                var key = kv[0]
                var value = kv[1]
                var multiple = value.split('|')

                if (multiple.length === 2) {
                    if (/[0-9]+\/[0-9]+\/[0-9]+/.exec(multiple[0])) {
                        value = {
                            "start": multiple[0],
                            "end": multiple[1]
                        }
                    } else {
                        value = {
                            "min": multiple[0],
                            "max": multiple[1]
                        }
                    }
                }

                filtros[key] = value
            }
        })

        self.filter = filtros
        self.aplicarFiltros(config, function () {
            self.linkId = link.Id
            self.linkDescricao = link.Descricao
            self.linkAtual = self.getFullUrl() + 'compartilhar-relatorio/' + link.PublicHash
            self.showShareHistory = false;
            self.showShareForm = true;
        })
    }

    self.consultarHistorico = function () {
        $http({ method: 'GET', url: uri.historicoLinks, params: { pagina: self.paginaHistorico, qtde: 5, busca: self.buscaHistorico } })
            .then(function (res) {
                self.historicoLinks = res.data.links
                self.paginaHistorico = res.data.pagina
                self.ultimaPaginaHistorico = res.data.ultimaPagina
            }, function (err) {
                self.mensagemErro(err)
            })
    }

    self.visualizarHistorico = function () {
        self.paginaHistorico = 0
        self.consultarHistorico()
    }

    self.historicoAnteriores = function () {
        self.paginaHistorico--
        self.consultarHistorico()
    }

    self.historicoProximos = function () {
        self.paginaHistorico++
        self.consultarHistorico()
    }

    self.copiarLink = function () {
        $('#permalink').select()
        document.execCommand('copy')
        $('#btnCopiarLink').attr('title', 'Link copiado!')
        $('#btnCopiarLink').tooltip('show')
        $timeout(function () {
            $('#btnCopiarLink').tooltip('destroy')
            $('#btnCopiarLink').attr('title', 'Copiar link')
        }, 2000)
    }

    self.enviarEmail = function () {
        $http({ method: 'POST', url: uri.enviarEmail, params: { linkId: self.linkId, email: self.enderecoEmail } })
            .then(function (res) {
                self.enviandoEmail = false
                self.enderecoEmail = undefined
                self.mensagemSucesso('Email enviado com sucesso!')
            }, function (err) {
                self.mensagemErro(err)
            })
    }

    self.carregarDatePicker = function () {
        $timeout(function () {
            $('.input-daterange').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'pt-BR',
                autoclose: true,
                todayHighlight: true,
                clearBtn: true,
            })
        }, 1000)
    }

    self.aplicarFiltros()

    $timeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);
});

module.filter('ImprimirFiltro', function () {
    return function (input, type) {
        switch (type) {
            case 'date':
            case 'datetime':
                return input.start + ' até ' + input.end
                break

            case 'integer':
                return input.min + ' até ' + input.max
                break

            case 'decimal':
                if (Number.isInteger(input.min) && Number.isInteger(input.max)) {
                    return input.min.toFixed(2) + ' até ' + input.max.toFixed(2)
                } else {
                    return input.min + ' até ' + input.max
                }
                break

            case 'boolean':
                return input == 'true' ? 'Sim' : 'Não'
                break

            default:
                return input
                break
        }
    }
});

module.filter('FormatarTempo', function () {
    return function (input) {
        var regex = {
            date: /\/Date\(([0-9]+)\)\//
        };

        var test = regex.date.exec(input);
        if (test != null) {
            return moment(new Date(parseInt(test[1]))).fromNow();
        } else {
            return input;
        }
    }
});

