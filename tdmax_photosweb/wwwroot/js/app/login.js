﻿var app = angular.module('WebFotos.Login', [
    'WebFotos.Common',
    'WebFotos.Data',
    'ui.bootstrap'
]);

/* Constantes. */
module.value('loginStorageKeyName', 'UserToken');

app.run(function ($rootScope, LoginApi, ServerInfoApi) {
    /* Obtém uma chave no servidor. */
    LoginApi.setKey();

    /* Carrega informações do servidor. */
    ServerInfoApi.getServerInfo().then(function (data) {
        $rootScope.serverInfo = data;
    });
});

/* Controller de autorização. */
app.controller('LoginCtrl', function ($scope, loginStorageKeyName, LoginApi) {
    var shakeError = function () {
        $scope.loading = false;
        $( '#btnAguarde' ).hide();
        $( '#btnEntrar' ).show();
        $( '#login' ).effect( 'shake' );
    };

    $scope.error = null;
    $scope.loading = false;

    $scope.alerts = [];

    $scope.closeAlert = function ( index ) {
        $scope.alerts.splice( index, 1 );
    };

    $scope.submit = function () {
        $scope.alerts = [];

        if ( !$scope.username || $scope.username.length == 0 ) {
            $scope.alerts.push( { type: 'warning', msg: 'Digite seu nome de usuário!' } );
        } else if ( !$scope.password || $scope.password.length == 0 ) {
            $scope.alerts.push( { type: 'warning', msg: 'Digite sua senha!' } );
        } else {
            $( '#btnEntrar' ).hide();
            $( '#btnAguarde' ).show();
            $scope.loading = true;

            var aes = Cryptography.Encrypt( 'user=' + $scope.username.trim() + '|' + 'passwd=' + $scope.password.trim() );

            LoginApi.setAuthentication( aes ).then( function ( response ) {
                if ( !response || response.id == 0 || !response.isValid ) {
                    $scope.alerts.push( { type: 'danger', msg: response.err } );
                    shakeError();
                } else {
                    Storage.Save( loginStorageKeyName, response, Storage.eType.Session );
                    location.href = $scope.basePath;
                }
            } ).catch( function ( response ) {
                $scope.alerts.push( { type: 'danger', msg: 'Falha na autenticação! Por favor tente novamente!' } );
                shakeError();
            } );
        }
    };
});