﻿var module = angular.module('WebFotos.Common', []);

module.factory('Auth', function ($rootScope, $q) {    
    var auth = {};
     
    auth.checkPermissionForView = function(view) {
        if (!view.requiresAuthentication) {
            return true;
        }
         
        return userHasPermissionForView(view);
    };
     
     
    var userHasPermissionForView = function(view){
        if(!auth.isLoggedIn()){
            return false;
        }
         
        if(!view.permissions || !view.permissions.length){
            return true;
        }
         
        return auth.userHasPermission(view.permissions);
    };
     
     
    auth.userHasPermission = function(permissions){
        if(!auth.isLoggedIn()){
            return false;
        }
         
        var found = false;
        angular.forEach(permissions, function(permission, index){
            if (auth.user_permission().indexOf(permission) >= 0){
                found = true;
                return;
            }                        
        });
         
        return found;
    };    
     
    auth.isLoggedIn = function(){
        return $rootScope.user != null;
    };

    auth.user_permission = function () {
        return $rootScope.user.PerfilDescricao;
    };
     
 
    return auth;
});

module.factory('CommonLib', function ($rootScope, $modal) {
    var render = function (frmt, data) {
        return frmt.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = data[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };

    return {
        OpenModal: function (template, controller, data) {
            $modal.open({
                templateUrl: template,
                controller: controller,
                resolve: {
                    modalData: function () {
                        return data;
                    }
                }
            });
        },
        getCadastroThumb: function (cadastroId) {
            var frmt = "{path}/{cadastroId}_thumb.jpg";
            var data = { path: $rootScope.path.cadastro, cadastroId: cadastroId };
            return render(frmt, data);
        },
        getCadastroFoto: function (cadastroId) {
            var frmt = "{path}/{cadastroId}/picture";
            var data = { path: $rootScope.path.cadastro, cadastroId: cadastroId };
            return render(frmt, data);
        },
        getAcessoThumb: function (numSerie, transacao, fotoId) {
            var frmt = "{path}/NumSerie/{numserie}/Transaction/{transacao}/Pictures/{fotoId}/Thumb";
            var data = { path: $rootScope.path.acesso, numserie: numSerie, transacao: transacao, fotoId: fotoId };
            return render(frmt, data);
        },
        getAcessoFoto: function ( numSerie, transacao, fotoId ) {
            var frmt = "{path}/NumSerie/{numserie}/Transaction/{transacao}/Pictures/{fotoId}";
            var data = { path: $rootScope.path.acesso, numserie: numSerie, transacao: transacao, fotoId: fotoId };
            return render( frmt, data );
        },
        getGratuitoThumb: function (equipamento, sequencia, fotoId) {
            var frmt = "{path}/{equipamento}/{sequencia}/gratuito.{equipamento}.{sequencia}.{fotoId}_thumb.jpg"
            var data = { path: $rootScope.path.acesso, equipamento: equipamento, sequencia: sequencia, fotoId: fotoId };
            return render(frmt, data);
        },
        getGratuitoFoto: function (equipamento, sequencia, fotoId) {
            var frmt = "{path}/{equipamento}/{sequencia}/gratuito.{equipamento}.{sequencia}.{fotoId}.jpg"
            console.log(frmt);
            var data = { path: $rootScope.path.acesso, equipamento: equipamento, sequencia: sequencia, fotoId: fotoId };
            return render(frmt, data);
        }
    }
});

module.directive('loadImage', function () {
    return {
        restrict: 'A',
        scope: {
            loadImage: '=',
            loadFallback: '='
        },
        link: function (scope, element) {

            carregarImagem();

            scope.$watch(function (s) {
                return s.loadImage;
            }, function () {
                carregarImagem();
            });

            function carregarImagem() {
                if (!scope.loadImage) {
                    return;
                }

                /* Show spinners while preloading the image. */
                element.removeClass('spinner-show');
                element.addClass('spinner-hide');

                /* Create spinner and center it vertically. */
                var spinner = document.createElement('span');
                spinner.setAttribute('class', 'spinner');
                spinner.setAttribute('style', 'height: ' + element[0].clientHeight + 'px');
                element.parent().append(spinner);

                /* Start preloading the image. */
                var image = document.createElement('img');
                image.setAttribute('src', scope.loadImage);
                image.setAttribute('alt', 'Imagem');

                /* Wait the image to load. */
                image.addEventListener('load', function () {
                    element[0].setAttribute('src', scope.loadImage);
                    element.removeClass('spinner-hide');
                    element.addClass('spinner-show');
                    element.parent().find('span').remove();
                });

                /* Listen to image errors. */
                image.addEventListener('error', function () {
                    console.error('Falhou carregamento da imagem ' + scope.loadImage);
                    element.parent().find('span').remove();

                    /* Create error icon and center it vertically. */
                    var spinner = document.createElement('span');
                    spinner.setAttribute('class', 'image-error');
                    spinner.setAttribute('style', 'height: ' + element[0].clientHeight + 'px');
                    element.parent().append(spinner);

                    if (scope.loadFallback) {
                        console.log('Vou carregar ' + scope.loadFallback);
                        element[0].setAttribute('src', scope.loadFallback);
                        element.removeClass('spinner-hide');
                        element.addClass('spinner-show');
                        element.parent().find('span').remove();
                    }
                });
            }

        }
    }
});

module.directive('focusMe', function ($timeout) {
    return {
        scope: { trigger: '@focusMe' },
        link: function (scope, element) {
            scope.$watch('trigger', function (value) {
                if (value === "true") {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        }
    };
});


