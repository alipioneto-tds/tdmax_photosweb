﻿var module = angular.module('WebFotos.Penalizacoes');


module.controller('PesquisaPenalizacoesCtrl', function ($scope, CadastrosTiposApi, SubTiposApi, PenalizacoesApi) {
    var efetuarBusca = undefined;

    $scope.busca = '';
    $scope.tipo = {};
    $scope.subtipo = {};

    $scope.tipo.id = 0;
    $scope.subtipo.id = 0;

    $scope.loaded = false;
    $scope.list = [];

    $scope.pag = { totalItems: 0, currentPage: 1, maxSize: 5, pageSize: 6 };

    CadastrosTiposApi.getCadastrosTipos().then(function (data) {
        $scope.tipos = data;

        if (data.length > 0) {
            $scope.tipo = data[0];
            $scope.subtipos = [data[0]];
            $scope.subtipo = data[0];
        }
    });

    $scope.getSubtipo = function () {
        SubTiposApi.getSubTipos($scope.tipo.id).then(function (data) {
            $scope.subtipos = data;

            if (data.length > 0) {
                $scope.subtipo = data[0];
            }

            carregar();
        });
    };

    var carregar = function () {
        PenalizacoesApi.getPenalizacoes($scope.busca, $scope.tipo.id, $scope.subtipo.id).then(function (data) {
            $scope.list = data;
            $scope.pag.totalItems = data.length;
            $scope.loaded = true;
        })
    };

    $scope.buscar = function (timeout) {
        if (efetuarBusca) {
            clearTimeout(efetuarBusca);
            efetuarBusca = undefined;
        }

        $scope.loaded = false;
        efetuarBusca = setTimeout(carregar, timeout);
    };

    $scope.removerPenalizacao = function (pen) {
        swal({
            title: "Deseja realmente remover a penalização " + pen.descricao + "?",
            text: "Atenção: Está operação não poderá ser desfeita.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-lg btn-danger",
            confirmButtonText: "Remover",
            cancelButtonText: "Cancelar"
        }).then(function () {
            PenalizacoesApi.setRemoverPenalizacao(pen.penalizacaoid).then(function (data) {
                if (data) {
                    swal("Penalização " + pen.descricao + " removida com sucesso!", "", "success");
                    carregar();
                }
            }).catch(function (textStatus) {
                swal("Erro ao remover penalização!", textStatus.data.ExceptionMessage, "error");
            });
        });
    };

    carregar();
});

module.controller('CadastroPenalizacoesCtrl', function ($scope, $location, $modal, $routeParams, PenalizacoesApi) {
    $scope.model = {};
    $scope.model.TsSelecionados = [];
    $scope.loaded = false;

    if ($routeParams.penalizacaoID != undefined) {
        PenalizacoesApi.getPenalizacao($routeParams.penalizacaoID).then(function (data) {
            $scope.model = data;

            if ($scope.model.Divergencia == 0) {
                $scope.model.Divergencia = null;
                $scope.model.TipoDivergencia = true;
            }
            else {
                $scope.model.TipoDivergencia = false;
            }

            $scope.loaded = true;
        });
    }
    else {
        $scope.model.PenalizacaoID = 0;
        $scope.model.Automatica = true;
        $scope.model.PenalizacoesOrdens = [];
        $scope.model.TiposSubtipos = [];
        $scope.model.TipoDivergencia = true;

        $scope.loaded = true;
    }

    $scope.changeAut = function () {
        var aux = $scope.model.PenalizacoesOrdens.filter(function (t) {
            return t.PenalizacaoTipoID == 2;
        });

        if (aux.length > 0) {
            swal("Atenção!", "Não é permitido alterar para automática, pois existem penalizações de \"Advertência pessoal\" cadastradas.", "warning");
            return;
        }

        $scope.model.Automatica = true;
    };

    $scope.ajuda = function () {
        swal({
            title: "Divergência:",
            html: '<ul><li style="text-align:left;"><strong>Por dia:</strong> Todos acessos divergentes realizados no mesmo dia serão considerados como apenas uma divergência;</li>' +
            '<li style="text-align:left;"><strong>Por qtd:</strong> Ao atingir o número configurado, no campo "quantidade", de acessos divergentes, será considerado uma divergência.</li></ul>',
            type: 'info'
        });
    };

    $scope.submit = function () {
        if ($scope.model.PenalizacoesOrdens.length == 0) {
            swal("Por favor, adicione uma penalização!", "", "warning");
            return;
        }

        if ($scope.model.TiposSubtipos.length == 0) {
            swal("Por favor, adicione um tipo / subtipo!", "", "warning");
            return;
        }

        if ($scope.model.TipoDivergencia) {
            $scope.model.Divergencia = 0;
        }

        PenalizacoesApi.setPenalizacoes($scope.model).then(function (data) {
            if (data) {
                if ($routeParams.penalizacaoID != undefined) {
                    swal("Penalização alterada com sucesso!", "", "success");
                }
                else {
                    swal("Penalização cadastrada com sucesso!", "", "success");
                }

                $location.path('/pesquisa-penalizacoes/');
            }
        }).catch(function (textStatus) {
            swal("Erro ao " + (($routeParams.penalizacaoID != undefined) ? "alterar " : "cadastrar ") + "penalização!", textStatus.data.ExceptionMessage, "error");
        });
    }

    $scope.removerPenalizacaoOrdem = function (index) {
        $scope.model.PenalizacoesOrdens.splice(index, 1);
    };

    $scope.addPenalizacao = function () {
        var count = $scope.model.PenalizacoesOrdens.length;

        if (count > 0) {
            if ($scope.model.PenalizacoesOrdens[count - 1].PenalizacaoTipoID == 5) {
                swal("Não é permitido cadastrar novas penalizações, visto que a última é bloqueio permanente.", "", "warning");
                return;
            }
        }

        var modal = $modal.open({
            templateUrl: 'Content/templates/penalizacoes/add-penalizacao.html',
            controller: 'AddPenalizacaoCtrl',
            resolve: {
                data: function () {
                    return { ordemId: $scope.model.PenalizacoesOrdens.length + 1, automatica: $scope.model.Automatica };
                }
            }
        });

        modal.result.then(function (data) {
            if (data != undefined) {
                $scope.model.PenalizacoesOrdens.push({
                    PenalizacaoTipoID: data.selecionado.id, Descricao: data.selecionado.descricao, QtdDias: data.selecionado.qtdDias || 0
                })
            }
        });
    }

    $scope.addTiposSubtipos = function () {
        var modal = $modal.open({
            templateUrl: 'Content/templates/penalizacoes/add-tiposSubtipos.html',
            controller: 'AddTiposSubtiposCtrl',
            resolve: {
                selecionados: function () {
                    return { penalizacaoId: $scope.model.PenalizacaoID, ts: $scope.model.TiposSubtipos };
                }
            }
        });

        modal.result.then(function (data) {
            if (data != undefined) {
                data.subtipos.forEach(function (t) {
                    $scope.model.TiposSubtipos.push({
                        TipoID: data.tipo.id, Tipo: data.tipo.descricao, SubTipoID: t.id, Subtipo: t.descricao, Selecionado: false
                    })
                });
            }
        });
    };

    $scope.removerTiposSubtipos = function () {
        $scope.model.TiposSubtipos = $scope.model.TiposSubtipos.filter(function (i) {
            return !i.Selecionado;
        });

        $scope.TsSelecionados = [];
    };

    $scope.selecionarTipoSubtipo = function (tipoSubtipo) {
        tipoSubtipo.Selecionado = !tipoSubtipo.Selecionado;

        $scope.TsSelecionados = $scope.model.TiposSubtipos.filter(function (t) {
            return t.Selecionado;
        });
    };

    var carregar = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    setTimeout(carregar, 500);

    $scope.$watch('model.TipoDivergencia', function () {
        setTimeout(carregar, 500);
    });

});


module.controller('AddPenalizacaoCtrl', function ($scope, $modalInstance, data) {
    $scope.ordem = data.ordemId;
    $scope.tipos = [];

    $scope.tipos.push({ id: 1, descricao: "Advertência por email" });
    $scope.tipos.push({ id: 2, descricao: "Advertência pessoal" });
    $scope.tipos.push({ id: 3, descricao: "Bloqueio temporário por período determinado" });
    $scope.tipos.push({ id: 4, descricao: "Bloqueio temporário por período indeterminado" });
    $scope.tipos.push({ id: 5, descricao: "Bloqueio permanente" });
    $scope.tipos.push({ id: 6, descricao: "Multa / Ordem de débito embarcado" });

    $scope.selecionado = {};

    $scope.save = function () {

        if ($scope.selecionado.id == 3) {

            if ($scope.selecionado.qtdDias == undefined) {
                $scope.selecionado.qtdDias = 0;
            }

            var val = $scope.selecionado.qtdDias;
            if (val < 1 || val > 255) {
                return;
            }
        }

        $scope.alerts = [];

        $modalInstance.close({
            selecionado: $scope.selecionado
        });
    }

    $scope.cancel = function () {
        $modalInstance.close();
    }

    $scope.selecionarTipo = function (tipo) {
        if (tipo.id == 6) {
            swal("Funcionalidade indisponível no momento.", tipo.descricao, "warning");
            return
        }

        if (tipo.id == 2 && data.automatica == true) {
            swal("Operação não permitada.", " A penalização \"" + tipo.descricao + "\" não pode ser adicionada em penalizações automáticas.", "warning");
            return
        }

        for (var i = 0; i < $scope.tipos.length; i++) {
            $scope.tipos[i].selecionado = false;
        }

        tipo.selecionado = true;

        $scope.selecionado = tipo;

        setTimeout(carregar, 500);
    };

    var carregar = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

});

module.controller('AddTiposSubtiposCtrl', function ($scope, $modalInstance, selecionados, CadastrosTiposApi, SubTiposApi) {
    $scope.loaded = false;
    $scope.tipos = [];
    $scope.subtipos = [];
    $scope.filterSubtipos = [];

    $scope.tipoSelecionado = {};
    $scope.subTiposSelecionados = [];

    CadastrosTiposApi.getCadastrosTipos(false).then(function (data) {
        $scope.tipos = data;
        $scope.loaded = true;
    });

    var getSubtipos = function () {
        SubTiposApi.getSubTiposPenalizacao($scope.tipoSelecionado.id, selecionados.penalizacaoId).then(function (data) {
            $scope.subtipos = data.filter(function (f) {
                var existe = selecionados.ts.filter(function (e) {
                    return e.TipoID == $scope.tipoSelecionado.id && e.SubTipoID == f.id;
                });

                return (existe.length == 0);
            });

            if ($scope.subtipos.length == 0) {
                swal($scope.tipoSelecionado.descricao, "Nenhum subtipo disponível.", "warning");
            }
        });
    };

    $scope.save = function () {
        $modalInstance.close({
            tipo: $scope.tipoSelecionado,
            subtipos: $scope.subTiposSelecionados
        });
    }

    $scope.cancel = function () {
        $modalInstance.close();
    }

    $scope.voltar = function () {
        $scope.subtipos = [];
    }

    $scope.selecionarTodos = function (subtipos) {
        for (var i = 0; i < subtipos.length; i++) {
            subtipos[i].selecionado = true;
        }

        getSubtiposSelecionados();
    }

    $scope.desmarcarTodos = function (subtipos) {
        for (var i = 0; i < subtipos.length; i++) {
            subtipos[i].selecionado = false;
        }

        getSubtiposSelecionados();
    }

    $scope.selecionarTipo = function (tipo) {
        $scope.tipoSelecionado = tipo;

        getSubtipos();
    };

    var getSubtiposSelecionados = function () {
        $scope.subTiposSelecionados = $scope.subtipos.filter(function (s) {
            return s.selecionado
        });
    };

    $scope.selecionarSubtipo = function (subtipo) {
        subtipo.selecionado = !subtipo.selecionado;
        getSubtiposSelecionados();
    };
});

