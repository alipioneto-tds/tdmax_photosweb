﻿var module = angular.module('WebFotos.AplicarPenalizacao');

module.controller('AplicarPenalizacaoCtrl', function ($scope, $rootScope, $q, $location, $routeParams,
    AplicarPenalizacaoApi, CommonLib, preloader, AcessoInfoApi, ConferenciaState, DashboardApi) {
    /* Declaração das variáveis locais. */
    $scope.transacoes = [];
    $scope.atual = {
        indice: -1,
        data: {}
    };

    $scope.detalhes = {
        acessoInfo: {},
        cadastroInfo: {},
        penalizacao: {}
    };

    $scope.TotalConferidas = 0;

    $scope.imagemAmpliada = {};
    $rootScope.loading = false;

    var indiceAnterior = -1;

    /* Carrega o dashboard. */
    DashboardApi.getDashboardPenalizacao().then(function (dashboard) {
        $scope.dashboard = dashboard;
    });

    /* Consulta o próximo lote. */
    var getProximasTransacoes = function (callback) {
        if ($routeParams.cadastroID != undefined) {
            AplicarPenalizacaoApi.getTransacoesCadastro($routeParams.cadastroID, $routeParams.cameraTransacaoId).then(function (data) {
                if (data && data.transacoes.length == 0) {
                    fimPenCadastro();
                    return;
                }

                $scope.transacoes = data.transacoes;

                updateState();

                if (callback) {
                    callback();
                }

            }).catch(function () {
                getProximasTransacoes(callback);
            });
        }
        else
        {
            AplicarPenalizacaoApi.getTransacoes().then(function (data) {
                if (data && data.transacoes.length == 0) {
                    $location.path('/');
                    swal("Bom trabalho!", "Por enquanto não há mais divergências para serem penalizadas. Volte novamente mais tarde!", "success");
                    return;
                }
                $scope.transacoes = data.transacoes;

                updateState();

                if (callback) {
                    callback();
                }

            }).catch(function () {
                getProximasTransacoes(callback);
            });
        }
    };


    /* Atualiza o estado da conferência para os componentes externos. */
    var updateState = function () {
        $rootScope.navAllowPrevious = $scope.atual.indice > 0;
        $rootScope.navAllowNext = ($scope.atual.data.conferencia && $scope.atual.data.conferencia.status && $scope.atual.indice < ($scope.transacoes.length - 1));
        ConferenciaState.updateFromScopePenalizacao($scope);
    };

    /* Consulta as próximas transações. */
    var getFotos = function (callback) {
        var transacoes = $scope.transacoes;

        for (var i = 0; i < transacoes.length; i++) {

            /* insere a transação no escopo */
            var tran = transacoes[i];

            tran.imagens = {};
            tran.imagens.cadastro = {
                url: CommonLib.getCadastroFoto(transacoes[i].cid), // baixando sempre a foto de cadastro em tamanho original, pois a miniatura está com problemas de otimização, ficando mais do que 100% maior que a original
                loaded: false
            };

            /* monta as urls das imagens de acesso */
            tran.imagens.acesso = [];
            for (var f = 0; f < tran.fts.length; f++) {
                tran.imagens.acesso.push({
                    fotoId: tran.fts[f],
                    url: CommonLib.getAcessoThumb(tran.eq, tran.sq, tran.fts[f]),
                    grd: CommonLib.getAcessoFoto(tran.eq, tran.sq, tran.fts[f]),
                    loaded: false
                });
            }
        }

        $scope.transacoes = [];
        $scope.transacoes = transacoes;

        updateState();

        if (callback) {
            callback();
        }
    };

    /* Navegação segura no índice das transações. */
    var navTo = function (offset) {
        if ($rootScope.loading) {
            console.log('RETORNOU! Estava loading...');
            return;
        }

        var whereto = $scope.atual.indice + offset;

        if (indiceAnterior != -1) {

            /* Define o whereto para retornar ao indice anterior */
            whereto = indiceAnterior;

            /* Reseta o indiceAnterior... */
            indiceAnterior = -1;

            console.log('<<< Retornando ao indice anterior: ' + whereto);
            loadIndice(whereto);
            return;
        }

        console.log('whereto: ' + whereto + ' Total: ' + $scope.transacoes.length);

        if (whereto < 0) {
            return;
        } else if (whereto >= $scope.transacoes.length) {
            /* para as penalizações ja aplicadas, pede apenas uma vez */
            if ($routeParams.cameraTransacaoId > 0) {
                fimPenCadastro();
            }
            else
            {
                $scope.atual.indice = 0;
                /// verificar se tem mais transacoes para verificar (getProximasTransacoes).
                //se sim, carrega as mesmas. se não, lote finalizado.
                getProximasTransacoes(function () {
                    getFotos(function () {
                        navTo(0);
                    });
                });
            }
            
            return;
        }

        loadIndice(whereto);
    };

    var loadIndice = function (indice) {
        console.log('Vai carregar indice: ' + indice + ' transacao: ' + $scope.transacoes[indice].id);
        $scope.atual.indice = indice;
        $scope.atual.data = $scope.transacoes[$scope.atual.indice];

        if ($routeParams.cameraTransacaoId > 0) {
            $scope.atual.data.conferencia = {
                status: 'naoConfere',
                foto: undefined
            };
        }

        $scope.imagemAmpliada = $scope.atual.data.imagens.acesso[0];
        loadDetails();
        preloadImages($scope.atual.indice);
    };

    /* Carrega os detalhes da transação atual. */
    var loadDetails = function () {
        var cadastroId = $scope.atual.data.cid;
        var cameraTransacaoId = $scope.atual.data.id;

        $rootScope.loading = true;

        $q.all([
            AcessoInfoApi.getAcessoInfo(cameraTransacaoId),
            AplicarPenalizacaoApi.getCadastroInfo(cadastroId),
            AplicarPenalizacaoApi.getPenalizacaoAplicada(cameraTransacaoId),
        ])
        .then(function (data) {
            $scope.detalhes.acessoInfo = data[0];
            $scope.detalhes.cadastroInfo = data[1];
            $scope.detalhes.penalizacao = data[2];
            updateState();
            $rootScope.loading = false;
        });
    };

    var isPreloading = false;
    /* Pré-carrega as imagens. */
    var preloadImages = function (idx, skip) {
        if (isPreloading && !skip) {
            return;
        }

        isPreloading = true;

        if ($scope.transacoes.length > idx) {
            var proxima = $scope.transacoes[idx];

            var imagens = [];
            //imagens.push(proxima.imagens.cadastro.url);

            for (var i = 0; i < proxima.imagens.acesso.length; i++) {
                imagens.push(proxima.imagens.acesso[i].url);
            }

            var goToNext = function () {
                preloadImages(idx + 1, true);
            };

            // Inicia o pré-carregamento.
            preloader.preloadImages(imagens).then(
                function handleResolve(imageLocations) {
                    // Terminou de pré-carregar, pode partir para as próximas.
                    goToNext();
                },
                function handleReject(imageLocation) {
                    // Falhou o carregamento de uma das imagens, não há o que fazer. Pode partir para as próximas.
                    goToNext();
                },
                function handleNotify(event) {
                    //
                }
            );
        } else {
            isPreloading = false;
        }
    };

    $scope.ampliarImagem = function (img, event, element) {
        $scope.imagemAmpliada = img;
        /* Não propagar o evento click para outras camadas. */
        if (event.stopPropagation) {
            event.stopPropagation();   // W3C model
            /* O stopPropagation está bloqueando a abertura do modal. */
            $('#imagemAmpliada').modal();
        } else {
            setVerificacao
            event.cancelBubble = true; // IE model
        }
    };

    /* Navega para a transação anterior. */
    $scope.$on('webFotosConfAnterior', function (event) {
        navTo(-1);

    });

    /* Navega para a próxima transação. */
    $scope.$on('webFotosConfProxima', function (event) {
        navTo(1);
    });

    $scope.estaVerificando = false;
    var setPenalizacao = function (status, foto) {
        var recId = 0;

        // Se ainda está dentro do timeout da verificação, impede o usuário de realizar outra ação.
        if ($scope.estaVerificando) {
            return;
        }

        $scope.estaVerificando = true;

        /* Define o recID... */
        switch (status) {
            case 'confere':
                recId = 2;
                break;

            case 'naoConfere':
                recId = 3;
                break;

            default:
                break;
        }

        $scope.atual.data.conferencia = {
            status: status,
            foto: foto
        };

        if (recId != 0) {

            /* CSTDMAXV2-3536 - Passar para a proxima transacao e contabilizar conferencia apenas se conseguir definir o Status da Verificacao, 
                                para evitar que pule  transacoes e prejudique a finalizacao do Lote. */
            AplicarPenalizacaoApi.setAplicarPenalizacao($scope.atual.data.id, recId, foto).then(
                /* Sucesso */
                function (tran) {
                    console.log('tran: ' + tran + ' transacao: ' + $scope.atual.data.id);

                    if (tran != $scope.atual.data.id) {
                        swal("Erro!", "Transacoes diferentes", "error");

                        if ($routeParams.cadastroID != undefined) {
                            $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
                        }
                        else {
                            $location.path('/');
                        }

                        return;
                    }

                    // Contabiliza a conferencia atual
                    $scope.TotalConferidas += 1;

                    //Chama a proxima transacao
                    navTo(1);
                }
            ).catch(function (textStatus) {
                swal("Erro!", textStatus.data.ExceptionMessage, "error");
                if ($routeParams.cadastroID != undefined) {
                    $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
                }
                else {
                    $location.path('/');
                }
                return;
            });
        }

        setTimeout(function () {
            $scope.estaVerificando = false;
        }, 1000);
    }

    /* Define o estado de uma comparação. */
    $rootScope.setVerificacao = function (status, foto) {
        if ($scope.atual.data.conferencia) {
            /* Ja foi conferida antes. */
            var statusAnterior = $scope.atual.data.conferencia.status;

            if (statusAnterior == 'confere') {
                swal("Atenção!", "Esta penalização está cancelada, e não pode ser alterada.", "warning");
            }
            else { /* statusAnterior == 'naoConfere' */
                if (status == 'naoConfere') {
                    swal("Atenção!", "Esta penalização já foi aplicada.", "warning");
                }
                else {
                    swal({
                        title: "Atenção!",
                        text: "Esta penalização já foi aplicada e não pode ser desfeita, será apenas removida do contador de divergências para as proximas penalizações. Deseja continuar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Continuar",
                        cancelButtonText: "Cancelar"
                    }).then(function () {
                        setPenalizacao(status, foto);
                    });
                }
            }
        }
        else
        {
            if (status == 'confere') {
                swal({
                    title: "Deseja realmente cancelar esta penalização?",
                    text: "Atenção: Esta operação não poderá ser desfeita.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-lg btn-danger",
                    confirmButtonText: "Continuar",
                    cancelButtonText: "Cancelar"
                }).then(function () {
                    setPenalizacao(status, foto);
                });
            }
            else
            {
                setPenalizacao(status, foto);
            }
        }
    };

    // Método utilizado apenas quando o usuário realiza alguma ação pelo teclado, na conferência de fotos; 
    // Nele, apenas são tratadas as teclas das fotos (0 a 9).
    $rootScope.setVerificacaoTeclado = function (status, key) {
        // obs.: se caso for PNE com acompanhante ou algum desses, a transação tem 10 fotos, habilitando a verificação com as teclas de 5 a 9
        if (($scope.atual.data.fts.length > 5 && key <= 9) || ($scope.atual.data.fts.length <= 5 && key <= 4)) {
            var foto = $scope.atual.data.fts[key];
            $rootScope.setVerificacao('confere', foto);
        } else {
            setTimeout(function () { }, 1000);
        }
    };

    /// váriavel que manipula se alguma tecla do teclado foi pressionada no ultimo periodo de tempo indicado; 
    /// utilizada na logica que ajuda a evitar que o usuario clique repetidas vezes em uma tecla.
    $scope.keyPressed = false;
    $rootScope.keyup = function ($event, $root) {
        if ($root.controller == 'AplicarPenalizacaoCtrl') {

            if (!$scope.keyPressed) {

                /* keyCodes podem mudar de Browser p/ Browser e de Idioma p/ Idioma, então é melhor realizar a comparação pelo próprio valor da tecla  */
                if (($event.key >= 0 && $event.key <= 9)) {

                    var key = parseInt($event.key); //Vai representar um numero de 0 a 9 que será equivalente ao index da foto no array

                    //Passo key -1 pois key é a tecla digitada e o array (a ser utilizado mais para frente) começa do 0
                    $rootScope.setVerificacaoTeclado('confere', key - 1);

                } else {

                    if ($event.key == "p" || $event.key == "P") { //c / Não Confere
                        $rootScope.setVerificacao('naoConfere', 0);
                    }
                    else if ($event.key == "ArrowLeft") { //seta para esquerda / Transação Anterior
                        if ($rootScope.navAllowPrevious) { // só permite navegar para a comparação anterior se o controle estiver habilitado
                            navTo(-1);
                        }
                    }
                    else if ($event.key == "ArrowRight") { //seta para direita / Próxima Transação
                        if ($rootScope.navAllowNext) { // só permite navegar para a próxima comparação se o controle estiver habilitado
                            navTo(1);
                        }
                    }
                }
                $scope.keyPressed = true;
            }

            setTimeout(function () {
                $scope.keyPressed = false;
            }, 800);
        }
    };

    getProximasTransacoes(function () {
        getFotos(function () {
            /* caso não esteja visualizando nenhuma transação, avança a navegação */
            if ($scope.atual.indice < 0) {
                navTo(1);
            }
        });
    });


    var fimPenCadastro = function () {
        $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);

        if ($routeParams.cameraTransacaoId == 0) {
            swal("Bom trabalho!", "Todas as divergências pendentes foram penalizadas com sucesso.", "success");
        }
        else {
            swal("Bom trabalho!", "Todas as penalizações foram analisadas com sucesso.", "success");
        }

        return;
    };

});