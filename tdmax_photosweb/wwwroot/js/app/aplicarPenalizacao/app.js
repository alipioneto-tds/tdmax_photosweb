﻿var app = angular.module('WebFotos.AplicarPenalizacao', []);


app.run(function ($rootScope, $location, $routeParams) {
    $rootScope.playAplicarPenalizacao = function (controller) {
        if (controller !== 'AplicarPenalizacaoCtrl') {
            $location.path('/aplicarPenalizacao');
        }
        else
        {
            if ($routeParams.cadastroID != undefined) {
                $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroID);
            }
            else
            {
                $location.path('/');
            }
        }
    }
});