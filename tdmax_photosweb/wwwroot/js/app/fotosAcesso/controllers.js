﻿var module = angular.module('WebFotos.FotosAcesso');

module.controller('FotosAcessoCtrl', function ($scope, $routeParams, $modal, FotosAcessoApi, CommonLib) {
    $scope.model = {};
    $scope.loaded = false;
    $scope.imagemAmpliada = {};

    var carregar = function () {
        FotosAcessoApi.getFotosAcesso($routeParams.cameraTransacaoId).then(function (data) {
            $scope.model = data;

            $scope.model.imagens = {};
            $scope.model.imagens.cadastro = {
                url: CommonLib.getCadastroFoto(data.CadastroID)
                //, loaded: false
            };

            data.equipamento = 0;
            data.sequencia = 0;

            /* monta as urls das imagens de acesso */
            $scope.model.imagens.acesso = [];
            for (var f = 0; f < data.Fotos.length; f++) {
                $scope.model.imagens.acesso.push({
                    fotoId: data.Fotos[f],
                    url: CommonLib.getAcessoThumb(data.Equipamento, data.Sequencia, data.Fotos[f]),
                    grd: CommonLib.getAcessoFoto(data.Equipamento, data.Sequencia, data.Fotos[f])
                    //, loaded: false
                });
            }

            $scope.loaded = true;
        }).catch(function (textStatus) {
            swal("Erro!", textStatus.data.ExceptionMessage, "error");
        });
    };

    $scope.ampliarImagem = function (img, event, element) {
        $scope.imagemAmpliada = img;
        /* Não propagar o evento click para outras camadas. */
        if (event.stopPropagation) {
            event.stopPropagation();   // W3C model
            /* O stopPropagation está bloqueando a abertura do modal. */
            $('#imagemAmpliada').modal();
        } else {
            setVerificacao
            event.cancelBubble = true; // IE model
        }
    };

    carregar();
});