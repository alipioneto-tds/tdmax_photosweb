﻿var module = angular.module('WebFotos.AdvertenciaPessoal');

module.controller('AdvPessoalCtrl', function ($scope, $location, AdvertenciaPessoalApi) {
    var efetuarBusca = undefined;

    $scope.busca = '';
    $scope.advStatus = [];

    $scope.status = {};
    $scope.status.id = 0; 

    $scope.loaded = false;
    $scope.list = [];

    $scope.pag = { totalItems: 0, currentPage: 1, pageSize: 10 };

    $scope.advStatus.push({ id: 0, descricao: "Todas" });
    $scope.advStatus.push({ id: 1, descricao: "Pendentes de ligação" });
    $scope.advStatus.push({ id: 2, descricao: "Pendente de advertência" });
    $scope.advStatus.push({ id: 3, descricao: "Desconsiderados" });

    var carregar = function () {
        AdvertenciaPessoalApi.getAdvPessoais($scope.pag.currentPage, $scope.busca, $scope.status.id).then(function (data) {
            $scope.list = data.transacoes;
            $scope.pag.totalItems = data.total;

            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
            }, 500);

            $scope.loaded = true;
        }).catch(function (textStatus) {
            swal("Erro!", textStatus.data.ExceptionMessage, "error");
        });
    };

    $scope.buscar = function (timeout) {
        if (efetuarBusca) {
            clearTimeout(efetuarBusca);
            efetuarBusca = undefined;
        }

        $scope.loaded = false;
        efetuarBusca = setTimeout(carregar, timeout);
    };

    $scope.pageChanged = function () {
        carregar();
    };


    $scope.ligar = function ( i ) {
        AdvertenciaPessoalApi.setAdvPessoal( i.cameraTransacaoID, true, false, false ).then( function ( data ) {
            if ( data ) {
                carregar();
            }
        } ).catch( function ( textStatus ) {
            swal( "Erro!", textStatus.data.ExceptionMessage, "error" );
        } );
    };

    $scope.emitirAdv = function ( i ) {
        swal( {
            title: "Confirma a impressão da advertência para o cadastro: " + i.nome + "?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn btn-lg btn-info",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar"
        } ).then( function () {
            /* Ja emitiu => segunda via */
            if ( i.advStatusID == 3 ) {
                $location.path( '/impressao-adv-pessoal/' + i.cameraTransacaoID + '/0' );
            }
            else {
                AdvertenciaPessoalApi.setAdvPessoal( i.cameraTransacaoID, false, true, false ).then( function ( data ) {
                    if ( data ) {
                        $location.path( '/impressao-adv-pessoal/' + i.cameraTransacaoID + '/0' );
                    }
                } ).catch( function ( textStatus ) {
                    swal( "Erro!", textStatus.data.ExceptionMessage, "error" );
                } );
            }
        } );
    };

    $scope.desconsiderar = function (i) {
        swal({
            title: "Deseja realmente desconsiderar a advertência do cadastro: " + i.nome + "?",
            text: "Atenção: Está operação não poderá ser desfeita.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Desconsiderar",
            cancelButtonText: "Cancelar"
        }).then(function () {
            AdvertenciaPessoalApi.setAdvPessoal(i.cameraTransacaoID, false, false, true).then(function (data) {
                if (data) {
                    swal("Advertência do cadastro: " + i.nome + " desconsiderada com sucesso!", "", "success");
                    carregar();
                }
            }).catch(function (textStatus) {
                swal("Erro!", textStatus.data.ExceptionMessage, "error");
            });
        });
    };

    carregar();
});



module.controller('ImpressaoAdvPessoalCtrl', function ($scope, $routeParams, $templateCache, $location, AdvertenciaPessoalApi) {
    AdvertenciaPessoalApi.getImpressaoAdvPessoal($routeParams.cameraTransacaoId).then(function (data) {
        $scope.list = data;

        var partials = {
            name: 'template',
            content: data.Content
        };

        $templateCache.put(partials.name, partials.content);

        $scope.partials = partials;
    }).catch(function (textStatus) {
        swal("Erro!", textStatus.data.ExceptionMessage, "error");
    });


    $scope.voltar = function () {
        if ($routeParams.cadastroId > 0) {
            $location.path('/exibe-auditar-conferencias/' + $routeParams.cadastroId);
        }
        else {
            $location.path('/advertencia-pessoal/');
        }
    };
});