﻿var module = angular.module('WebFotos.GrupoOperadora');

module.controller('PesquisaGrupoOperadoraCtrl', function (
    $scope, $rootScope
    , LoteApi, GrupoOperadoraApi
) {
    var efetuarBusca = undefined;

    $scope.busca = '';

    $scope.loaded = false;
    $scope.list = [];

    $scope.pag = { totalItems: 0, currentPage: 1, maxSize: 5, pageSize: 10 };

    var carregar = function (pagina) {
        GrupoOperadoraApi.getGruposOperadorasFiltro($scope.busca).then(function (data) {
            $scope.list = data;
            $scope.pag.totalItems = data.length;
            $scope.loaded = true;
        })
    };

    $scope.buscar = function (timeout) {
        if (efetuarBusca) {
            clearTimeout(efetuarBusca);
            efetuarBusca = undefined;
        }

        $scope.loaded = false;
        efetuarBusca = setTimeout(carregar, timeout);
    };

    $scope.buscar();

    $scope.desativarGrupoOperadora = function (gp) {       
        swal({
            title: "Deseja realmente remover o grupo de operadora: " + gp.nomeGrupoOperadora + "?",
            text: "Atenção: Está operação não poderá ser desfeita. Todos os usuários vinculados a este grupo, perderão o acesso ao sistema.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-lg btn-danger",
            confirmButtonText: "Remover",
            cancelButtonText: "Cancelar"
        }).then(function () {
            GrupoOperadoraApi.desativarGrupoOperadora(gp.grupoOperadoraID).then(function (data) {
                if (data) {
                    swal("Grupo de operadora " + gp.nomeGrupoOperadora + " removido com sucesso!", "", "success");
                    carregar(0);
                }
            }).catch(function (textStatus) {
                swal("Erro ao remover grupo de operadora!", textStatus.data.ExceptionMessage, "error");
            });
        });
    };
});

module.controller('CadastroGrupoOperadoraCtrl', function (
    $scope, $rootScope, $routeParams, $window
    , EmpresaApi, UsuarioWebFotosApi, GrupoOperadoraApi
) {
    $scope.busca = {}
    $scope.busca.disp = '';
    $scope.busca.sel = '';

    $scope.model = {};

    $scope.model.Empresas = [];

    $scope.model.GrupoOperadoraID = $routeParams.GrupoOperadoraID || 0;
    $scope.loaded = false;

    EmpresaApi.getEmpresasGrupoOperadora($scope.model.GrupoOperadoraID).then(function (retorno) {
        if (retorno.empresas.length == 0 && $scope.model.GrupoOperadoraID == 0) {
            swal("Não existem empresas operadoras disponíveis!", "Todas já estão vinculadas.", "warning");
            $window.location.hash = '#/pesquisa-grupo-operadora';
            return;
        } else {
            $scope.empDisp.empresasDisponiveis = retorno.empresas;
            $scope.empSel.empresasSelecionadas = retorno.empresasSelecionadas;

            if (retorno.grupo != null) {
                $scope.model.NomeGrupoOperadora = retorno.grupo.NomeGrupoOperadora;
                getUsuariosList(retorno.grupo.UsuarioAdminID);
            }
            else {
                $scope.model.GrupoOperadoraID = 0;
                getUsuariosList(0);
                $scope.model.NomeGrupoOperadora = '';
            }

        }
    }).catch(function () {
        //colocar mensagem
    });

    $scope.combo = {
        usuarioAdminSelecionado: {},
        usuarios: []
    }

    $scope.empDisp = {
        empresasDisponiveisSelecao: [],
        empresasDisponiveis: []
    }

    $scope.empSel = {
        empresasSelecionadas: [],
        empresasSelecionadasSelecao: []
    }

    $scope.atualizarCombo = function () {
        if ($('.selectpicker').hasClass('ng-pristine')) {
            $('.selectpicker').selectpicker('refresh');
        }
    }

    var getUsuariosList = function (usuarioAdminID) {
        UsuarioWebFotosApi.GetUsuariosSemGrupo(usuarioAdminID).then(function (retorno) {
            if (retorno.usuarios.length == 0) {
                swal("Não existem usuários disponíveis!", "", "warning");
                $window.location.hash = '#/pesquisa-grupo-operadora';
                return;
            } else {
                $scope.combo.usuarios = retorno.usuarios;

                setTimeout(function () {
                    if (usuarioAdminID > 0) {
                        $('select[name=selUsuario]').selectpicker('val', usuarioAdminID);

                        for (var i = 0; i < $scope.combo.usuarios.length; i++) {
                            if ($scope.combo.usuarios[i].UsuarioID == usuarioAdminID) {
                                $scope.combo.usuarioAdminSelecionado = $scope.combo.usuarios[i];
                                break;
                            }
                        }
                    }

                    $('.selectpicker').selectpicker('refresh');

                }, 100);

                $scope.loaded = true;
            }
        }).catch(function () {
            //colocar mensagem
        });
    };

    $scope.moveAllRight = function (from) {
        $scope.empDisp.empresasDisponiveis = $scope.empDisp.empresasDisponiveis.filter(function (f) {
            var existe = from.filter(function (e) {
                return e.CadastroID == f.CadastroID;
            });

            return (existe.length == 0);
        });


        angular.forEach(from, function (item) {
            $scope.empSel.empresasSelecionadas.push(item);
        });

        from.length = 0;
        limparBuscas();
    };
    $scope.moveAllLeft = function (from) {
        $scope.empSel.empresasSelecionadas = $scope.empSel.empresasSelecionadas.filter(function (f) {
            var existe = from.filter(function (e) {
                return e.CadastroID == f.CadastroID;
            });

            return (existe.length == 0);
        });


        angular.forEach(from, function (item) {
            $scope.empDisp.empresasDisponiveis.push(item);
        });

        from.length = 0;
        limparBuscas();
    };
    $scope.btnRight = function () {
        //move selected.
        angular.forEach($scope.empDisp.empresasDisponiveisSelecao, function (value, key) {
            this.push(value);
        }, $scope.empSel.empresasSelecionadas);

        //remove the ones that were moved.
        angular.forEach($scope.empDisp.empresasDisponiveisSelecao, function (value, key) {
            for (var i = $scope.empDisp.empresasDisponiveis.length - 1; i >= 0; i--) {
                if ($scope.empDisp.empresasDisponiveis[i].CadastroID == value.CadastroID) {
                    $scope.empDisp.empresasDisponiveis.splice(i, 1);
                }
            }
        });
        $scope.empDisp.empresasDisponiveisSelecao = [];
        limparBuscas();
    };
    $scope.btnLeft = function () {
        //move selected.
        angular.forEach($scope.empSel.empresasSelecionadasSelecao, function (value, key) {
            this.push(value);
        }, $scope.empDisp.empresasDisponiveis);

        //remove the ones that were moved from the source container.
        angular.forEach($scope.empSel.empresasSelecionadasSelecao, function (value, key) {
            for (var i = $scope.empSel.empresasSelecionadas.length - 1; i >= 0; i--) {
                if ($scope.empSel.empresasSelecionadas[i].CadastroID == value.CadastroID) {
                    $scope.empSel.empresasSelecionadas.splice(i, 1);
                }
            }
        });
        $scope.empSel.empresasSelecionadasSelecao = [];
        limparBuscas();
    };

    var limparBuscas = function () {
        $scope.busca.disp = '';
        $scope.busca.sel = '';
    }

    $scope.salvarGrupoOperadora = function () {
        if ($scope.combo.usuarioAdminSelecionado == null) {
            swal("Por favor, selecione um usuário administrador do grupo!", "", "warning");
            return;
        }
        else if ($scope.combo.usuarioAdminSelecionado.UsuarioID == undefined) {
            swal("Por favor, selecione um usuário administrador do grupo!", "", "warning");
            return;
        }

        if ($scope.empSel.empresasSelecionadas.length == 0) {
            swal("Por favor, adicione uma empresa!", "", "warning");
            return;
        }

        $scope.empSel.empresasSelecionadas.forEach(function (entry) {
            $scope.model.Empresas.push(entry.CadastroID);
        });

        $scope.model.UsuarioAdminID = $scope.combo.usuarioAdminSelecionado.UsuarioID;

        GrupoOperadoraApi.salvarGrupoOperadora($scope.model).then(function (data) {
            if (data) {
                if ($scope.model.GrupoOperadoraID != 0)
                {
                    swal("Grupo de operadora alterado com sucesso!", "", "success");
                }
                else
                {
                    swal("Grupo de operadora cadastrado com sucesso!", "", "success");
                }

                $window.location.hash = '#/pesquisa-grupo-operadora';
            }
        }).catch(function () {
            //colocar mensagem
        });
    };
});