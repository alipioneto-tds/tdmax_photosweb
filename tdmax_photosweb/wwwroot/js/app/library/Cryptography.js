﻿function Cryptography() { }

Cryptography.ClientKey = "";

Cryptography.GenerateKey = function () {
    Cryptography.ClientKey = Generate_key();
}
Cryptography.Encrypt = function (text) {
    return Encrypt_Text(text, Str2Hex(Cryptography.ClientKey));
}

Cryptography.Init = function () {
    Cryptography.ClientKey = Storage.Load("crypt.key", Storage.eType.Session);
}