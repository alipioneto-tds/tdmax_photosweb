﻿angular.module('globalization.pt-BR', [])
    .factory('Formatacao', function () {
        return {
            normalizaCentavos: function (value) {
                var num = parseFloat(value),
                    str = num + '';

                if (str.indexOf(",") >= 0)
                    str = parseInt(num * Math.pow(10, str.indexOf(',') - str.length + 4)) + '';
                else if (str.indexOf(".") >= 0)
                    str = parseInt(num * Math.pow(10, str.indexOf('.') - str.length + 4)) + '';
                else
                    str = num + "00";

                return str;
            },
            numero: function (value) {
                var padroes = [
                    { len: 14, regex: /([\d]{3})([\d]{3})([\d]{3})([\d]{2}$)/g, rep: ".$1.$2.$3,$4" },
                    { len: 10, regex: /([\d]{3})([\d]{3})([\d]{2}$)/g, rep: ".$1.$2,$3" },
                    { len: 6, regex: /([\d]{3})([\d]{2}$)/g, rep: ".$1,$2" },
                    { len: 3, regex: /([0-9]{2})$/g, rep: ",$1" }
                ];

                value = parseInt(value.replace(/\D/g, ""), 10) + '';
                for (var i = 0, max = padroes.length; i < max; i += 1) {
                    var padrao = padroes[i];
                    if (value.length >= padrao.len) {
                        return value.replace(padrao.regex, padrao.rep);
                    }
                }
                switch (value.length) {
                    case 2:
                        return "0," + value;
                    case 1:
                        return "0,0" + value;
                }

                return "0,00";
            },
            inteiro: function (value) {
                var padroes = [
                    { len: 12, regex: /([\d]{3})([\d]{3})([\d]{3}$)/g, rep: ".$1.$2.$3" },
                    { len: 8, regex: /([\d]{3})([\d]{3}$)/g, rep: ".$1.$2" },
                    { len: 4, regex: /([\d]{3}$)/g, rep: ".$1" }
                ];

                value = parseInt(value.replace(/\D/g, ""), 10) + '';
                for (var i = 0, max = padroes.length; i < max; i += 1) {
                    var padrao = padroes[i];
                    if (value.length >= padrao.len) {
                        return value.replace(padrao.regex, padrao.rep);
                    }
                }

                return "0";
            },
            data: function (value) {
                var fm = function (v) { v = v + ''; return (v.length == 1) ? "0" + v : v; },
                    toString = function (value) {
                        var date = new Date(parseInt(value.replace(/\/+Date\(([\d+-]+)\)\/+/, '$1')));
                        return (
                            fm(date.getDate()) + '/' +
                            fm(date.getMonth() + 1) + '/' +
                            date.getFullYear()
                        );
                    }

                var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/g;
                if (regex.test(value)) {
                    return value;
                } else if (value !== undefined && value.indexOf("Date") >= 0) {
                    return toString(value);
                }
            }
        }
    })
    .filter('real', function (Formatacao) {
        return function (input) { return "R$ " + Formatacao.numero(Formatacao.normalizaCentavos(input)); };
    })
    .filter('inteiro', function (Formatacao) {
        return function (input) { return Formatacao.inteiro(input); };
    })
    .filter('dataFiltro', function (Formatacao) {
        return function (value) { return Formatacao.data(value); }
    })
    .directive('focusMe', function ($timeout) {
        return {
            scope: { trigger: '@focusMe' },
            link: function (scope, element) {
                scope.$watch('trigger', function (value) {
                    if (value === "true") {
                        $timeout(function () {
                            element[0].focus();
                        });
                    }
                });
            }
        };
    })
    .directive('currency', function ($parse, $filter, Formatacao) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.keyup(function () { $(this).val(Formatacao.numero($(this).val())); });
                ngModel.$formatters.push(function (value) { return Formatacao.numero(Formatacao.normalizaCentavos(value)); });
                ngModel.$parsers.push(function (val) { return (val == '') ? 0 : parseFloat(val.replace(/[\D]/g, '')) / 100; });
            }
        }

    })
    .directive('databr', function ($parse, $filter) {
        var fn = function (num) { return (num.toString().length < 2) ? "0" + num : num; };
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$formatters.push(
                    function (value) {
                        var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/g;
                        if (regex.test(value)) {
                            return value;
                        } else if (value !== undefined && value.indexOf("Date") >= 0) {
                            var dateInt = parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")),
                                date = new Date(dateInt);

                            if (dateInt < 0) {
                                date = new Date(Date.now());
                            }
                            return date;
                        }
                        return "";
                    }
                );
                ngModel.$parsers.push(function (value) { return value; });
            }
        }
    });

$().ready(function () {
    if (jQuery.validator !== undefined) {
        jQuery.extend(jQuery.validator.methods, {
            date: function (value, element) {
                return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d?\d?$/.test(value);
            },
            number: function (value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
            },
            range: function (value, element, param) {
                var temp = value.replace(/\./g, '').replace(/,/g, '.');

                return (parseFloat(temp) >= parseFloat(param[0]) && parseFloat(temp) <= parseFloat(param[1]));
            }
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "Este campo &eacute; requerido.",
            remote: "Por favor, corrija este campo.",
            email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
            url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
            date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
            dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).",
            number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
            digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
            creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
            equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
            accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
            maxlength: jQuery.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
            minlength: jQuery.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
            rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
            range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
            max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
            min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
        });
    }
});
