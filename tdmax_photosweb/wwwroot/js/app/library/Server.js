﻿function Server() {
    ///<summary>Este objeto armazena as informações do servidor e oferece ferramentas para conversões de Url. A carga inicial dos dados do servidor é dada pelo Controller: Server, Action: ServerInfo</summary>
}

Server.Info = {
    ///<summary>Propriedades que armazenam os dados da aplicação servidora. As informações por padrão estão vazias até a carga inicial com o método Init</summary>
    Url: '',
    Aplicacao: '',
    AplicacaoId: -1,
    Versao: '0.0.0.0',
    LoginUrl: '',
    Culture: '',
    Token: ''
}

Server.Content = function (url) {
    ///<summary>Traduz uma Url para uso seguro na aplicação. O caracter que representa a url básica da aplicação é o "~".</summary>
    ///<param name="url" type="String">Texto da url a ser traduzida</param>
    ///<returns type="String">Retorna a URL completa a partir do endereço relativo. Ex.: se a url da aplicação for "http://localhost/Viper" a string "~/Tutorial/Index" será convertida para "http://localhost/Viper/Tutorial/Index"</returns>
    if (!url) {
        url = "~";
    }

    if (Server.Info.Url != "") {
        return url.replace("~", Server.Info.Url);
    } else {
        var local = document.location.href.replace("http://", ""),
            isInt = function (n) { return !isNaN(parseInt(n, 10)) && isFinite(n); },
            segments = local.split("/"),
            infoUrl = ((segments.length > 3 && !isInt(segments[3])) ?
                        "http://" + segments[0] + "/" + segments[1] :
                        "http://" + segments[0]);

        Server.Info.Url = infoUrl;
        return url.replace("~", infoUrl);
    }
}

Server.Action = function (controller, action, params) {
    ///<summary>Retorna o endereço completo para uma action</summary>
    ///<param name="controller" type="String">Nome do controller que contém a action</param>
    ///<param name="action" type="String">Nome da action a ser executada</param>
    ///<param name="params" type="String">lista de parâmetros que devem ser enviados em formato JSON</param>
    ///<returns type="String">Retorna a URL completa de uma action no formato: http://{url da aplicação}/{controller}/{action}?{parametros}</returns>
    var sComplemento = '',
        addComplemento = function (item, value) {
            sComplemento += ((sComplemento == '') ?
                                ('?' + item + '=' + value) :
                                ('&' + item + '=' + value));
        };

    if (params != undefined && params != null) {
        for (var item in params) {
            if ($.isArray(params[item])) {
                for (var index_arr in params[item]) {
                    if (params[item][index_arr] != null) {
                        addComplemento(item, params[item][index_arr]);
                    }
                }
            } else if (params[item] != null) {
                addComplemento(item, params[item]);
            }
        }
    }
    return Server.Content("~/" + controller + '/' + action + sComplemento);
}
Server.WebApi = function (controller, action, params) {
    ///<summary>Retorna o endereço completo para uma webapi</summary>
    ///<param name="controller" type="String">Nome do controllerApi que contém a action</param>
    ///<param name="action" type="String">Nome da action a ser executada</param>
    ///<param name="params" type="String">lista de parâmetros que devem ser enviados em formato JSON</param>
    ///<returns type="String">Retorna a URL completa de uma action no formato: http://{url da aplicação}/api/{controllerApi}/{action}?{parametros}</returns>
    var sComplemento = '',
        addComplemento = function (item, value) {
            sComplemento += ((sComplemento == '') ?
                                ('?' + item + '=' + value) :
                                ('&' + item + '=' + value));
        };

    if (params != undefined && params != null) {
        for (var item in params) {
            if ($.isArray(params[item])) {
                for (var index_arr in params[item]) {
                    if (params[item][index_arr] != null) {
                        addComplemento(item, params[item][index_arr]);
                    }
                }
            } else if (params[item] != null) {
                addComplemento(item, params[item]);
            }
        }
    }
    return Server.Content("~/api/" + controller + '/' + action + sComplemento);
}
Server.MvcRoute = function () {
    ///<summary>Retorna uma estrutura com os dados de roteamento da página atual</summary>
    ///<returns type="String">Estrutura no formato: {controller: "controller atual", action: "action atual", params: "lista de parâmetros" }</returns>
    var retorno = { controller: "", action: "", params: Array() };
    var href = document.location.href;
    var root = Server.Content();
    if (href.length > root.length) {
        var url = href.substr(href.indexOf(root) + root.length + 1);
        var urlArray = url.replace(document.location.search, "").split("/");

        if (urlArray.length >= 2) {
            retorno.controller = urlArray[0];
            retorno.action = urlArray[1];
        } else if (urlArray.length == 1) {
            retorno.controller = urlArray[0];
            retorno.action = "Index";
        }

        for (var i = 2; i < urlArray.length; i++) {
            retorno.params[i - 2] = unescape(urlArray[i]);
        }
    }

    if (document.location.search.length > 0) {
        var pairs = document.location.search.substr(1).split("&");
        for (var i = 0; i < pairs.length; i++) {
            var param = pairs[i].split("=");
            retorno.params[param[0]] = unescape(param[1]);
        }
    }

    return retorno;
}

Server.Init = function () {
    ///<summary>Função que preenche os valores iniciais dos dados armazenados na estrutura "Server.Info"</summary>
    var storeName = "server.info",
        storeType = ((typeof Storage !== "undefined") ? Storage.eType.Session : 0),
        loadFromStore = function () {
            if (typeof Storage !== "undefined") {
                var obj = Storage.Load(storeName, storeType);
                if (obj != null) {
                    Server.Info = obj;
                    return true;
                }
            }
            return false;
        },
        loadFromServer = function () {
            if (typeof ServerInfo !== "undefined") {
                var local = Server.Info,
                    remote = ServerInfo.Global;

                local.Url = remote.Url;
                local.Aplicacao = remote.AppName;
                local.AplicacaoId = remote.AppId;
                local.Versao = remote.Version;
                local.LoginUrl = remote.LoginUrl;
                local.Culture = remote.Lang;

                if (typeof Storage !== "undefined") {
                    Storage.Save(storeName, local, storeType);
                }

                return true;
            }
            return false;
        }

    if (!loadFromStore() && !loadFromServer()) {
        $.getScript(
            Server.Content("~/Server/ServerInfo"), function (data, status) {
                if (status == "success") {
                    loadFromServer();
                }
            }
        ).fail(function (jqxhr) {
            console.log(jqxhr);
        });
    }
}