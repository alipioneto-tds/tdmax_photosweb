﻿function Storage() {
    ///<summary>Objeto que gerencia a gravação, leitura e exclusão de dados armazenados no navegador</summary>
}

Storage.eType = {
    Session: 0,
    Local: 1
}

Storage.Save = function (name, obj, type) {
    ///<signature>
    ///<summary>Grava dados de um objeto (JSON) na "sessionStorage" do navegador.</summary>
    ///<param name="name" type="String">Identificação do dado para posterior recuperação</param>
    ///<param name="obj" type="Object">Objeto que será serializado e armazenado.</param>
    ///</signature>
    ///<signature>
    ///<summary>Grava dados de um objeto (JSON) no navegador.</summary>
    ///<param name="name" type="String">Identificação do dado para posterior recuperação</param>
    ///<param name="obj" type="Object">Objeto que será serializado e armazenado.</param>
    ///<param name="type" type="Number">Tipo de repositório onde o objeto será armazenado (0 = "sessionStorage" ou 1 = "localStorage"). Utilize a estrutura "Storage.eType" para preencher este parâmetro.</param>
    ///</signature>
    if (!type || (type !== Storage.eType.Session && type !== Storage.eType.Local)) {
        type = Storage.eType.Session;
    }

    var json = JSON.stringify(obj);
    if (type == Storage.eType.Local && window.localStorage) {
        window.localStorage[name] = json;
    } else if (window.sessionStorage) {
        window.sessionStorage[name] = json;
    }
}
Storage.Load = function (name, type) {
    ///<signature>
    ///<summary>Retorna um objeto (JSON) armazenado na "sessionStorage" ou "localStorage" do navegador.</summary>
    ///<param name="name" type="String">Identificação do dado a ser recuperado.</param>
    ///<returns type="Object">Objeto deserializado.</returns>
    ///</signature>
    ///<signature>
    ///<summary>Retorna um objeto (JSON) no navegador.</summary>
    ///<param name="name" type="String">Identificação do dado a ser recuperado.</param>
    ///<param name="type" type="Storage.eType">Tipo de repositório onde o objeto foi armazenado ("sessionStorage" ou "localStorage").</param>
    ///<returns type="Object">Objeto deserializado.</returns>
    ///</signature>
    var loadSession = function () {
            if (window.sessionStorage && window.sessionStorage[name]) {
                return JSON.parse(window.sessionStorage[name]);
            }
            return null;
        },
        loadLocal = function () {
            if (window.localStorage && window.localStorage[name]) {
                return JSON.parse(window.localStorage[name]);
            }
            return null;
        }

    switch (type) {
        case Storage.eType.Session:
            return loadSession();
        case Storage.eType.Local:
            return loadLocal();
        default:
            var obj = loadLocal();
            if (obj == null) {
                return loadSession();
            }
            return obj;
    }
}
Storage.Delete = function (name, type) {
    ///<signature>
    ///<summary>Exclui um objeto (JSON) armazenado na "sessionStorage" ou "localStorage" do navegador.</summary>
    ///<param name="name" type="String">Identificação do dado a ser excluído.</param>
    ///<returns type="Boolean">Retorna se o objeto foi ou não excluído.</returns>
    ///</signature>
    ///<signature>
    ///<summary>Exclui um objeto (JSON) no navegador.</summary>
    ///<param name="name" type="String">Identificação do dado a ser excluído.</param>
    ///<param name="type" type="Number">Tipo de repositório onde o objeto foi armazenado (0 = "sessionStorage" ou 1 = "localStorage"). Utilize a estrutura "Storage.eType" para preencher este parâmetro.</param>
    ///<returns type="Boolean">Retorna se o objeto foi ou não excluído.</returns>
    ///</signature>
    var deleteSession = function () {
            if (window.sessionStorage && window.sessionStorage[name]) {
                window.sessionStorage.removeItem(name);
                return true;
            }
            return false;
        },
        deleteLocal = function () {
            if (window.localStorage && window.localStorage[name]) {
                window.localStorage.removeItem(name);
                return true;
            }
            return false;
        }

    switch (type) {
        case Storage.eType.Session:
            return deleteSession();
        case Storage.eType.Local:
            return deleteLocal();
        default:
            var success = deleteLocal();
            if (!success) {
                return deleteSession();
            }
            return success;
    }
}

$().ready(function () {
    //var isCollapsed = function () {
    //        return sessionStorage.getItem("collapsed");
    //    },
    //    tooglebar = function(){
    //        $("#wrapper").toggleClass("toggled");

    //        if ($("#wrapper").hasClass("toggled"))
    //            sessionStorage.setItem("collapsed", true);
    //        else
    //            sessionStorage.setItem("collapsed", false);
    //    };

    //if (isCollapsed() !== "true") {
    //    $("#wrapper").removeClass("toggled");
    //}

    if (sessionStorage.getItem("selectedItem")) {
        var $a = $(".sidebar-nav a[href='" + sessionStorage.getItem("selectedItem") + "']");
        $a.addClass("selected");
        $a.closest(".collapse").collapse("show");
    }

    //$("[mode='sidebar-collapse']").click(function (e) {
    //    e.preventDefault();
    //    tooglebar();
    //});

    //$(".sidebar-nav .panel-group").click(function () {
    //    if ($("#wrapper").hasClass("toggled"))
    //        tooglebar();
    //});

    $(".sidebar-nav a:not([data-toggle])").click(function () {
        sessionStorage.setItem("selectedItem", $(this).attr("href"));
    })
})