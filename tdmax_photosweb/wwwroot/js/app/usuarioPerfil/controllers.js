﻿var module = angular.module('WebFotos.UsuarioPerfil');

module.controller('PesquisaUsuarioPerfilCtrl', function ($scope, $modal, UsuarioWebFotosApi, PerfilApi) {
    $scope.perfis = [];
    $scope.usuariosPerfis = [];

    $scope.pag = { totalItems: 0, currentPage: 1, maxSize: 5, pageSize: 7 };

    var efetuarBusca = undefined;

    $scope.busca = '';
    $scope.perfil = {};
    $scope.perfil.PerfilID = 0;

    $scope.loaded = false;
    $scope.list = [];

    $scope.getPerfis = function () {
        PerfilApi.getPerfis(true).then(function (retorno) {
            if (retorno.perfis == null || retorno.perfis.length == 0) {
                return;
            } else {
                $scope.perfis = retorno.perfis;
            }
        }).catch(function () {
            //colocar mensagem
        });
    };
    $scope.getUsuariosPerfis = function () {
        UsuarioWebFotosApi.getUsuariosPerfis().then(function (retorno) {
            if (retorno.usuariosPerfis == null || retorno.usuariosPerfis.length == 0) {
                return;
            } else {
                $scope.usuariosPerfis = retorno.usuariosPerfis;
                $scope.$data = retorno.usuariosPerfis;

                $scope.params = [];
                $scope.params.data = $scope.$data;

                $scope.totalItems = $scope.usuariosPerfis.length;
            }
        }).catch(function () {
            //colocar mensagem
        });
    };

    $scope.getPerfis();

    var carregar = function () {
        UsuarioWebFotosApi.getUsuariosPerfisFiltro($scope.busca, $scope.perfil.PerfilID).then(function (data) {
            $scope.list = data;
            $scope.pag.totalItems = data.length;
            $scope.loaded = true;
        })
    };

    $scope.buscar = function (timeout) {
        if (efetuarBusca) {
            clearTimeout(efetuarBusca);
            efetuarBusca = undefined;
        }

        $scope.loaded = false;
        efetuarBusca = setTimeout(carregar, timeout);
    };

    $scope.buscar(400);

    $scope.addUsuario = function (usuarioId) {
        var modal = $modal.open({
            templateUrl: 'Content/templates/usuarioPerfil/add-usuario.html',
            controller: 'AddUsuarioCtrl',
            resolve: {
                usuarioID: function () {
                    return usuarioId;
                }
            }
        });

        modal.result.then(function (data) {
            if (data != undefined) {
                carregar(0);
            }
        });
    }

    $scope.desativarUsuario = function (user) {
        var fullName = user.login + ' - ' + user.nome;

        swal({
            title: "Deseja realmente remover o usuário: " + fullName + "?",
            text: "Atenção: Está operação não poderá ser desfeita. Este usuário perderá acesso ao sistema.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-lg btn-danger",
            confirmButtonText: "Remover",
            cancelButtonText: "Cancelar"
        }).then(function () {
            UsuarioWebFotosApi.desativarUsuario(user.usuarioID).then(function (data) {
                if (data) {
                    swal("Usuário " + fullName + " removido com sucesso!", "", "success");
                    carregar();
                }
            }).catch(function (textStatus) {
                swal("Erro ao remover grupo de operadora!", textStatus.data.ExceptionMessage, "error");
            });
        });
    };
});

module.controller('AddUsuarioCtrl', function ($scope, $modalInstance, usuarioID, UsuarioWebFotosApi, PerfilApi) {
    $scope.nome = '';
    $scope.loaded = false;

    $scope.perfis = {
        itens: [],
        perfil: {}
    }

    $scope.combo = {
        usuarioSelecionado: {},
        usuarios: []
    }

    PerfilApi.getPerfis(false).then(function (retorno) {
        if (retorno.perfis.length > 0) {
            $scope.perfis.itens = retorno.perfis;
        }
    }).catch(function () {
        //colocar mensagem
    });

    if (usuarioID == 0) {
        UsuarioWebFotosApi.GetUsuariosSemGrupo(0).then(function (retorno) {
            if (retorno.usuarios == null || retorno.usuarios.length == 0) {
                return;
            } else {
                $scope.combo.usuarios = retorno.usuarios;

                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 100);

                $scope.loaded = true;
            }
        }).catch(function () {
            //colocar mensagem
        });
    }
    else
    {
        UsuarioWebFotosApi.getUsuario(usuarioID).then(function (retorno) {
            if ( retorno ) {
                $scope.perfis.perfil.PerfilID = retorno.perfilID;
                $scope.nome = retorno.login + ' - ' +  retorno.nome;
                $scope.loaded = true;
            }
        }).catch(function () {
            //colocar mensagem
        });
    }

    $scope.atualizarCombo = function () {
        if ($('.selectpicker').hasClass('ng-pristine')) {
            $('.selectpicker').selectpicker('refresh');
        }
    };

    $scope.salvarUsuario = function () {
        var usuario = usuarioID;

        if (usuario == 0) {
            if ($scope.combo.usuarioSelecionado == null) {
                swal("Por favor, selecione um usuário administrador do grupo!", "", "warning");
                return;
            }
            else if ($scope.combo.usuarioSelecionado.UsuarioID == undefined) {
                swal("Por favor, selecione um usuário administrador do grupo!", "", "warning");
                return;
            }
            else
            {
                usuario = $scope.combo.usuarioSelecionado.UsuarioID;
            }
        }

        if ($scope.perfis.perfil.PerfilID == 0) {
            swal("Por favor, selecione um perfil!", "", "warning");
            return;
        }

        UsuarioWebFotosApi.salvarUsuarioPerfil(usuario, $scope.perfis.perfil.PerfilID, 0).then(function (data) {
            if (data) {
                swal("Usuário " + ((usuarioID > 0) ? "alterado" : "adicionado") + " com sucesso!", "", "success");
                $modalInstance.close({
                    sucesso: true
                });
            }
        }).catch(function (textStatus) {
            swal("Erro ao adicionar usuário!", textStatus.data.ExceptionMessage, "error");
        });
    };

    $scope.cancel = function () {
        $modalInstance.close();
    }
});

