﻿var app = angular.module( 'WebFotos', [
    'ngRoute',
    'ui.bootstrap',
    'ui.sortable',
    'ngIdle',
    'WebFotos.Common',
    'WebFotos.Data',
    'WebFotos.Dashboard',
    'WebFotos.Conferencia',
    'WebFotos.FotosAcesso',
    'Transdata.Bootstrap.Validation'
] );

/* Constantes. */
app.value( 'loginStorageKeyName', 'UserToken' );

app.filter( 'startFrom', function () {
    return function ( input, start ) {
        if ( input ) {
            start = +start;
            return input.slice( start );
        }
        return [];
    };
} );

app.filter( 'secondsToDateTime', [function () {
    return function ( seconds ) {
        return new Date( 1970, 0, 1 ).setSeconds( seconds );
    };
}] );

app.config( ['KeepaliveProvider', 'IdleProvider', function ( KeepaliveProvider, IdleProvider ) {
    IdleProvider.idle( 1 );
    IdleProvider.timeout( 1799 );
}] );

/* config app */
app.config( function ( $routeProvider ) {
    $routeProvider
        .when( '/inicio', {
            templateUrl: '/templates//index.html',
            controller: 'IndexCtrl'
        } )
        .when( '/conferencia', {
            templateUrl: '/templates//conferencia.html',
            controller: 'ConferenciaCtrl'
        } )
        .when( '/conferenciaEmbarcado', {
            templateUrl: '/templates//conferenciaEmbarcado.html',
            controller: 'ConferenciaEmbarcadoCtrl'
        } )
        .when( '/conferenciaGratuito', {
            templateUrl: '/templates//conferenciaEmbarcado.html',
            controller: 'ConferenciaGratuitoCtrl'
        } )
        .when( '/foto-acessso/:cameraTransacaoId?', {
            templateUrl: '/templates//fotosAcesso/fotos-acesso.html',
            controller: 'FotosAcessoCtrl',
            requiresAuthentication: true,
            permissions: ['Admin TDMax', 'Admin Grupo Operadora', 'Crítico']
        } )
        .otherwise( {
            redirectTo: '/inicio'
        } );
} );

/* Referências das configurações globais utilizadas. */
module.value( 'habilitarLotePeriodo', {
    processo: "21", item: "1", indice: "16"
} );

module.value( 'sistemaSeccionado', {
    processo: "2", item: "2", indice: "5"
} );

module.value( 'habilitarTirarFotosEmbarcado', {
    processo: "12", item: "1", indice: "7"
} );

module.value( 'cidade', {
    processo: "2", item: "2", indice: "1"
} );

module.value( 'habilitarAcessoGratuitoBotoeira', {
    processo: "12", item: "2", indice: "5"
} );

var getConfigs = async function ( $rootScope, ConfiguracaoApi, habilitarLotePeriodo, sistemaSeccionado,
    habilitarTirarFotosEmbarcado, habilitarAcessoGratuitoBotoeira, cidade ) {
    try {
        var lot = await ConfiguracaoApi.getConfiguracao( habilitarLotePeriodo );
        $rootScope.config.habilitarLotePeriodo = lot.data;

        var sec = await ConfiguracaoApi.getConfiguracao( sistemaSeccionado );
        $rootScope.config.sistemaSeccionado = sec.data;

        var emb = await ConfiguracaoApi.getConfiguracao( habilitarTirarFotosEmbarcado );
        //$rootScope.config.habilitarTirarFotosEmbarcado = emb & 0x10000; // mascara do habilitarTirarFotosEmbarcado: 0x00010000
        $rootScope.config.habilitarTirarFotosEmbarcado = true;

        var gra = await ConfiguracaoApi.getConfiguracao( habilitarAcessoGratuitoBotoeira );
        //$rootScope.config.habilitarAcessoGratuitoBotoeira = gra & 0x1000; // mascara do habilitarAcessoGratuitoBotoeira: 0x00001000
        $rootScope.config.habilitarAcessoGratuitoBotoeira = true;

        var cid = await ConfiguracaoApi.getConfiguracao( cidade );
        $rootScope.config.cidade = cid.data;
    } catch ( textStatus ) {
        $rootScope.showErrorMessage( textStatus, null );
    }
};

/* app events */
app.run( function ( $rootScope, $location, CommonLib, ServerInfoApi, LoginApi, ConfiguracaoApi, loginStorageKeyName, habilitarLotePeriodo
    , sistemaSeccionado, habilitarTirarFotosEmbarcado, habilitarAcessoGratuitoBotoeira, Auth, EmpresaApi, Idle, cidade ) {
    /* Inicializa as configurações globais. */
    if ( !$rootScope.config ) {
        $rootScope.config = {};
    }
    $rootScope.config.habilitarLotePeriodo = false;
    $rootScope.config.sistemaSeccionado = false;
    $rootScope.config.habilitarTirarFotosEmbarcado = false;
    $rootScope.config.habilitarAcessoGratuitoBotoeira = false;
    $rootScope.config.cidade = "";

    $rootScope.showErrorMessage = function ( textStatus, onClose ) {
        var msg = textStatus.data || ( textStatus.config != undefined
            ? "Erro ao acessar " + textStatus.config.url : ( textStatus.message || "Erro desconhecido" ) );

        swal( {
            title: "Erro!",
            html: msg,
            type: "error",
            customClass: msg.length > 1000 ? 'swal-wide' : '',
            onClose: function () {
                if ( onClose != null ) {
                    onClose();
                }
            }
        } );
    };

    /* Consulta as configurações globais. */
    getConfigs( $rootScope, ConfiguracaoApi, habilitarLotePeriodo, sistemaSeccionado,
        habilitarTirarFotosEmbarcado, habilitarAcessoGratuitoBotoeira, cidade );

    $rootScope.$on( '$routeChangeSuccess', function ( ev, data ) {
        $rootScope.controller = data.controller;
    } );

    Idle.watch();

    $rootScope.$on( 'IdleTimeout', function () {
        Storage.Delete( loginStorageKeyName );
        location.href = './Account/Login';
    } );

    $rootScope.$on( "$routeChangeStart", function ( evt, to, from ) {
        // requires authorization?
        if ( !Auth.checkPermissionForView( to ) ) {
            evt.preventDefault();
            $location.path( "/inicio" );
        }

        if ( to.authorize === true ) {
            to.resolve = to.resolve || {};
            if ( !to.resolve.authorizationResolver ) {
                // inject resolver
                to.resolve.authorizationResolver = ["authService", function ( authService ) {
                    return authService.authorize();
                }];
            }
        }
    } );

    $rootScope.$on( "$routeChangeError", function ( evt, to, from, error ) {
        if ( error instanceof AuthorizationError ) {
            // redirect to login with original path we'll be returning back to
            $location
                .path( "/inicio" )
                .search( "returnTo", to.originalPath );
        }
    } );

    $rootScope.$on( "$locationChangeStart", function ( event, next, current ) {
        for ( var i in window.routes ) {
            if ( next.indexOf( i ) != -1 ) {
                if ( window.routes[i].requireLogin && !SessionService.getUserAuthenticated() ) {
                    alert( "You need to be authenticated to see this page!" );
                    event.preventDefault();
                }
            }
        }
    } );

    /* Exibe a modal de configurações do sistema. */
    $rootScope.abrirConfiguracoes = function () {
        CommonLib.OpenModal( '/templates//configuracoes.html', 'ConfiguracoesCtrl' );
    };

    /* Exibe a modal de alteração de senha. */
    $rootScope.alterarSenha = function () {
        CommonLib.OpenModal( '/templates//alterar-senha.html', 'AlterarSenhaCtrl' );
    };

    /* Carrega os dados de autenticação do session storage. */
    var user = Storage.Load( loginStorageKeyName, Storage.eType.Session );
    if ( !user ) {
        location.href = './Account/Login';
    } else {
        $rootScope.user = user;
    }

    if ( $rootScope.user.PerfilDescricao == 'Admin TDMax' ) {
        EmpresaApi.getEmpresasGrupoOperadora( 0 ).then( function ( retorno ) {
            if ( retorno.empresas != null ) {
                if ( retorno.empresas.length > 0 ) {
                    swal( "Existem empresas operadoras não vinculadas a grupos!", "Atenção: não serão gerados lotes de fotos para empresas operadoras não vinculadas, por favor, vincule-as ", "warning" );
                }
            }
        } ).catch( function () {
            //colocar mensagem
        } );
    }

    /* Realiza logoff do aplicativo. */
    $rootScope.logoff = function () {
        Storage.Delete( loginStorageKeyName );
        location.href = './Account/Login';
    };

    /* Confirma direto no servidor se o usuário está autenticado. */
    LoginApi.getAuthentication()
        .then( function ( data ) {
            if ( data && data.id !== 0 ) {
                $rootScope.user = data;
                Storage.Save( loginStorageKeyName, data, Storage.eType.Session );
            } else {
                $rootScope.logoff();
            }
        } )
        .catch( function ( textStatus ) {
            $rootScope.showErrorMessage( textStatus, $rootScope.logoff );
        } );

    /* Carrega informações do servidor. */
    ServerInfoApi.getServerInfo().then( function ( data ) {
        $rootScope.serverInfo = data;
    } );
} );

/* index controller */
app.controller( 'IndexCtrl', function ( $scope, $rootScope, LoteApi ) {
    $rootScope.limparFiltroPeriodo = function () {
        $rootScope.dataIni = "";
        $rootScope.dataFim = "";
    };
} );

/* change password controller */
app.controller( 'AlterarSenhaCtrl', function ( $scope, $modalInstance, LoginApi ) {
    $scope.alerts = [];

    $scope.save = function () {
        $scope.alerts = [];

        LoginApi.changePassword( $scope.oldPassword, $scope.newPassword, $scope.confirmation ).then( function ( data ) {
            if ( data.sucesso ) {
                $modalInstance.close();
            } else {
                $scope.alerts.push( { type: 'warning', msg: data.msg } );
            }
        } );
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
} );

