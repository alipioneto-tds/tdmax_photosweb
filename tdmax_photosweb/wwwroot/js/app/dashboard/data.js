﻿var module = angular.module('WebFotos.Dashboard');

module.service( 'DashboardService', function ( ConfiguracaoApi, DashboardDefaults ) {

    this.getDashboard = async function ( args ) {
        var response = await ConfiguracaoApi.getConfiguracao( args );

        var model = "";

        if ( response.status == 204 ) {
            model = DashboardDefaults.defaultDashboard;
        } else {
            model = JSON.parse( response.data.value );
        }

        return model;
    };

    this.setDashboard = async function ( index, model ) {
        var response = await BaseApi.postApiController( ['Settings', index.toString()], {
            dashboard: JSON.stringify( model )
        } );
            
        return response.data;
    };
});