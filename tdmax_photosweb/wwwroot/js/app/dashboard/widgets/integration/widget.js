﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Integration', ['adf.provider', 'WebFotos.Conferencia'])
  .config(function (dashboardProvider) {
      dashboardProvider
          .widget('integration', {
            title: 'Acesso',
            description: 'Informações sobre o turno do acesso.',
            templateUrl: '{widgetsPath}/integration/view.html',
            controller: 'widgetIntegrationController',
            controllerAs: 'integration',
            config: {
                exibir: {
                    equipamento: true,
                    sequencia: true,
                    numSerie: true,
                    transacao: true,
                    dataAcesso: true,
                    motorista: true,
                    cobrador: true,
                    linha: true,
                    prefixo: true
                }
            },
            edit: {
                templateUrl: '{widgetsPath}/integration/edit.html'
            }
        });
  })
    .controller('widgetIntegrationController', function ($scope, $interval, config, ConferenciaState) {
      $scope.config = config;
      $scope.data = null;

      function getData() {
          $scope.data = ConferenciaState.data;

          if ( config.exibir.dataAcesso && $scope.data.integration.dataAcesso ) {
              var d = new moment( $scope.data.integration.dataAcesso );
              $scope.dataAcesso = d.format( 'DD/MM/YYYY HH:mm:ss' );
          }
      }

      getData();

      // refresh every second
      var promise = $interval(getData, 100);

      // cancel interval on scope destroy
      $scope.$on('$destroy', function () {
          $interval.cancel(promise);
      });
  });