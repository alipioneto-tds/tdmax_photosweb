﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Cadastro', ['adf.provider', 'WebFotos.Conferencia'])
    .config(function (dashboardProvider) {
        dashboardProvider
            .widget('cadastro', {
                title: 'Cadastro',
                description: 'Informações sobre o cadastro atual.',
                templateUrl: '{widgetsPath}/cadastro/view.html',
                controller: 'widgetCadastroController',
                controllerAs: 'cadastro',
                config: {
                    exibir: {
                        equipamento: false,
                        sequencia: false,
                        turnoId: true,
                        motorista: true,
                        cobrador: false,
                        linha: true,
                        prefixo: true
                    }
                },
                edit: {
                    templateUrl: '{widgetsPath}/cadastro/edit.html'
                }
            });
    })
    .controller('widgetCadastroController', function ($scope, $interval, config, ConferenciaState) {
        $scope.config = config;
        $scope.data = null;

        function getData() {
            $scope.data = ConferenciaState.data;
        }

        getData();

        // refresh every second
        var promise = $interval(getData, 100);

        // cancel interval on scope destroy
        $scope.$on('$destroy', function () {
            $interval.cancel(promise);
        });
    });