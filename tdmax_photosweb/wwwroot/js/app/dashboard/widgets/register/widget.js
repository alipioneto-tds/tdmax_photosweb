﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Register', ['adf.provider', 'WebFotos.Conferencia'])
  .config(function (dashboardProvider) {
      dashboardProvider
          .widget('register', {
            title: 'Cadastro',
            description: 'Informações sobre o cadastro do usuário.',
            templateUrl: '{widgetsPath}/register/view.html',
            controller: 'widgetRegisterController',
              controllerAs: 'register',
            config: {
                exibir: {
                    cadastroId: true,
                    transacaoId: true,
                    cadastroTipo: true,
                    subTipo: true,
                    acessosMeiaViagem: true
                }
            },
            edit: {
                templateUrl: '{widgetsPath}/register/edit.html'
            }
        });
  })
    .controller('widgetRegisterController', function ($scope, $interval, config, ConferenciaState) {
      $scope.config = config;
      $scope.data = null;

      function getData() {
          $scope.data = ConferenciaState.data;
      }

      getData();

      // refresh every second
      var promise = $interval(getData, 100);

      // cancel interval on scope destroy
      $scope.$on('$destroy', function () {
          $interval.cancel(promise);
      });
  });