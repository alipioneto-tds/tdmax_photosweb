﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Relogio', ['adf.provider'])
  .config(function (dashboardProvider) {
      dashboardProvider
        .widget('relogio', {
            title: 'Relógio',
            description: 'Exibe data e hora do sistema.',
            templateUrl: '{widgetsPath}/relogio/view.html',
            controller: 'widgetClockController',
            controllerAs: 'relogio',
            config: {
                timePattern: 'HH:mm:ss',
                datePattern: 'DD/MM/YYYY'
            },
            edit: {
                templateUrl: '{widgetsPath}/relogio/edit.html'
            }
        });
  })
  .controller('widgetClockController', function ($scope, $interval, config) {
      var clock = this;

      function setDateAndTime() {
          var d = new moment();
          clock.hora = d.format(config.timePattern);
          clock.data = d.format(config.datePattern);
      }

      setDateAndTime();

      // refresh every second
      var promise = $interval(setDateAndTime, 1000);

      // cancel interval on scope destroy
      $scope.$on('$destroy', function () {
          $interval.cancel(promise);
      });
  });