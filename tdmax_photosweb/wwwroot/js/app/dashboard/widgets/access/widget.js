﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Access', ['adf.provider', 'WebFotos.Conferencia'])
  .config(function (dashboardProvider) {
      dashboardProvider
          .widget('access', {
            title: 'Acesso',
            description: 'Informações sobre o turno do acesso.',
            templateUrl: '{widgetsPath}/access/view.html',
            controller: 'widgetAccessController',
            controllerAs: 'access',
            config: {
                exibir: {
                    equipamento: true,
                    sequencia: true,
                    turnoId: true,
                    dataAcesso: true,
                    motorista: true,
                    cobrador: true,
                    linha: true,
                    prefixo: true
                }
            },
            edit: {
                templateUrl: '{widgetsPath}/access/edit.html'
            }
        });
  })
    .controller('widgetAccessController', function ($scope, $interval, config, ConferenciaState) {
      $scope.config = config;
      $scope.data = null;

      function getData() {
          $scope.data = ConferenciaState.data;

          if ( config.exibir.dataAcesso && $scope.data.access.dataAcesso ) {
              var d = new moment( $scope.data.access.dataAcesso );
              $scope.dataAcesso = d.format( 'DD/MM/YYYY HH:mm:ss' );
          }
      }

      getData();

      // refresh every second
      var promise = $interval(getData, 100);

      // cancel interval on scope destroy
      $scope.$on('$destroy', function () {
          $interval.cancel(promise);
      });
  });