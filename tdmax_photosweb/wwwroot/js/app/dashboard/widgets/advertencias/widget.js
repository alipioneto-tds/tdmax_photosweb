﻿'use strict';

angular.module('WebFotos.Dashboard.Widget.Advertencias', ['adf.provider', 'WebFotos.Conferencia'])
  .config(function (dashboardProvider) {
      dashboardProvider
        .widget('advertencias', {
            title: 'Advertências',
            description: 'Exibe o resumo das advertências recebidas pelo usuário da transação que está sendo comparada.',
            templateUrl: '{widgetsPath}/advertencias/view.html',
            controller: 'widgetAdvertenciasController',
            controllerAs: 'advertencias',
            config: {
                exibir: {
                    notificadas: true,
                    canceladas: true,
                    total: true
                }
            },
            edit: {
                templateUrl: '{widgetsPath}/advertencias/edit.html'
            }
        });
  })
  .controller('widgetAdvertenciasController', function ($scope, $interval, config, ConferenciaState) {
      $scope.config = config;
      $scope.data = null;

      function getData() {
          $scope.data = ConferenciaState.data;
      }

      getData();

      // refresh every second
      var promise = $interval(getData, 100);

      // cancel interval on scope destroy
      $scope.$on('$destroy', function () {
          $interval.cancel(promise);
      });
  });