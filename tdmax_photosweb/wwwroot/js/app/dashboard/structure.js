﻿var module = angular.module('WebFotos.Dashboard.Estrutura', ['adf']);

/* setup structures */
module.config(function (dashboardProvider) {
    dashboardProvider
        .widgetsPath('/js/app/dashboard/widgets/');

    dashboardProvider
        .structure('4-4-4', {
            rows: [{
                columns: [{
                    styleClass: 'col-xs-12 col-sm-6 col-md-4',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-6 col-md-4',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-6 col-md-4',
                    widgets: []
                }]
            }]
        });

    dashboardProvider
        .structure('3-3-3-3', {
            rows: [{
                columns: [{
                    styleClass: 'col-xs-12 col-sm-4 col-md-3',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4 col-md-3',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4 col-md-3',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4 col-md-3',
                    widgets: []
                }]
            }]
        });

    dashboardProvider
        .structure('2-4-3-3', {
            rows: [{
                columns: [{
                    styleClass: 'col-xs-12 col-sm-2',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-3',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-3',
                    widgets: []
                }]
            }]
        });

    dashboardProvider
        .structure('2-2-4-4', {
            rows: [{
                columns: [{
                    styleClass: 'col-xs-12 col-sm-2',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-2',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4',
                    widgets: []
                }, {
                    styleClass: 'col-xs-12 col-sm-4',
                    widgets: []
                }]
            }]
        });
});