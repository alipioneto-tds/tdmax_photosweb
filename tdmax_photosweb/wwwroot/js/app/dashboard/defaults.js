﻿var module = angular.module('WebFotos.Dashboard');

module.service( 'DashboardDefaults', function () {
    /* Formato padrão do painel de informações. */
    this.defaultDashboard = {
        structure: "4-4-4",
        rows: [
            {
                columns: [
                    {
                        styleClass: "col-xs-12 col-sm-6 col-md-4",
                        widgets: [
                            {
                                type: "register",
                                config: {
                                    exibir: {
                                        cadastroId: true,
                                        numSerie: true,
                                        cadastroTipo: true,
                                        subTipo: true,
                                        acessosMeiaViagem: false
                                    }
                                },
                                title: "Cadastro",
                                wid: 9
                            }
                        ],
                        cid: 8
                    },
                    {
                        styleClass: "col-xs-12 col-sm-6 col-md-4",
                        widgets: [
                            {
                                type: "access",
                                config: {
                                    exibir: {
                                        equipamento: false,
                                        sequencia: false,
                                        turnoId: true,
                                        motorista: true,
                                        cobrador: true,
                                        linha: true,
                                        prefixo: true,
                                        transacao: true,
                                        dataAcesso: true
                                    }
                                },
                                title: "Acesso",
                                wid: 4
                            }
                        ],
                        cid: 7
                    },
                    {
                        styleClass: "col-xs-12 col-sm-6 col-md-4",
                        widgets: [
                            {
                                type: "relogio",
                                config: {
                                    timePattern: "HH:mm:ss",
                                    datePattern: "DD/MM/YYYY"
                                },
                                title: "Relógio",
                                wid: 5
                            }
                        ],
                        cid: 6
                    }
                ]
            }
        ],
        title: "Dashboard"
    };
} );