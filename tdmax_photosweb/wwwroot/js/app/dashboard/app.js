﻿var module = angular.module('WebFotos.Dashboard', [
    'adf',
    'WebFotos.Data',
    'WebFotos.Dashboard.Estrutura',
    'WebFotos.Dashboard.Widget.Advertencias',
    'WebFotos.Dashboard.Widget.Cadastro',
    'WebFotos.Dashboard.Widget.Register',
    'WebFotos.Dashboard.Widget.Relogio',
    'WebFotos.Dashboard.Widget.Access',
    'WebFotos.Dashboard.Widget.Integration'
]);

/* adf config */
module.config(function (dashboardProvider) {
    dashboardProvider
        .widgetsPath('/js/app/dashboard/widgets/');
});

module.run(function ($rootScope) {
    $rootScope.personalizarPainel = function () {
        $rootScope.$broadcast( 'adfToggleEditMode' );
    };
});