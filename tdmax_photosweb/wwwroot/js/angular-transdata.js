angular.module('ngTransdata', [])

.value('ngTransdataTemplatePath', '/ngTransdata/templates/');

Array.prototype.contains = function (obj) {
	var i = this.length;
	while (i--) {
		if (this[i] === obj) {
			return true;
		}
	}
	return false;
}

angular.module('ngTransdata')

.directive('tdForm', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {
			action: '@action',
			submit: '@submit',
			icon: '@icon'
		},
		transclude: 'true',
		link: function ($scope, $element, $attr) {
			$scope.model = {};
			$scope.model.submit = $scope.submit || 'Enviar';
		},
		templateUrl: ngTransdataTemplatePath + 'form.html'
	}
}]);

angular.module('ngTransdata')

.directive('tdInput', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {
			nome: '@nome',
			descricao: '@descricao',
			validacao: '@validacao'
		},
		link: function ($scope, $element, $attr) {
			// http://huei90.github.io/angular-validation/
			// http://www.ng-newsletter.com/posts/validations.html
		},
		templateUrl: ngTransdataTemplatePath + 'input.html'
	}
}]);

angular.module('ngTransdata')

.directive('tdLogin', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {
			produto: '@produto',
			versao: '@versao',
			config: '='
		},
		link: function ($scope, $element, $attr) {
			/* Objetos expostos à view. */
			$scope.action = {};
			$scope.model = {};
			$scope.form = {
				entrar: {}
			};

			/* Helpers para manipulação do formulário. */
			var setLoginLoading = function (busy, text) {
				$scope.form.entrar.busy = busy;
				$scope.form.entrar.text = text || 'Entrar';
			}

			setLoginLoading(false);

			/* Nomes de produtos válidos. É aqui que você deve incluir um novo nome de produto.
			 * Certifique-se de ter criado logotipos para o novo produto no diretório /src/images/. */
			var produtos = ['transdata', 'tdmax'];
			var prod = $scope.produto || ($scope.config ? $scope.config.produto : null);

			/* Valida a presença do atributo produto. */
			if (!prod) {
				console.error("<td-login/>: Você deve definir o atributo 'produto'!");
				prod = 'transdata';
			}

			/* Valida o valor do atributo produto. */
			if (!produtos.contains(prod)) {
				console.error("<td-login/>: Nome de produto inválido! Corrija o atributo 'produto' ou crie o novo produto no ngTransdata!");
				prod = 'transdata';
			}

			/* Valida a configuração das funções. */
			if (!$scope.config.validateLogin) {
				console.error('<td-login/>: Função de login não definida!');
			}

			/* Passa as configurações para a view. */
			$scope.model.produto = prod;
			$scope.model.logo = prod;
			$scope.model.versao = $scope.versao || $scope.config.versao || '-';

			/* Passa o ano atual para o copyright. */
			$scope.model.ano = new Date().getYear() + 1900;

			/* Procedimento de login. */
			$scope.action.login = function () {
				setLoginLoading(true, 'Carregando');

				$scope.config.validateLogin($scope.form.username, $scope.form.password, $scope.form.remember, function (isValid) {
					setLoginLoading(false);
					if (!isValid) {
						jQuery('#td-login').effect('shake');
						localStorage.removeItem("ngTransdata");
					} else {
						if($scope.form.remember){
							var strData = btoa(JSON.stringify({ 
								lg:{
									username: $scope.form.username,
									password: $scope.form.password,
									remember: $scope.form.remember
								} 
							}));
							localStorage.setItem("ngTransdata", strData);
						}
						setLoginLoading(true, 'Redirecionando');
					}
				});
			};
			
			/* Recupera informações do usuário */
			var cacheData = localStorage.getItem("ngTransdata");
			if(cacheData && cacheData != null){
				try{
					jQuery.extend($scope.form, JSON.parse(atob(cacheData)).lg);	
				}
				catch(error){
					$scope.form = {}	
				}
			}
			
			$scope.changeFocus = function($event, formId){
				if($event.charCode == 13){
					jQuery("[form-id='"+ formId +"']").focus();					
				}
			}
		},
		templateUrl: ngTransdataTemplatePath + 'login.html'
	}
}]);

angular.module('ngTransdata')

.directive('tdNavbar', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {
			produto: '@produto',
			appname: '@appname',
			active: '=',
			navigation: '='
		},
		link: function ($scope, $element, $attr) {
			/* Objetos expostos à view. */
			$scope.model = {};

			/* Nomes de produtos válidos. É aqui que você deve incluir um novo nome de produto.
			 * Certifique-se de ter criado logotipos para o novo produto no diretório /src/images/. */
			var produtos = ['transdata', 'tdmax'];
			var prod = $scope.produto || ($scope.config ? $scope.config.produto : null);

			/* Valida a presença do atributo produto. */
			if (!prod) {
				console.error("<td-navbar/>: Você deve definir o atributo 'produto'!");
				prod = 'transdata';
			}

			/* Valida o valor do atributo produto. */
			if (!produtos.contains(prod)) {
				console.error("<td-navbar/>: Nome de produto inválido! Corrija o atributo 'produto' ou crie o novo produto no ngTransdata!");
				prod = 'transdata';
			}

			/* Valida o nome da aplicação. */
			if (!$scope.appname) {
				console.error("<td-navbar/>: Você deve definir o atributo 'appname'!");
			}

			/* Passa as configurações para a view. */
			$scope.model.appname = $scope.appname || 'Hello World';
			$scope.model.produto = prod;
			$scope.model.logo = prod;
		},
		templateUrl: ngTransdataTemplatePath + 'navbar.html'
	}
}]);

angular.module('ngTransdata')

.directive('tdUploadArquivo', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {},
		link: function ($scope, $element, $attr) {
			//
		},
		templateUrl: ngTransdataTemplatePath + 'uploadArquivo.html'
	}
}]);

angular.module('ngTransdata')

.directive('tdUploadImagem', ["ngTransdataTemplatePath", function (ngTransdataTemplatePath) {
	return {
		scope: {},
		link: function ($scope, $element, $attr) {
			//
		},
		templateUrl: ngTransdataTemplatePath + 'uploadImagem.html'
	}
}]);

/* ===========================================================
 * Bootstrap: fileinput.js v3.1.3
 * http://jasny.github.com/bootstrap/javascript/#fileinput
 * ===========================================================
 * Copyright 2012-2014 Arnold Daniels
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

+function ($) { "use strict";

  var isIE = window.navigator.appName == 'Microsoft Internet Explorer'

  // FILEUPLOAD PUBLIC CLASS DEFINITION
  // =================================

  var Fileinput = function (element, options) {
    this.$element = $(element)

    this.$input = this.$element.find(':file')
    if (this.$input.length === 0) return

    this.name = this.$input.attr('name') || options.name

    this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]')
    if (this.$hidden.length === 0) {
      this.$hidden = $('<input type="hidden">').insertBefore(this.$input)
    }

    this.$preview = this.$element.find('.fileinput-preview')
    var height = this.$preview.css('height')
    if (this.$preview.css('display') !== 'inline' && height !== '0px' && height !== 'none') {
      this.$preview.css('line-height', height)
    }

    this.original = {
      exists: this.$element.hasClass('fileinput-exists'),
      preview: this.$preview.html(),
      hiddenVal: this.$hidden.val()
    }

    this.listen()
  }

  Fileinput.prototype.listen = function() {
    this.$input.on('change.bs.fileinput', $.proxy(this.change, this))
    $(this.$input[0].form).on('reset.bs.fileinput', $.proxy(this.reset, this))

    this.$element.find('[data-trigger="fileinput"]').on('click.bs.fileinput', $.proxy(this.trigger, this))
    this.$element.find('[data-dismiss="fileinput"]').on('click.bs.fileinput', $.proxy(this.clear, this))
  },

  Fileinput.prototype.change = function(e) {
    var files = e.target.files === undefined ? (e.target && e.target.value ? [{ name: e.target.value.replace(/^.+\\/, '')}] : []) : e.target.files

    e.stopPropagation()

    if (files.length === 0) {
      this.clear()
      this.$element.trigger('clear.bs.fileinput')
      return
    }

    this.$hidden.val('')
    this.$hidden.attr('name', '')
    this.$input.attr('name', this.name)

    var file = files[0]

    if (this.$preview.length > 0 && (typeof file.type !== "undefined" ? file.type.match(/^image\/(gif|png|jpeg)$/) : file.name.match(/\.(gif|png|jpe?g)$/i)) && typeof FileReader !== "undefined") {
      var reader = new FileReader()
      var preview = this.$preview
      var element = this.$element

      reader.onload = function(re) {
        var $img = $('<img>')
        $img[0].src = re.target.result
        files[0].result = re.target.result

        element.find('.fileinput-filename').text(file.name)

        // if parent has max-height, using `(max-)height: 100%` on child doesn't take padding and border into account
        if (preview.css('max-height') != 'none') $img.css('max-height', parseInt(preview.css('max-height'), 10) - parseInt(preview.css('padding-top'), 10) - parseInt(preview.css('padding-bottom'), 10)  - parseInt(preview.css('border-top'), 10) - parseInt(preview.css('border-bottom'), 10))

        preview.html($img)
        element.addClass('fileinput-exists').removeClass('fileinput-new')

        element.trigger('change.bs.fileinput', files)
      }

      reader.readAsDataURL(file)
    } else {
      this.$element.find('.fileinput-filename').text(file.name)
      this.$preview.text(file.name)

      this.$element.addClass('fileinput-exists').removeClass('fileinput-new')

      this.$element.trigger('change.bs.fileinput')
    }
  },

  Fileinput.prototype.clear = function(e) {
    if (e) e.preventDefault()

    this.$hidden.val('')
    this.$hidden.attr('name', this.name)
    this.$input.attr('name', '')

    //ie8+ doesn't support changing the value of input with type=file so clone instead
    if (isIE) {
      var inputClone = this.$input.clone(true);
      this.$input.after(inputClone);
      this.$input.remove();
      this.$input = inputClone;
    } else {
      this.$input.val('')
    }

    this.$preview.html('')
    this.$element.find('.fileinput-filename').text('')
    this.$element.addClass('fileinput-new').removeClass('fileinput-exists')

    if (e !== undefined) {
      this.$input.trigger('change')
      this.$element.trigger('clear.bs.fileinput')
    }
  },

  Fileinput.prototype.reset = function() {
    this.clear()

    this.$hidden.val(this.original.hiddenVal)
    this.$preview.html(this.original.preview)
    this.$element.find('.fileinput-filename').text('')

    if (this.original.exists) this.$element.addClass('fileinput-exists').removeClass('fileinput-new')
     else this.$element.addClass('fileinput-new').removeClass('fileinput-exists')

    this.$element.trigger('reset.bs.fileinput')
  },

  Fileinput.prototype.trigger = function(e) {
    this.$input.trigger('click')
    e.preventDefault()
  }


  // FILEUPLOAD PLUGIN DEFINITION
  // ===========================

  var old = $.fn.fileinput

  $.fn.fileinput = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('bs.fileinput')
      if (!data) $this.data('bs.fileinput', (data = new Fileinput(this, options)))
      if (typeof options == 'string') data[options]()
    })
  }

  $.fn.fileinput.Constructor = Fileinput


  // FILEINPUT NO CONFLICT
  // ====================

  $.fn.fileinput.noConflict = function () {
    $.fn.fileinput = old
    return this
  }


  // FILEUPLOAD DATA-API
  // ==================

  $(document).on('click.fileinput.data-api', '[data-provides="fileinput"]', function (e) {
    var $this = $(this)
    if ($this.data('bs.fileinput')) return
    $this.fileinput($this.data())

    var $target = $(e.target).closest('[data-dismiss="fileinput"],[data-trigger="fileinput"]');
    if ($target.length > 0) {
      e.preventDefault()
      $target.trigger('click.bs.fileinput')
    }
  })

}(window.jQuery);

/* ===========================================================
 * Bootstrap: inputmask.js v3.1.0
 * http://jasny.github.io/bootstrap/javascript/#inputmask
 *
 * Based on Masked Input plugin by Josh Bush (digitalbush.com)
 * ===========================================================
 * Copyright 2012-2014 Arnold Daniels
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

+function ($) { "use strict";

  var isIphone = (window.orientation !== undefined)
  var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1
  var isIE = window.navigator.appName == 'Microsoft Internet Explorer'

  // INPUTMASK PUBLIC CLASS DEFINITION
  // =================================

  var Inputmask = function (element, options) {
    if (isAndroid) return // No support because caret positioning doesn't work on Android

    this.$element = $(element)
    this.options = $.extend({}, Inputmask.DEFAULTS, options)
    this.mask = String(this.options.mask)

    this.init()
    this.listen()

    this.checkVal() //Perform initial check for existing values
  }

  Inputmask.DEFAULTS = {
    mask: "",
    placeholder: "_",
    definitions: {
      '9': "[0-9]",
      'a': "[A-Za-z]",
      'w': "[A-Za-z0-9]",
      'X': "[Xx0-9]",
      '*': "."
    }
  }

  Inputmask.prototype.init = function() {
    var defs = this.options.definitions
    var len = this.mask.length

    this.tests = []
    this.partialPosition = this.mask.length
    this.firstNonMaskPos = null

    $.each(this.mask.split(""), $.proxy(function(i, c) {
      if (c == '?') {
        len--
        this.partialPosition = i
      } else if (defs[c]) {
        this.tests.push(new RegExp(defs[c]))
        if (this.firstNonMaskPos === null)
          this.firstNonMaskPos =  this.tests.length - 1
      } else {
        this.tests.push(null)
      }
    }, this))

    this.buffer = $.map(this.mask.split(""), $.proxy(function(c, i) {
      if (c != '?') return defs[c] ? this.options.placeholder : c
    }, this))

    this.focusText = this.$element.val()

    this.$element.data("rawMaskFn", $.proxy(function() {
      return $.map(this.buffer, function(c, i) {
        return this.tests[i] && c != this.options.placeholder ? c : null
      }).join('')
    }, this))
  }

  Inputmask.prototype.listen = function() {
    if (this.$element.attr("readonly")) return

    var pasteEventName = (isIE ? 'paste' : 'input') + ".bs.inputmask"

    this.$element
      .on("unmask.bs.inputmask", $.proxy(this.unmask, this))

      .on("focus.bs.inputmask", $.proxy(this.focusEvent, this))
      .on("blur.bs.inputmask", $.proxy(this.blurEvent, this))

      .on("keydown.bs.inputmask", $.proxy(this.keydownEvent, this))
      .on("keypress.bs.inputmask", $.proxy(this.keypressEvent, this))

      .on(pasteEventName, $.proxy(this.pasteEvent, this))
  }

  //Helper Function for Caret positioning
  Inputmask.prototype.caret = function(begin, end) {
    if (this.$element.length === 0) return
    if (typeof begin == 'number') {
      end = (typeof end == 'number') ? end : begin
      return this.$element.each(function() {
        if (this.setSelectionRange) {
          this.setSelectionRange(begin, end)
        } else if (this.createTextRange) {
          var range = this.createTextRange()
          range.collapse(true)
          range.moveEnd('character', end)
          range.moveStart('character', begin)
          range.select()
        }
      })
    } else {
      if (this.$element[0].setSelectionRange) {
        begin = this.$element[0].selectionStart
        end = this.$element[0].selectionEnd
      } else if (document.selection && document.selection.createRange) {
        var range = document.selection.createRange()
        begin = 0 - range.duplicate().moveStart('character', -100000)
        end = begin + range.text.length
      }
      return {
        begin: begin,
        end: end
      }
    }
  }

  Inputmask.prototype.seekNext = function(pos) {
    var len = this.mask.length
    while (++pos <= len && !this.tests[pos]);

    return pos
  }

  Inputmask.prototype.seekPrev = function(pos) {
    while (--pos >= 0 && !this.tests[pos]);

    return pos
  }

  Inputmask.prototype.shiftL = function(begin,end) {
    var len = this.mask.length

    if (begin < 0) return

    for (var i = begin, j = this.seekNext(end); i < len; i++) {
      if (this.tests[i]) {
        if (j < len && this.tests[i].test(this.buffer[j])) {
          this.buffer[i] = this.buffer[j]
          this.buffer[j] = this.options.placeholder
        } else
          break
        j = this.seekNext(j)
      }
    }
    this.writeBuffer()
    this.caret(Math.max(this.firstNonMaskPos, begin))
  }

  Inputmask.prototype.shiftR = function(pos) {
    var len = this.mask.length

    for (var i = pos, c = this.options.placeholder; i < len; i++) {
      if (this.tests[i]) {
        var j = this.seekNext(i)
        var t = this.buffer[i]
        this.buffer[i] = c
        if (j < len && this.tests[j].test(t))
          c = t
        else
          break
      }
    }
  },

  Inputmask.prototype.unmask = function() {
    this.$element
      .unbind(".bs.inputmask")
      .removeData("bs.inputmask")
  }

  Inputmask.prototype.focusEvent = function() {
    this.focusText = this.$element.val()
    var len = this.mask.length
    var pos = this.checkVal()
    this.writeBuffer()

    var that = this
    var moveCaret = function() {
      if (pos == len)
        that.caret(0, pos)
      else
        that.caret(pos)
    }

    moveCaret()
    setTimeout(moveCaret, 50)
  }

  Inputmask.prototype.blurEvent = function() {
    this.checkVal()
    if (this.$element.val() !== this.focusText) {
      this.$element.trigger('change')
      this.$element.trigger('input')
    }
  }

  Inputmask.prototype.keydownEvent = function(e) {
    var k = e.which

    //backspace, delete, and escape get special treatment
    if (k == 8 || k == 46 || (isIphone && k == 127)) {
      var pos = this.caret(),
      begin = pos.begin,
      end = pos.end

      if (end - begin === 0) {
        begin = k != 46 ? this.seekPrev(begin) : (end = this.seekNext(begin - 1))
        end = k == 46 ? this.seekNext(end) : end
      }
      this.clearBuffer(begin, end)
      this.shiftL(begin, end - 1)

      return false
    } else if (k == 27) {//escape
      this.$element.val(this.focusText)
      this.caret(0, this.checkVal())
      return false
    }
  }

  Inputmask.prototype.keypressEvent = function(e) {
    var len = this.mask.length

    var k = e.which,
    pos = this.caret()

    if (e.ctrlKey || e.altKey || e.metaKey || k < 32)  {//Ignore
      return true
    } else if (k) {
      if (pos.end - pos.begin !== 0) {
        this.clearBuffer(pos.begin, pos.end)
        this.shiftL(pos.begin, pos.end - 1)
      }

      var p = this.seekNext(pos.begin - 1)
      if (p < len) {
        var c = String.fromCharCode(k)
        if (this.tests[p].test(c)) {
          this.shiftR(p)
          this.buffer[p] = c
          this.writeBuffer()
          var next = this.seekNext(p)
          this.caret(next)
        }
      }
      return false
    }
  }

  Inputmask.prototype.pasteEvent = function() {
    var that = this

    setTimeout(function() {
      that.caret(that.checkVal(true))
    }, 0)
  }

  Inputmask.prototype.clearBuffer = function(start, end) {
    var len = this.mask.length

    for (var i = start; i < end && i < len; i++) {
      if (this.tests[i])
        this.buffer[i] = this.options.placeholder
    }
  }

  Inputmask.prototype.writeBuffer = function() {
    return this.$element.val(this.buffer.join('')).val()
  }

  Inputmask.prototype.checkVal = function(allow) {
    var len = this.mask.length
    //try to place characters where they belong
    var test = this.$element.val()
    var lastMatch = -1

    for (var i = 0, pos = 0; i < len; i++) {
      if (this.tests[i]) {
        this.buffer[i] = this.options.placeholder
        while (pos++ < test.length) {
          var c = test.charAt(pos - 1)
          if (this.tests[i].test(c)) {
            this.buffer[i] = c
            lastMatch = i
            break
          }
        }
        if (pos > test.length)
          break
      } else if (this.buffer[i] == test.charAt(pos) && i != this.partialPosition) {
        pos++
        lastMatch = i
      }
    }
    if (!allow && lastMatch + 1 < this.partialPosition) {
      this.$element.val("")
      this.clearBuffer(0, len)
    } else if (allow || lastMatch + 1 >= this.partialPosition) {
      this.writeBuffer()
      if (!allow) this.$element.val(this.$element.val().substring(0, lastMatch + 1))
    }
    return (this.partialPosition ? i : this.firstNonMaskPos)
  }


  // INPUTMASK PLUGIN DEFINITION
  // ===========================

  var old = $.fn.inputmask

  $.fn.inputmask = function (options) {
    return this.each(function () {
      var $this = $(this)
      var data = $this.data('bs.inputmask')

      if (!data) $this.data('bs.inputmask', (data = new Inputmask(this, options)))
    })
  }

  $.fn.inputmask.Constructor = Inputmask


  // INPUTMASK NO CONFLICT
  // ====================

  $.fn.inputmask.noConflict = function () {
    $.fn.inputmask = old
    return this
  }


  // INPUTMASK DATA-API
  // ==================

  $(document).on('focus.bs.inputmask.data-api', '[data-mask]', function (e) {
    var $this = $(this)
    if ($this.data('bs.inputmask')) return
    $this.inputmask($this.data())
  })

}(window.jQuery);

angular.module("ngTransdata").run(["$templateCache", function($templateCache) {$templateCache.put("/ngTransdata/templates/form.html","<form method=POST action={{action}} class=form-horizontal><ng-transclude></ng-transclude><div class=form-group><div class=\"col-md-offset-3 col-md-7\"><button class=\"btn btn-primary\"><i ng-if=icon class=\"fa fa-{{icon}} fa-fw\"></i> {{model.submit}}</button></div></div></form>");
$templateCache.put("/ngTransdata/templates/input.html","<div class=form-group><label for={{nome}} class=\"control-label col-md-3\">{{descricao}}</label><div class=col-md-7><input type=text class=form-control name={{nome}} id={{nome}}></div></div>");
$templateCache.put("/ngTransdata/templates/login.html","<div id=td-login class=row><div class=\"td-login col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\"><header><div class=hole></div><p><img ng-src={{config.contentUrl}}/images/{{model.logo}}/logo-default-x50.png alt=\"Transdata Smart\"></p></header><content><div class=form-group><input form-id=username ng-model=form.username type=text class=form-control placeholder=\"Nome de usuário\" ng-keypress=\"changeFocus($event, \'password\')\"></div><div class=form-group><input form-id=password ng-model=form.password type=password class=form-control placeholder=Senha ng-keypress=\"changeFocus($event, \'submit\')\"></div><div class=form-group><div class=pull-right><div class=checkbox><label><input form-id=remember ng-model=form.remember type=checkbox>Gravar meus dados de login</label></div></div></div><div class=\"form-group pull-right\"><button form-id=submit type=button class=\"btn btn-primary\" ng-click=action.login() ng-disabled=form.entrar.busy><i ng-if=!form.entrar.busy class=\"fa fa-sign-in fa-fw\"></i> <img ng-if=form.entrar.busy src={{config.contentUrl}}/images/spinners/td-login.gif alt=Carregando> <span ng-bind=form.entrar.text>Entrar</span></button></div></content><footer><p class=copyright>Transdata Smart &copy; {{model.ano}} - Todos os direitos reservados</p><p class=version>{{model.versao}}</p></footer></div></div>");
$templateCache.put("/ngTransdata/templates/navbar.html","<nav class=\"navbar navbar-default navbar-fixed-top\"><div><div class=\"navbar-header pull-right\"><button type=button class=\"navbar-toggle collapsed\" data-toggle=collapse data-target=#navbar aria-expanded=false aria-controls=navbar><span class=sr-only>Alterar navegação</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button> <a class=navbar-brand href=#><header ng-bind=model.appname></header><svg width=10 height=50><defs><clippath id=cut-off-bottom><rect x=0 y=0 width=10 height=50></rect></clippath></defs><circle cx=52 cy=25 r=50 clip-path=url(#cut-off-bottom) stroke=#999 stroke-width=1 fill=#fff></circle></svg><img src=/images/{{model.logo}}/logo-default-x40.png alt={{model.appname}}></a></div><div id=navbar class=\"navbar-collapse collapse\"><ul class=\"nav navbar-nav\"><li ng-repeat=\"item in navigation\" ng-class=\"{ \'active\': item.id === active }\"><a ng-href=\"{{ item.route }}\"><i class=\"{{ item.icon ? \'fa fa-\' + item.icon + \' fa-fw\' : null }}\"></i> <span ng-bind=item.label></span></a></li></ul></div></div></nav>");
$templateCache.put("/ngTransdata/templates/uploadArquivo.html","<div class=\"fileinput fileinput-new input-group\" data-provides=fileinput><div class=form-control data-trigger=fileinput><i class=\"glyphicon glyphicon-file fileinput-exists\"></i> <span class=fileinput-filename></span></div><span class=\"input-group-addon btn btn-default btn-file\"><span class=fileinput-new>Selecionar Arquivo</span><span class=fileinput-exists>Alterar</span><input type=file name=...></span> <a href=# class=\"input-group-addon btn btn-default fileinput-exists\" data-dismiss=fileinput>Remover</a></div>");
$templateCache.put("/ngTransdata/templates/uploadImagem.html","<div class=\"fileinput fileinput-new\" data-provides=fileinput><div class=\"fileinput-preview thumbnail\" data-trigger=fileinput style=\"width: 200px; height: 150px;\"></div><div><span class=\"btn btn-default btn-file\"><span class=fileinput-new>Selecionar Imagem</span><span class=fileinput-exists>Alterar</span><input type=file name=...></span> <a href=# class=\"btn btn-default fileinput-exists\" data-dismiss=fileinput>Remover</a></div></div>");}]);