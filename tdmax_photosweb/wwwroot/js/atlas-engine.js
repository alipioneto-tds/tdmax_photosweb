(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function atoa (a, n) { return Array.prototype.slice.call(a, n); }

},{}],2:[function(require,module,exports){
'use strict';

var ticky = require('ticky');

module.exports = function debounce (fn, args, ctx) {
  if (!fn) { return; }
  ticky(function run () {
    fn.apply(ctx || null, args || []);
  });
};

},{"ticky":11}],3:[function(require,module,exports){
'use strict';

var atoa = require('atoa');
var debounce = require('./debounce');

module.exports = function emitter (thing, options) {
  var opts = options || {};
  var evt = {};
  if (thing === undefined) { thing = {}; }
  thing.on = function (type, fn) {
    if (!evt[type]) {
      evt[type] = [fn];
    } else {
      evt[type].push(fn);
    }
    return thing;
  };
  thing.once = function (type, fn) {
    fn._once = true; // thing.off(fn) still works!
    thing.on(type, fn);
    return thing;
  };
  thing.off = function (type, fn) {
    var c = arguments.length;
    if (c === 1) {
      delete evt[type];
    } else if (c === 0) {
      evt = {};
    } else {
      var et = evt[type];
      if (!et) { return thing; }
      et.splice(et.indexOf(fn), 1);
    }
    return thing;
  };
  thing.emit = function () {
    var args = atoa(arguments);
    return thing.emitterSnapshot(args.shift()).apply(this, args);
  };
  thing.emitterSnapshot = function (type) {
    var et = (evt[type] || []).slice(0);
    return function () {
      var args = atoa(arguments);
      var ctx = this || thing;
      if (type === 'error' && opts.throws !== false && !et.length) { throw args.length === 1 ? args[0] : args; }
      et.forEach(function emitter (listen) {
        if (opts.async) { debounce(listen, args, ctx); } else { listen.apply(ctx, args); }
        if (listen._once) { thing.off(type, listen); }
      });
      return thing;
    };
  };
  return thing;
};

},{"./debounce":2,"atoa":1}],4:[function(require,module,exports){
(function (global){
'use strict';

var customEvent = require('custom-event');
var eventmap = require('./eventmap');
var doc = global.document;
var addEvent = addEventEasy;
var removeEvent = removeEventEasy;
var hardCache = [];

if (!global.addEventListener) {
  addEvent = addEventHard;
  removeEvent = removeEventHard;
}

module.exports = {
  add: addEvent,
  remove: removeEvent,
  fabricate: fabricateEvent
};

function addEventEasy (el, type, fn, capturing) {
  return el.addEventListener(type, fn, capturing);
}

function addEventHard (el, type, fn) {
  return el.attachEvent('on' + type, wrap(el, type, fn));
}

function removeEventEasy (el, type, fn, capturing) {
  return el.removeEventListener(type, fn, capturing);
}

function removeEventHard (el, type, fn) {
  var listener = unwrap(el, type, fn);
  if (listener) {
    return el.detachEvent('on' + type, listener);
  }
}

function fabricateEvent (el, type, model) {
  var e = eventmap.indexOf(type) === -1 ? makeCustomEvent() : makeClassicEvent();
  if (el.dispatchEvent) {
    el.dispatchEvent(e);
  } else {
    el.fireEvent('on' + type, e);
  }
  function makeClassicEvent () {
    var e;
    if (doc.createEvent) {
      e = doc.createEvent('Event');
      e.initEvent(type, true, true);
    } else if (doc.createEventObject) {
      e = doc.createEventObject();
    }
    return e;
  }
  function makeCustomEvent () {
    return new customEvent(type, { detail: model });
  }
}

function wrapperFactory (el, type, fn) {
  return function wrapper (originalEvent) {
    var e = originalEvent || global.event;
    e.target = e.target || e.srcElement;
    e.preventDefault = e.preventDefault || function preventDefault () { e.returnValue = false; };
    e.stopPropagation = e.stopPropagation || function stopPropagation () { e.cancelBubble = true; };
    e.which = e.which || e.keyCode;
    fn.call(el, e);
  };
}

function wrap (el, type, fn) {
  var wrapper = unwrap(el, type, fn) || wrapperFactory(el, type, fn);
  hardCache.push({
    wrapper: wrapper,
    element: el,
    type: type,
    fn: fn
  });
  return wrapper;
}

function unwrap (el, type, fn) {
  var i = find(el, type, fn);
  if (i) {
    var wrapper = hardCache[i].wrapper;
    hardCache.splice(i, 1); // free up a tad of memory
    return wrapper;
  }
}

function find (el, type, fn) {
  var i, item;
  for (i = 0; i < hardCache.length; i++) {
    item = hardCache[i];
    if (item.element === el && item.type === type && item.fn === fn) {
      return i;
    }
  }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./eventmap":5,"custom-event":6}],5:[function(require,module,exports){
(function (global){
'use strict';

var eventmap = [];
var eventname = '';
var ron = /^on/;

for (eventname in global) {
  if (ron.test(eventname)) {
    eventmap.push(eventname.slice(2));
  }
}

module.exports = eventmap;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],6:[function(require,module,exports){
(function (global){

var NativeCustomEvent = global.CustomEvent;

function useNative () {
  try {
    var p = new NativeCustomEvent('cat', { detail: { foo: 'bar' } });
    return  'cat' === p.type && 'bar' === p.detail.foo;
  } catch (e) {
  }
  return false;
}

/**
 * Cross-browser `CustomEvent` constructor.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent.CustomEvent
 *
 * @public
 */

module.exports = useNative() ? NativeCustomEvent :

// IE >= 9
'function' === typeof document.createEvent ? function CustomEvent (type, params) {
  var e = document.createEvent('CustomEvent');
  if (params) {
    e.initCustomEvent(type, params.bubbles, params.cancelable, params.detail);
  } else {
    e.initCustomEvent(type, false, false, void 0);
  }
  return e;
} :

// IE <= 8
function CustomEvent (type, params) {
  var e = document.createEventObject();
  e.type = type;
  if (params) {
    e.bubbles = Boolean(params.bubbles);
    e.cancelable = Boolean(params.cancelable);
    e.detail = params.detail;
  } else {
    e.bubbles = false;
    e.cancelable = false;
    e.detail = void 0;
  }
  return e;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],7:[function(require,module,exports){
'use strict';

var cache = {};
var start = '(?:^|\\s)';
var end = '(?:\\s|$)';

function lookupClass (className) {
  var cached = cache[className];
  if (cached) {
    cached.lastIndex = 0;
  } else {
    cache[className] = cached = new RegExp(start + className + end, 'g');
  }
  return cached;
}

function addClass (el, className) {
  var current = el.className;
  if (!current.length) {
    el.className = className;
  } else if (!lookupClass(className).test(current)) {
    el.className += ' ' + className;
  }
}

function rmClass (el, className) {
  el.className = el.className.replace(lookupClass(className), ' ').trim();
}

module.exports = {
  add: addClass,
  rm: rmClass
};

},{}],8:[function(require,module,exports){
(function (global){
'use strict';

var emitter = require('contra/emitter');
var crossvent = require('crossvent');
var classes = require('./classes');
var doc = document;
var documentElement = doc.documentElement;

function dragula (initialContainers, options) {
  var len = arguments.length;
  if (len === 1 && Array.isArray(initialContainers) === false) {
    options = initialContainers;
    initialContainers = [];
  }
  var _mirror; // mirror image
  var _source; // source container
  var _item; // item being dragged
  var _offsetX; // reference x
  var _offsetY; // reference y
  var _moveX; // reference move x
  var _moveY; // reference move y
  var _initialSibling; // reference sibling when grabbed
  var _currentSibling; // reference sibling now
  var _copy; // item used for copying
  var _renderTimer; // timer for setTimeout renderMirrorImage
  var _lastDropTarget = null; // last container item was over
  var _grabbed; // holds mousedown context until first mousemove

  var o = options || {};
  if (o.moves === void 0) { o.moves = always; }
  if (o.accepts === void 0) { o.accepts = always; }
  if (o.invalid === void 0) { o.invalid = invalidTarget; }
  if (o.containers === void 0) { o.containers = initialContainers || []; }
  if (o.isContainer === void 0) { o.isContainer = never; }
  if (o.copy === void 0) { o.copy = false; }
  if (o.copySortSource === void 0) { o.copySortSource = false; }
  if (o.revertOnSpill === void 0) { o.revertOnSpill = false; }
  if (o.removeOnSpill === void 0) { o.removeOnSpill = false; }
  if (o.direction === void 0) { o.direction = 'vertical'; }
  if (o.ignoreInputTextSelection === void 0) { o.ignoreInputTextSelection = true; }
  if (o.mirrorContainer === void 0) { o.mirrorContainer = doc.body; }

  var drake = emitter({
    containers: o.containers,
    start: manualStart,
    end: end,
    cancel: cancel,
    remove: remove,
    destroy: destroy,
    canMove: canMove,
    dragging: false
  });

  if (o.removeOnSpill === true) {
    drake.on('over', spillOver).on('out', spillOut);
  }

  events();

  return drake;

  function isContainer (el) {
    return drake.containers.indexOf(el) !== -1 || o.isContainer(el);
  }

  function events (remove) {
    var op = remove ? 'remove' : 'add';
    touchy(documentElement, op, 'mousedown', grab);
    touchy(documentElement, op, 'mouseup', release);
  }

  function eventualMovements (remove) {
    var op = remove ? 'remove' : 'add';
    touchy(documentElement, op, 'mousemove', startBecauseMouseMoved);
  }

  function movements (remove) {
    var op = remove ? 'remove' : 'add';
    crossvent[op](documentElement, 'selectstart', preventGrabbed); // IE8
    crossvent[op](documentElement, 'click', preventGrabbed);
  }

  function destroy () {
    events(true);
    release({});
  }

  function preventGrabbed (e) {
    if (_grabbed) {
      e.preventDefault();
    }
  }

  function grab (e) {
    _moveX = e.clientX;
    _moveY = e.clientY;

    var ignore = whichMouseButton(e) !== 1 || e.metaKey || e.ctrlKey;
    if (ignore) {
      return; // we only care about honest-to-god left clicks and touch events
    }
    var item = e.target;
    var context = canStart(item);
    if (!context) {
      return;
    }
    _grabbed = context;
    eventualMovements();
    if (e.type === 'mousedown') {
      if (isInput(item)) { // see also: https://github.com/bevacqua/dragula/issues/208
        item.focus(); // fixes https://github.com/bevacqua/dragula/issues/176
      } else {
        e.preventDefault(); // fixes https://github.com/bevacqua/dragula/issues/155
      }
    }
  }

  function startBecauseMouseMoved (e) {
    if (!_grabbed) {
      return;
    }
    if (whichMouseButton(e) === 0) {
      release({});
      return; // when text is selected on an input and then dragged, mouseup doesn't fire. this is our only hope
    }
    // truthy check fixes #239, equality fixes #207
    if (e.clientX !== void 0 && e.clientX === _moveX && e.clientY !== void 0 && e.clientY === _moveY) {
      return;
    }
    if (o.ignoreInputTextSelection) {
      var clientX = getCoord('clientX', e);
      var clientY = getCoord('clientY', e);
      var elementBehindCursor = doc.elementFromPoint(clientX, clientY);
      if (isInput(elementBehindCursor)) {
        return;
      }
    }

    var grabbed = _grabbed; // call to end() unsets _grabbed
    eventualMovements(true);
    movements();
    end();
    start(grabbed);

    var offset = getOffset(_item);
    _offsetX = getCoord('pageX', e) - offset.left;
    _offsetY = getCoord('pageY', e) - offset.top;

    classes.add(_copy || _item, 'gu-transit');
    renderMirrorImage();
    drag(e);
  }

  function canStart (item) {
    if (drake.dragging && _mirror) {
      return;
    }
    if (isContainer(item)) {
      return; // don't drag container itself
    }
    var handle = item;
    while (getParent(item) && isContainer(getParent(item)) === false) {
      if (o.invalid(item, handle)) {
        return;
      }
      item = getParent(item); // drag target should be a top element
      if (!item) {
        return;
      }
    }
    var source = getParent(item);
    if (!source) {
      return;
    }
    if (o.invalid(item, handle)) {
      return;
    }

    var movable = o.moves(item, source, handle, nextEl(item));
    if (!movable) {
      return;
    }

    return {
      item: item,
      source: source
    };
  }

  function canMove (item) {
    return !!canStart(item);
  }

  function manualStart (item) {
    var context = canStart(item);
    if (context) {
      start(context);
    }
  }

  function start (context) {
    if (isCopy(context.item, context.source)) {
      _copy = context.item.cloneNode(true);
      drake.emit('cloned', _copy, context.item, 'copy');
    }

    _source = context.source;
    _item = context.item;
    _initialSibling = _currentSibling = nextEl(context.item);

    drake.dragging = true;
    drake.emit('drag', _item, _source);
  }

  function invalidTarget () {
    return false;
  }

  function end () {
    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    drop(item, getParent(item));
  }

  function ungrab () {
    _grabbed = false;
    eventualMovements(true);
    movements(true);
  }

  function release (e) {
    ungrab();

    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    var clientX = getCoord('clientX', e);
    var clientY = getCoord('clientY', e);
    var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
    var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
    if (dropTarget && ((_copy && o.copySortSource) || (!_copy || dropTarget !== _source))) {
      drop(item, dropTarget);
    } else if (o.removeOnSpill) {
      remove();
    } else {
      cancel();
    }
  }

  function drop (item, target) {
    var parent = getParent(item);
    if (_copy && o.copySortSource && target === _source) {
      parent.removeChild(_item);
    }
    if (isInitialPlacement(target)) {
      drake.emit('cancel', item, _source, _source);
    } else {
      drake.emit('drop', item, target, _source, _currentSibling);
    }
    cleanup();
  }

  function remove () {
    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    var parent = getParent(item);
    if (parent) {
      parent.removeChild(item);
    }
    drake.emit(_copy ? 'cancel' : 'remove', item, parent, _source);
    cleanup();
  }

  function cancel (revert) {
    if (!drake.dragging) {
      return;
    }
    var reverts = arguments.length > 0 ? revert : o.revertOnSpill;
    var item = _copy || _item;
    var parent = getParent(item);
    var initial = isInitialPlacement(parent);
    if (initial === false && reverts) {
      if (_copy) {
        if (parent) {
          parent.removeChild(_copy);
        }
      } else {
        _source.insertBefore(item, _initialSibling);
      }
    }
    if (initial || reverts) {
      drake.emit('cancel', item, _source, _source);
    } else {
      drake.emit('drop', item, parent, _source, _currentSibling);
    }
    cleanup();
  }

  function cleanup () {
    var item = _copy || _item;
    ungrab();
    removeMirrorImage();
    if (item) {
      classes.rm(item, 'gu-transit');
    }
    if (_renderTimer) {
      clearTimeout(_renderTimer);
    }
    drake.dragging = false;
    if (_lastDropTarget) {
      drake.emit('out', item, _lastDropTarget, _source);
    }
    drake.emit('dragend', item);
    _source = _item = _copy = _initialSibling = _currentSibling = _renderTimer = _lastDropTarget = null;
  }

  function isInitialPlacement (target, s) {
    var sibling;
    if (s !== void 0) {
      sibling = s;
    } else if (_mirror) {
      sibling = _currentSibling;
    } else {
      sibling = nextEl(_copy || _item);
    }
    return target === _source && sibling === _initialSibling;
  }

  function findDropTarget (elementBehindCursor, clientX, clientY) {
    var target = elementBehindCursor;
    while (target && !accepted()) {
      target = getParent(target);
    }
    return target;

    function accepted () {
      var droppable = isContainer(target);
      if (droppable === false) {
        return false;
      }

      var immediate = getImmediateChild(target, elementBehindCursor);
      var reference = getReference(target, immediate, clientX, clientY);
      var initial = isInitialPlacement(target, reference);
      if (initial) {
        return true; // should always be able to drop it right back where it was
      }
      return o.accepts(_item, target, _source, reference);
    }
  }

  function drag (e) {
    if (!_mirror) {
      return;
    }
    e.preventDefault();

    var clientX = getCoord('clientX', e);
    var clientY = getCoord('clientY', e);
    var x = clientX - _offsetX;
    var y = clientY - _offsetY;

    _mirror.style.left = x + 'px';
    _mirror.style.top = y + 'px';

    var item = _copy || _item;
    var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
    var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
    var changed = dropTarget !== null && dropTarget !== _lastDropTarget;
    if (changed || dropTarget === null) {
      out();
      _lastDropTarget = dropTarget;
      over();
    }
    var parent = getParent(item);
    if (dropTarget === _source && _copy && !o.copySortSource) {
      if (parent) {
        parent.removeChild(item);
      }
      return;
    }
    var reference;
    var immediate = getImmediateChild(dropTarget, elementBehindCursor);
    if (immediate !== null) {
      reference = getReference(dropTarget, immediate, clientX, clientY);
    } else if (o.revertOnSpill === true && !_copy) {
      reference = _initialSibling;
      dropTarget = _source;
    } else {
      if (_copy && parent) {
        parent.removeChild(item);
      }
      return;
    }
    if (
      (reference === null && changed) ||
      reference !== item &&
      reference !== nextEl(item)
    ) {
      _currentSibling = reference;
      dropTarget.insertBefore(item, reference);
      drake.emit('shadow', item, dropTarget, _source);
    }
    function moved (type) { drake.emit(type, item, _lastDropTarget, _source); }
    function over () { if (changed) { moved('over'); } }
    function out () { if (_lastDropTarget) { moved('out'); } }
  }

  function spillOver (el) {
    classes.rm(el, 'gu-hide');
  }

  function spillOut (el) {
    if (drake.dragging) { classes.add(el, 'gu-hide'); }
  }

  function renderMirrorImage () {
    if (_mirror) {
      return;
    }
    var rect = _item.getBoundingClientRect();
    _mirror = _item.cloneNode(true);
    _mirror.style.width = getRectWidth(rect) + 'px';
    _mirror.style.height = getRectHeight(rect) + 'px';
    classes.rm(_mirror, 'gu-transit');
    classes.add(_mirror, 'gu-mirror');
    o.mirrorContainer.appendChild(_mirror);
    touchy(documentElement, 'add', 'mousemove', drag);
    classes.add(o.mirrorContainer, 'gu-unselectable');
    drake.emit('cloned', _mirror, _item, 'mirror');
  }

  function removeMirrorImage () {
    if (_mirror) {
      classes.rm(o.mirrorContainer, 'gu-unselectable');
      touchy(documentElement, 'remove', 'mousemove', drag);
      getParent(_mirror).removeChild(_mirror);
      _mirror = null;
    }
  }

  function getImmediateChild (dropTarget, target) {
    var immediate = target;
    while (immediate !== dropTarget && getParent(immediate) !== dropTarget) {
      immediate = getParent(immediate);
    }
    if (immediate === documentElement) {
      return null;
    }
    return immediate;
  }

  function getReference (dropTarget, target, x, y) {
    var horizontal = o.direction === 'horizontal';
    var reference = target !== dropTarget ? inside() : outside();
    return reference;

    function outside () { // slower, but able to figure out any position
      var len = dropTarget.children.length;
      var i;
      var el;
      var rect;
      for (i = 0; i < len; i++) {
        el = dropTarget.children[i];
        rect = el.getBoundingClientRect();
        if (horizontal && (rect.left + rect.width / 2) > x) { return el; }
        if (!horizontal && (rect.top + rect.height / 2) > y) { return el; }
      }
      return null;
    }

    function inside () { // faster, but only available if dropped inside a child element
      var rect = target.getBoundingClientRect();
      if (horizontal) {
        return resolve(x > rect.left + getRectWidth(rect) / 2);
      }
      return resolve(y > rect.top + getRectHeight(rect) / 2);
    }

    function resolve (after) {
      return after ? nextEl(target) : target;
    }
  }

  function isCopy (item, container) {
    return typeof o.copy === 'boolean' ? o.copy : o.copy(item, container);
  }
}

function touchy (el, op, type, fn) {
  var touch = {
    mouseup: 'touchend',
    mousedown: 'touchstart',
    mousemove: 'touchmove'
  };
  var pointers = {
    mouseup: 'pointerup',
    mousedown: 'pointerdown',
    mousemove: 'pointermove'
  };
  var microsoft = {
    mouseup: 'MSPointerUp',
    mousedown: 'MSPointerDown',
    mousemove: 'MSPointerMove'
  };
  if (global.navigator.pointerEnabled) {
    crossvent[op](el, pointers[type], fn);
  } else if (global.navigator.msPointerEnabled) {
    crossvent[op](el, microsoft[type], fn);
  } else {
    crossvent[op](el, touch[type], fn);
    crossvent[op](el, type, fn);
  }
}

function whichMouseButton (e) {
  if (e.touches !== void 0) { return e.touches.length; }
  if (e.which !== void 0 && e.which !== 0) { return e.which; } // see https://github.com/bevacqua/dragula/issues/261
  if (e.buttons !== void 0) { return e.buttons; }
  var button = e.button;
  if (button !== void 0) { // see https://github.com/jquery/jquery/blob/99e8ff1baa7ae341e94bb89c3e84570c7c3ad9ea/src/event.js#L573-L575
    return button & 1 ? 1 : button & 2 ? 3 : (button & 4 ? 2 : 0);
  }
}

function getOffset (el) {
  var rect = el.getBoundingClientRect();
  return {
    left: rect.left + getScroll('scrollLeft', 'pageXOffset'),
    top: rect.top + getScroll('scrollTop', 'pageYOffset')
  };
}

function getScroll (scrollProp, offsetProp) {
  if (typeof global[offsetProp] !== 'undefined') {
    return global[offsetProp];
  }
  if (documentElement.clientHeight) {
    return documentElement[scrollProp];
  }
  return doc.body[scrollProp];
}

function getElementBehindPoint (point, x, y) {
  var p = point || {};
  var state = p.className;
  var el;
  p.className += ' gu-hide';
  el = doc.elementFromPoint(x, y);
  p.className = state;
  return el;
}

function never () { return false; }
function always () { return true; }
function getRectWidth (rect) { return rect.width || (rect.right - rect.left); }
function getRectHeight (rect) { return rect.height || (rect.bottom - rect.top); }
function getParent (el) { return el.parentNode === doc ? null : el.parentNode; }
function isInput (el) { return el.tagName === 'INPUT' || el.tagName === 'TEXTAREA' || el.tagName === 'SELECT' || isEditable(el); }
function isEditable (el) {
  if (!el) { return false; } // no parents were editable
  if (el.contentEditable === 'false') { return false; } // stop the lookup
  if (el.contentEditable === 'true') { return true; } // found a contentEditable element in the chain
  return isEditable(getParent(el)); // contentEditable is set to 'inherit'
}

function nextEl (el) {
  return el.nextElementSibling || manually();
  function manually () {
    var sibling = el;
    do {
      sibling = sibling.nextSibling;
    } while (sibling && sibling.nodeType !== 1);
    return sibling;
  }
}

function getEventHost (e) {
  // on touchend event, we have to use `e.changedTouches`
  // see http://stackoverflow.com/questions/7192563/touchend-event-properties
  // see https://github.com/bevacqua/dragula/issues/34
  if (e.targetTouches && e.targetTouches.length) {
    return e.targetTouches[0];
  }
  if (e.changedTouches && e.changedTouches.length) {
    return e.changedTouches[0];
  }
  return e;
}

function getCoord (coord, e) {
  var host = getEventHost(e);
  var missMap = {
    pageX: 'clientX', // IE8
    pageY: 'clientY' // IE8
  };
  if (coord in missMap && !(coord in host) && missMap[coord] in host) {
    coord = missMap[coord];
  }
  return host[coord];
}

module.exports = dragula;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./classes":7,"contra/emitter":3,"crossvent":4}],9:[function(require,module,exports){
/*! @preserve 
 * numeral.js language configuration
 * language : portuguese brazil (pt-br)
 * author : Ramiro Varandas Jr : https://github.com/ramirovjr
 */
(function () {
    var language = {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'mil',
            million: 'milhões',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function (number) {
            return 'º';
        },
        currency: {
            symbol: 'R$'
        }
    };

    // Node
    if (typeof module !== 'undefined' && module.exports) {
        module.exports = language;
    }
    // Browser
    if (typeof window !== 'undefined' && this.numeral && this.numeral.language) {
        this.numeral.language('pt-br', language);
    }
}());
},{}],10:[function(require,module,exports){
/*! @preserve
 * numeral.js
 * version : 1.5.6
 * author : Adam Draper
 * license : MIT
 * http://adamwdraper.github.com/Numeral-js/
 */

(function() {

    /************************************
        Variables
    ************************************/

    var numeral,
        VERSION = '1.5.6',
        // internal storage for language config files
        languages = {},
        defaults = {
            currentLanguage: 'en',
            zeroFormat: null,
            nullFormat: null,
            defaultFormat: '0,0'
        },
        options = {
            currentLanguage: defaults.currentLanguage,
            zeroFormat: defaults.zeroFormat,
            nullFormat: defaults.nullFormat,
            defaultFormat: defaults.defaultFormat
        },
        byteSuffixes = {
            bytes: ['B','KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            iec: ['B','KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
        };


    /************************************
        Constructors
    ************************************/


    // Numeral prototype object
    function Numeral(number) {
        this._value = number;
    }

    /**
     * Implementation of toFixed() that treats floats more like decimals
     *
     * Fixes binary rounding issues (eg. (0.615).toFixed(2) === '0.61') that present
     * problems for accounting- and finance-related software.
     */
    function toFixed (value, maxDecimals, roundingFunction, optionals) {
        var splitValue = value.toString().split('.'),
            minDecimals = maxDecimals - (optionals || 0),
            boundedPrecision,
            optionalsRegExp,
            power,
            output;

        // Use the smallest precision value possible to avoid errors from floating point representation
        if (splitValue.length === 2) {
          boundedPrecision = Math.min(Math.max(splitValue[1].length, minDecimals), maxDecimals);
        } else {
          boundedPrecision = minDecimals;
        }

        power = Math.pow(10, boundedPrecision);

        //roundingFunction = (roundingFunction !== undefined ? roundingFunction : Math.round);
        // Multiply up by precision, round accurately, then divide and use native toFixed():
        output = (roundingFunction(value * power) / power).toFixed(boundedPrecision);

        if (optionals > maxDecimals - boundedPrecision) {
            optionalsRegExp = new RegExp('\\.?0{1,' + (optionals - (maxDecimals - boundedPrecision)) + '}$');
            output = output.replace(optionalsRegExp, '');
        }

        return output;
    }

    /************************************
        Formatting
    ************************************/

    // determine what type of formatting we need to do
    function formatNumeral(n, format, roundingFunction) {
        var output;

        if (n._value === 0 && options.zeroFormat !== null) {
            output = options.zeroFormat;
        } else if (n._value === null && options.nullFormat !== null) {
            output = options.nullFormat;
        } else {
            // figure out what kind of format we are dealing with
            if (format.indexOf('$') > -1) {
                output = formatCurrency(n, format, roundingFunction);
            } else if (format.indexOf('%') > -1) {
                output = formatPercentage(n, format, roundingFunction);
            } else if (format.indexOf(':') > -1) {
                output = formatTime(n, format);
            } else if (format.indexOf('b') > -1 || format.indexOf('ib') > -1) {
                output = formatBytes(n, format, roundingFunction);
            } else if (format.indexOf('o') > -1) {
                output = formatOrdinal(n, format, roundingFunction);
            } else {
                output = formatNumber(n._value, format, roundingFunction);
            }
        }

        return output;
    }

    function formatCurrency(n, format, roundingFunction) {
        var symbolIndex = format.indexOf('$'),
            openParenIndex = format.indexOf('('),
            minusSignIndex = format.indexOf('-'),
            space = '',
            spliceIndex,
            output;

        // check for space before or after currency
        if (format.indexOf(' $') > -1) {
            space = ' ';
            format = format.replace(' $', '');
        } else if (format.indexOf('$ ') > -1) {
            space = ' ';
            format = format.replace('$ ', '');
        } else {
            format = format.replace('$', '');
        }

        // format the number
        output = formatNumber(n._value, format, roundingFunction, false);

        // position the symbol
        if (symbolIndex <= 1) {
            if (output.indexOf('(') > -1 || output.indexOf('-') > -1) {
                output = output.split('');
                spliceIndex = 1;
                if (symbolIndex < openParenIndex || symbolIndex < minusSignIndex) {
                    // the symbol appears before the "(" or "-"
                    spliceIndex = 0;
                }
                output.splice(spliceIndex, 0, languages[options.currentLanguage].currency.symbol + space);
                output = output.join('');
            } else {
                output = languages[options.currentLanguage].currency.symbol + space + output;
            }
        } else {
            if (output.indexOf(')') > -1) {
                output = output.split('');
                output.splice(-1, 0, space + languages[options.currentLanguage].currency.symbol);
                output = output.join('');
            } else {
                output = output + space + languages[options.currentLanguage].currency.symbol;
            }
        }

        return output;
    }

    function formatPercentage(n, format, roundingFunction) {
        var space = '',
            output,
            value = n._value * 100;

        // check for space before %
        if (format.indexOf(' %') > -1) {
            space = ' ';
            format = format.replace(' %', '');
        } else {
            format = format.replace('%', '');
        }

        output = formatNumber(value, format, roundingFunction);

        if (output.indexOf(')') > -1) {
            output = output.split('');
            output.splice(-1, 0, space + '%');
            output = output.join('');
        } else {
            output = output + space + '%';
        }

        return output;
    }

    function formatBytes(n, format, roundingFunction) {
        var output,
            suffixes = format.indexOf('ib') > -1 ? byteSuffixes.iec : byteSuffixes.bytes,
            value = n._value,
            suffix = '',
            power,
            min,
            max;

        // check for space before
        if (format.indexOf(' b') > -1 || format.indexOf(' ib') > -1) {
            suffix = ' ';
            format = format.replace(' ib', '').replace(' b', '');
        } else {
            format = format.replace('ib', '').replace('b', '');
        }

        for (power = 0; power <= suffixes.length; power++) {
            min = Math.pow(1024, power);
            max = Math.pow(1024, power + 1);

            if (value === null || value === 0 || value >= min && value < max) {
                suffix += suffixes[power];

                if (min > 0) {
                    value = value / min;
                }

                break;
            }
        }

        output = formatNumber(value, format, roundingFunction);

        return output + suffix;
    }

    function formatOrdinal(n, format, roundingFunction) {
        var output,
            ordinal = '';

        // check for space before
        if (format.indexOf(' o') > -1) {
            ordinal = ' ';
            format = format.replace(' o', '');
        } else {
            format = format.replace('o', '');
        }

        ordinal += languages[options.currentLanguage].ordinal(n._value);

        output = formatNumber(n._value, format, roundingFunction);

        return output + ordinal;
    }

    function formatTime(n) {
        var hours = Math.floor(n._value / 60 / 60),
            minutes = Math.floor((n._value - (hours * 60 * 60)) / 60),
            seconds = Math.round(n._value - (hours * 60 * 60) - (minutes * 60));

        return hours + ':' + ((minutes < 10) ? '0' + minutes : minutes) + ':' + ((seconds < 10) ? '0' + seconds : seconds);
    }

    function formatNumber(value, format, roundingFunction) {
        var negP = false,
            signed = false,
            optDec = false,
            abbr = '',
            abbrK = false, // force abbreviation to thousands
            abbrM = false, // force abbreviation to millions
            abbrB = false, // force abbreviation to billions
            abbrT = false, // force abbreviation to trillions
            abbrForce = false, // force abbreviation
            abs,
            min,
            max,
            power,
            w,
            precision,
            thousands,
            d = '',
            neg = false;

        if (value === null) {
            value = 0;
        }

        abs = Math.abs(value);

        // see if we should use parentheses for negative number or if we should prefix with a sign
        // if both are present we default to parentheses
        if (format.indexOf('(') > -1) {
            negP = true;
            format = format.slice(1, -1);
        } else if (format.indexOf('+') > -1) {
            signed = true;
            format = format.replace(/\+/g, '');
        }

        // see if abbreviation is wanted
        if (format.indexOf('a') > -1) {
            // check if abbreviation is specified
            abbrK = format.indexOf('aK') >= 0;
            abbrM = format.indexOf('aM') >= 0;
            abbrB = format.indexOf('aB') >= 0;
            abbrT = format.indexOf('aT') >= 0;
            abbrForce = abbrK || abbrM || abbrB || abbrT;

            // check for space before abbreviation
            if (format.indexOf(' a') > -1) {
                abbr = ' ';
            }

            format = format.replace(new RegExp(abbr + 'a[KMBT]?'), '');

            if (abs >= Math.pow(10, 12) && !abbrForce || abbrT) {
                // trillion
                abbr = abbr + languages[options.currentLanguage].abbreviations.trillion;
                value = value / Math.pow(10, 12);
            } else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9) && !abbrForce || abbrB) {
                // billion
                abbr = abbr + languages[options.currentLanguage].abbreviations.billion;
                value = value / Math.pow(10, 9);
            } else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6) && !abbrForce || abbrM) {
                // million
                abbr = abbr + languages[options.currentLanguage].abbreviations.million;
                value = value / Math.pow(10, 6);
            } else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3) && !abbrForce || abbrK) {
                // thousand
                abbr = abbr + languages[options.currentLanguage].abbreviations.thousand;
                value = value / Math.pow(10, 3);
            }
        }


        if (format.indexOf('[.]') > -1) {
            optDec = true;
            format = format.replace('[.]', '.');
        }

        w = value.toString().split('.')[0];
        precision = format.split('.')[1];
        thousands = format.indexOf(',');

        if (precision) {
            if (precision.indexOf('[') > -1) {
                precision = precision.replace(']', '');
                precision = precision.split('[');
                d = toFixed(value, (precision[0].length + precision[1].length), roundingFunction, precision[1].length);
            } else {
                d = toFixed(value, precision.length, roundingFunction);
            }

            w = d.split('.')[0];

            if (d.indexOf('.') > -1) {
                d = languages[options.currentLanguage].delimiters.decimal + d.split('.')[1];
            } else {
                d = '';
            }

            if (optDec && Number(d.slice(1)) === 0) {
                d = '';
            }
        } else {
            w = toFixed(value, null, roundingFunction);
        }

        // format number
        if (w.indexOf('-') > -1) {
            w = w.slice(1);
            neg = true;
        }

        if (thousands > -1) {
            w = w.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + languages[options.currentLanguage].delimiters.thousands);
        }

        if (format.indexOf('.') === 0) {
            w = '';
        }

        return ((negP && neg) ? '(' : '') + ((!negP && neg) ? '-' : '') + ((!neg && signed) ? '+' : '') + w + d + ((abbr) ? abbr : '') + ((negP && neg) ? ')' : '');
    }


    /************************************
        Unformatting
    ************************************/

    // revert to number
    function unformatNumeral(n, string) {
        var stringOriginal = string,
            thousandRegExp,
            millionRegExp,
            billionRegExp,
            trillionRegExp,
            bytesMultiplier = false,
            power,
            value;

        if (string.indexOf(':') > -1) {
            value = unformatTime(string);
        } else {
            if (string === options.zeroFormat || string === options.nullFormat) {
                value = 0;
            } else {
                if (languages[options.currentLanguage].delimiters.decimal !== '.') {
                    string = string.replace(/\./g, '').replace(languages[options.currentLanguage].delimiters.decimal, '.');
                }

                // see if abbreviations are there so that we can multiply to the correct number
                thousandRegExp = new RegExp('[^a-zA-Z]' + languages[options.currentLanguage].abbreviations.thousand + '(?:\\)|(\\' + languages[options.currentLanguage].currency.symbol + ')?(?:\\))?)?$');
                millionRegExp = new RegExp('[^a-zA-Z]' + languages[options.currentLanguage].abbreviations.million + '(?:\\)|(\\' + languages[options.currentLanguage].currency.symbol + ')?(?:\\))?)?$');
                billionRegExp = new RegExp('[^a-zA-Z]' + languages[options.currentLanguage].abbreviations.billion + '(?:\\)|(\\' + languages[options.currentLanguage].currency.symbol + ')?(?:\\))?)?$');
                trillionRegExp = new RegExp('[^a-zA-Z]' + languages[options.currentLanguage].abbreviations.trillion + '(?:\\)|(\\' + languages[options.currentLanguage].currency.symbol + ')?(?:\\))?)?$');

                // see if bytes are there so that we can multiply to the correct number
                for (power = 1; power <= byteSuffixes.bytes.length; power++) {
                    bytesMultiplier = ((string.indexOf(byteSuffixes.bytes[power]) > -1) || (string.indexOf(byteSuffixes.iec[power]) > -1))? Math.pow(1024, power) : false;

                    if (bytesMultiplier) {
                        break;
                    }
                }

                // do some math to create our number
                value = bytesMultiplier ? bytesMultiplier : 1;
                value *= stringOriginal.match(thousandRegExp) ? Math.pow(10, 3) : 1;
                value *= stringOriginal.match(millionRegExp) ? Math.pow(10, 6) : 1;
                value *= stringOriginal.match(billionRegExp) ? Math.pow(10, 9) : 1;
                value *= stringOriginal.match(trillionRegExp) ? Math.pow(10, 12) : 1;
                // check for percentage
                value *= string.indexOf('%') > -1 ? 0.01 : 1;
                // check for negative number
                value *= (string.split('-').length + Math.min(string.split('(').length - 1, string.split(')').length - 1)) % 2 ? 1 : -1;
                // remove non numbers
                value *= Number(string.replace(/[^0-9\.]+/g, ''));
                // round if we are talking about bytes
                value = bytesMultiplier ? Math.ceil(value) : value;
            }
        }

        n._value = value;

        return n._value;
    }
    function unformatTime(string) {
        var timeArray = string.split(':'),
            seconds = 0;
        // turn hours and minutes into seconds and add them all up
        if (timeArray.length === 3) {
            // hours
            seconds = seconds + (Number(timeArray[0]) * 60 * 60);
            // minutes
            seconds = seconds + (Number(timeArray[1]) * 60);
            // seconds
            seconds = seconds + Number(timeArray[2]);
        } else if (timeArray.length === 2) {
            // minutes
            seconds = seconds + (Number(timeArray[0]) * 60);
            // seconds
            seconds = seconds + Number(timeArray[1]);
        }
        return Number(seconds);
    }


    /************************************
        Top Level Functions
    ************************************/

    numeral = function(input) {
        if (numeral.isNumeral(input)) {
            input = input.value();
        } else if (input === 0 || typeof input === 'undefined') {
            input = 0;
        } else if (input === null) {
            input = null;
        } else if (!Number(input)) {
            input = numeral.fn.unformat(input);
        } else {
            input = Number(input);
        }

        return new Numeral(input);
    };

    // version number
    numeral.version = VERSION;

    // compare numeral object
    numeral.isNumeral = function(obj) {
        return obj instanceof Numeral;
    };


    // This function will load languages and then set the global language.  If
    // no arguments are passed in, it will simply return the current global
    // language key.
    numeral.language = function(key, values) {
        if (!key) {
            return options.currentLanguage;
        }

        key = key.toLowerCase();

        if (key && !values) {
            if (!languages[key]) {
                throw new Error('Unknown language : ' + key);
            }

            options.currentLanguage = key;
        }

        if (values || !languages[key]) {
            loadLanguage(key, values);
        }

        return numeral;
    };

    numeral.reset = function() {
        for (var property in defaults) {
            options[property] = defaults[property];
        }
    };

    // This function provides access to the loaded language data.  If
    // no arguments are passed in, it will simply return the current
    // global language object.
    numeral.languageData = function(key) {
        if (!key) {
            return languages[options.currentLanguage];
        }

        if (!languages[key]) {
            throw new Error('Unknown language : ' + key);
        }

        return languages[key];
    };

    numeral.language('en', {
        delimiters: {
            thousands: ',',
            decimal: '.'
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function(number) {
            var b = number % 10;
            return (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
        },
        currency: {
            symbol: '$'
        }
    });

    numeral.zeroFormat = function(format) {
        options.zeroFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.nullFormat = function (format) {
        options.nullFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.defaultFormat = function(format) {
        options.defaultFormat = typeof(format) === 'string' ? format : '0.0';
    };

    numeral.validate = function(val, culture) {
        var _decimalSep,
            _thousandSep,
            _currSymbol,
            _valArray,
            _abbrObj,
            _thousandRegEx,
            languageData,
            temp;

        //coerce val to string
        if (typeof val !== 'string') {
            val += '';
            if (console.warn) {
                console.warn('Numeral.js: Value is not string. It has been co-erced to: ', val);
            }
        }

        //trim whitespaces from either sides
        val = val.trim();

        //if val is just digits return true
        if ( !! val.match(/^\d+$/)) {
            return true;
        }

        //if val is empty return false
        if (val === '') {
            return false;
        }

        //get the decimal and thousands separator from numeral.languageData
        try {
            //check if the culture is understood by numeral. if not, default it to current language
            languageData = numeral.languageData(culture);
        } catch (e) {
            languageData = numeral.languageData(numeral.language());
        }

        //setup the delimiters and currency symbol based on culture/language
        _currSymbol = languageData.currency.symbol;
        _abbrObj = languageData.abbreviations;
        _decimalSep = languageData.delimiters.decimal;
        if (languageData.delimiters.thousands === '.') {
            _thousandSep = '\\.';
        } else {
            _thousandSep = languageData.delimiters.thousands;
        }

        // validating currency symbol
        temp = val.match(/^[^\d]+/);
        if (temp !== null) {
            val = val.substr(1);
            if (temp[0] !== _currSymbol) {
                return false;
            }
        }

        //validating abbreviation symbol
        temp = val.match(/[^\d]+$/);
        if (temp !== null) {
            val = val.slice(0, -1);
            if (temp[0] !== _abbrObj.thousand && temp[0] !== _abbrObj.million && temp[0] !== _abbrObj.billion && temp[0] !== _abbrObj.trillion) {
                return false;
            }
        }

        _thousandRegEx = new RegExp(_thousandSep + '{2}');

        if (!val.match(/[^\d.,]/g)) {
            _valArray = val.split(_decimalSep);
            if (_valArray.length > 2) {
                return false;
            } else {
                if (_valArray.length < 2) {
                    return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx));
                } else {
                    if (_valArray[0].length === 1) {
                        return ( !! _valArray[0].match(/^\d+$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    } else {
                        return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    }
                }
            }
        }

        return false;
    };

    /************************************
        Helpers
    ************************************/

    function loadLanguage(key, values) {
        languages[key] = values;
    }

    /************************************
        Floating-point helpers
    ************************************/

    // The floating-point helper functions and implementation
    // borrows heavily from sinful.js: http://guipn.github.io/sinful.js/

    // Production steps of ECMA-262, Edition 5, 15.4.4.21
    // Reference: http://es5.github.io/#x15.4.4.21
    if (!Array.prototype.reduce) {
        Array.prototype.reduce = function(callback /*, initialValue*/) {
            'use strict';
            if (this === null) {
                throw new TypeError('Array.prototype.reduce called on null or undefined');
            }

            if (typeof callback !== 'function') {
                throw new TypeError(callback + ' is not a function');
            }

            var t = Object(this), len = t.length >>> 0, k = 0, value;

            if (arguments.length === 2) {
                value = arguments[1];
            } else {
                while (k < len && !(k in t)) {
                    k++;
                }

                if (k >= len) {
                    throw new TypeError('Reduce of empty array with no initial value');
                }

                value = t[k++];
            }
            for (; k < len; k++) {
                if (k in t) {
                    value = callback(value, t[k], k, t);
                }
            }
            return value;
        };
    }

    /**
     * Computes the multiplier necessary to make x >= 1,
     * effectively eliminating miscalculations caused by
     * finite precision.
     */
    function multiplier(x) {
        var parts = x.toString().split('.');
        if (parts.length < 2) {
            return 1;
        }
        return Math.pow(10, parts[1].length);
    }

    /**
     * Given a variable number of arguments, returns the maximum
     * multiplier that must be used to normalize an operation involving
     * all of them.
     */
    function correctionFactor() {
        var args = Array.prototype.slice.call(arguments);
        return args.reduce(function(prev, next) {
            var mp = multiplier(prev),
                mn = multiplier(next);
            return mp > mn ? mp : mn;
        }, -Infinity);
    }


    /************************************
        Numeral Prototype
    ************************************/


    numeral.fn = Numeral.prototype = {

        clone: function() {
            return numeral(this);
        },

        format: function (inputString, roundingFunction) {
            return formatNumeral(this,
                inputString ? inputString : options.defaultFormat,
                roundingFunction !== undefined ? roundingFunction : Math.round
            );
        },

        unformat: function (inputString) {
            if (Object.prototype.toString.call(inputString) === '[object Number]') {
                return inputString;
            }

            return unformatNumeral(this, inputString ? inputString : options.defaultFormat);
        },

        value: function() {
            return this._value;
        },

        valueOf: function() {
            return this._value;
        },

        set: function(value) {
            this._value = Number(value);
            return this;
        },

        add: function(value) {
            var corrFactor = correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum + corrFactor * curr;
            }
            this._value = [this._value, value].reduce(cback, 0) / corrFactor;
            return this;
        },

        subtract: function(value) {
            var corrFactor = correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum - corrFactor * curr;
            }
            this._value = [value].reduce(cback, this._value * corrFactor) / corrFactor;
            return this;
        },

        multiply: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = correctionFactor(accum, curr);
                return (accum * corrFactor) * (curr * corrFactor) /
                    (corrFactor * corrFactor);
            }
            this._value = [this._value, value].reduce(cback, 1);
            return this;
        },

        divide: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = correctionFactor(accum, curr);
                return (accum * corrFactor) / (curr * corrFactor);
            }
            this._value = [this._value, value].reduce(cback);
            return this;
        },

        difference: function(value) {
            return Math.abs(numeral(this._value).subtract(value).value());
        }

    };

    /************************************
        Exposing Numeral
    ************************************/

    // CommonJS module is defined
    if (typeof module !== 'undefined' && module.exports) {
        module.exports = numeral;
    }

    /*global ender:false */
    if (typeof ender === 'undefined') {
        // here, `this` means `window` in the browser, or `global` on the server
        // add `numeral` as a global object via a string identifier,
        // for Closure Compiler 'advanced' mode
        this['numeral'] = numeral;
    }

    /*global define:false */
    if (typeof define === 'function' && define.amd) {
        define([], function() {
            return numeral;
        });
    }
}).call(this);

},{}],11:[function(require,module,exports){
var si = typeof setImmediate === 'function', tick;
if (si) {
  tick = function (fn) { setImmediate(fn); };
} else {
  tick = function (fn) { setTimeout(fn, 0); };
}

module.exports = tick;
},{}],12:[function(require,module,exports){
module.exports = "\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n      <p>Itens disponíveis:</p>\r\n        <div class=\"item-field\" ng-mouseover=\"$ctrl.hover = 0\" ng-mouseleave=\"$ctrl.hover = undefined\">\r\n            <div class=\"form-control\" ng-click=\"$ctrl.setColumnsHidden(false)\"\r\n            ng-class=\"{ 'active-item': $ctrl.hover === 0 }\">\r\n                <i class=\"fa fa-check fa-fw\"></i> Mostrar todos\r\n            </div>\r\n        </div>\r\n        <div class=\"item-field\" ng-mouseover=\"$ctrl.hover = 1\" ng-mouseleave=\"$ctrl.hover = undefined\">\r\n            <div class=\"form-control\" ng-click=\"$ctrl.setColumnsHidden(true)\"\r\n            ng-class=\"{ 'active-item': $ctrl.hover === 1 }\">\r\n                <i class=\"fa fa-times fa-fw\"></i> Esconder todos\r\n            </div>\r\n        </div>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <p>Tipo de gráfico:</p>\r\n    <div id=\"{{ i.type }}\" class=\"item-field\" ng-repeat=\"i in $ctrl.chartTypes\">\r\n        <div class=\"form-control\" ng-class=\"{ 'active-item' : i.sel }\"\r\n        title=\"clique para mudar o tipo do gráfico\"\r\n        ng-click=\"$ctrl.setChartType(i.type)\">\r\n        <i class=\"{{i.icon}}\"></i>\r\n        {{i.label}}\r\n        </div>\r\n    </div>\r\n</div>";

},{}],13:[function(require,module,exports){
/* global angular, _, $ */
'use strict'

var dragula = require('dragula')

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/chart-editor.html', require('./chart-editor.html'))
})

.controller('ChartEditorCtrl', function ($rootScope, $scope, $timeout, EngineSvc) {
  var self = this
  self.chartTypes = []
  self.chartTypes.push({label: "Linhas", type: "line", icon: "fa fa-line-chart fa-fw", sel: true })
  self.chartTypes.push({label: "Barras", type: "bar", icon: "fa fa-bar-chart fa-fw", sel: false })
  self.chartTypes.push({label: "Radar", type: "radar", icon: "fa fa-pie-chart fa-fw", sel: false })

  self.orderType = ['asc']
  self.sortOrder = []

  // Apply selected options
  self.apply = function () {
    if (self.shouldFade) {
      $('.atlas-display').hide()
      delete self.shouldFade
    }

    EngineSvc.setInput(self.input)
      .setColumns(self.settings.columns)
      .groupRows(self.settings.groups)
      .aggregateValues(self.settings.values, self.settings.aggregations)
      .styleRows(self.settings.groups)
      .sortRows(self.sortOrder, self.orderType)
      .getOutput(function (data) {
        self.output = data
        $('.atlas-display').fadeIn(500)
      })
  }

  // Setup columns and their respective default operations
  function setupColumns () {
    // Initialise settings if it isn't yet
    if (!self.settings) { self.settings = {} }
    if (!self.settings.columns) { self.settings.columns = [] }
    if (!self.settings.hide) { self.settings.hide = [] }
    if (!self.settings.groups) { self.settings.groups = [] }
    if (!self.settings.values) { self.settings.values = [] }
    if (!self.settings.aggregations) { self.settings.aggregations = {} }
    if (!self.settings.chartType) { self.settings.chartType = undefined }

    // Check if there's input
    if (!self.input || !self.input.columns || !self.input.rows) {
      return
    }

    // Copy columns from input
    self.settings.columns = _.chain(self.input.columns)
      .clone()
      .filter(function (obj) {
        var a = _.find(self.settings.groups, { id: obj.id })
        return (typeof a === 'undefined')
      })
      .value()

    // Reset aggregations
    self.settings.aggregations = {}

    // Set a default operation for each column
    self.input.columns.forEach(function (i) {
      var operation = 'count'

      // Default operation is based in the value's type
      if ('integer|decimal|money'.indexOf(i.type) !== -1) {
        operation = 'sum'
      }

      self.settings.aggregations[i.id] = operation
    })

   //set order
   if (self.settings.groups.length > 0) {
     self.sortOrder = _.chain(self.settings.groups)
     .clone()
     .map(function (i) {
       return i.id
      })
      .value()
    }
  }

  self.setChartType = function (type) {
    self.chartTypes.forEach(function (i) {
      if (i.type === type)
      {
        i.sel = true
        self.input.chartSettings.Type = i.type
        self.settings.chartType = i.type
      }
      else
      {
        i.sel = false
      }
    })

    setupColumns()
  }

  self.hideColumns = function () {
    // Copy columns from input
    self.settings.columns = _.chain(self.input.columns)
      .clone()
      .filter(function (obj) {
          var a = _.find(self.settings.groups, { id: obj.id })
          return (typeof a === 'undefined')
      })
      .map(function (col) {
          // Check if should hide this column
          col.hidden = (self.settings.hide.indexOf(col.id) !== -1)
          return col
      })
      .value()
  }

  self.setColumnsHidden = function (hide) {
    self.input.columns.forEach(function (i) {
      if (i.hidden !== undefined) {
        i.hidden = hide
      }
    })

    if (self.input.chartSettings.HideAll === undefined) {
	    self.input.chartSettings.HideAll = false
    }
    else {
      self.input.chartSettings.HideAll = (!self.input.chartSettings.HideAll)
    }

    setupColumns()
  }

  // Watch for changes
  // $scope.$watch(function () {
  //   return self.input
  // }, function () {
  //   setupColumns()
  //   self.apply()
  // }, true)

  // Initialisation
  self.setChartType(self.settings.chartType || self.input.chartSettings.Type)
  self.hideColumns()
  self.apply()
})

.component('pivotChartEditor', { // tag HTML: pivot-editor
  templateUrl: '/atlas-engine/partials/chart-editor.html',
  controller: 'ChartEditorCtrl',
  transclude: true,
  bindings: {
    input: '=',
    output: '=',
    settings: '='
  }
})

},{"./chart-editor.html":12,"dragula":8}],14:[function(require,module,exports){
module.exports = "<div class=\"print-area\">\r\n  <canvas id=\"AtlasChart\"></canvas>\r\n</div>\r\n";

},{}],15:[function(require,module,exports){
/* global angular, _, Chart */
'use strict'

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/chart.html', require('./chart.html'))
})

.controller('ChartCtrl', function ($scope, $filter) {
  var self = this
  self.chartType = 'line'

  var colors = [
    '#1fd21f', //green
    '#EA4247', //red
    '#63b8ff', //blue
    '#d277ff', //purple
    '#ffdd00', //yellow
    '#f68b1f', //orange
    '#8E5303', //brown
    '#BAEF04', //withe green
    '#0074bf', //dark blue
    '#d6b256', //gold
    '#27263f', //black
    '#D8DCEB',
    '#91182e',
    '#37CEA9',
    '#035638' //dark green
  ]

  var ctx = document.getElementById('AtlasChart')
  var chart = undefined // eslint-disable-line no-undef-init

  function getRandomColor() {
    var letters = '0123456789ABCDEF'
    var color = '#'
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)]
    }

    return color
  }

  // Load data into chart
  function loadChart () {
    var data = {}
    var options = {}

    // Check if it's ok before starting
    if (!self.data || !self.data.columns || !self.data.rows) {
      return
    }

  // Clear previous chart
  if (chart !== undefined) {
      chart.destroy()
      chart = undefined
    }

    // Get only the first group, we shouldn't nest groups in a chart
    var allGroups = _.filter(self.data.columns, function (i) {
      return i.grouped
    })
    var grouped = _.find(allGroups)

    if (!grouped) {
      return
    }

    // Select labels for the first group
    data.labels = _.chain(self.data.rows)
      .filter(function (i) {
        if (allGroups.length === 1) {
          return i.type === 'detail'
        } else {
          return i.type === 'total' && i.level === 1
        }
      })
      .map(function (i) {
        return $filter('FormatValueChart')(i[grouped.id], grouped.type)
      })
      .value()

    // Select aggregated columns
    var aggregated = _.filter(self.data.columns, function (i) {
      return i.aggregated !== undefined
    })

    // Fill datasets
    data.datasets = _.chain(aggregated)
      .map(function (agg, idx) {
        var color = (idx < colors.length) ? colors[(idx)] : getRandomColor()
        var ret = {
          label: agg.label,
          backgroundColor: color,
          borderColor: color,
          lineTension: 0,
          hidden: agg.hidden
        }

        ret.data = _.chain(self.data.rows)
          .filter(function (i) {
            if (allGroups.length === 1) {
              return i.type === 'detail'
            } else {
              return i.type === 'total' && i.level === 1
            }
          })
          .map(function (value) {
            return $filter('FormatValueChart')(value[agg.id], agg.type, agg.aggregated)
          })
          .value()

        return ret
      })
      .value()

    // Set options
    options = {
      legend: {
        display: true,
        labels: {
            fontFamily: 'Lucida Sans Typewriter'
        },
        onHover: function (e, elements) {
            e.target.style.cursor = 'pointer';
        }
      },

      hover: {
        onHover: function (e) {
            var point = this.getElementAtEvent(e);
            if (point.length) e.target.style.cursor = 'pointer';
            else e.target.style.cursor = 'default';
        }
      },
      scales: {
        yAxes: [{
          display: true,
          ticks: {
            suggestedMin: 0,
            beginAtZero: true
          }
        }]
      }
    }

    // Load chart
    chart = new Chart(ctx, {
      type: self.data.chartSettings.Type || 'line',
      data: data,
      options: options
    })
  }

  function updateHidden() {
      var i = 1
      if (chart !== undefined) {
          chart.getDataset().forEach(function (e) {
              self.data.columns[i].hidden = e.hidden
              i++
          })
      }
  }

  // Watch for changes
  //Quando atualizar o tipo de grafico
  $scope.$watch(function () {
    return self.data.chartSettings.Type
  }, function () {
    loadChart()
  }, true)

  //Quando clicar em mostrar/esconder todos
  $scope.$watch(function () {
    return self.data.chartSettings.HideAll
  }, function () {
    loadChart()
  }, true)

  //Ao clicar em alguma legenda do grafico, para esconder ou mostrar algum item
  $scope.$watch(function () {
    return (chart !== undefined) ? chart.getCountHidden() : 0
  }, function (newValue, oldValue) {
      if (newValue !== oldValue) {
        updateHidden()
      }
  }, true)

  // Initialisation
  loadChart()
})

.component('pivotChart', { // tag HTML: pivot-chart
  templateUrl: '/atlas-engine/partials/chart.html',
  controller: 'ChartCtrl',
  bindings: {
    data: '='
  }
})

},{"./chart.html":14}],16:[function(require,module,exports){
module.exports = "<div class=\"print-area atlas-display\">\r\n\r\n  <toast></toast>\r\n\r\n  <table class=\"table table-condensed table-hover\">\r\n    <thead>\r\n      <tr>\r\n        <th class=\"column-order\" ng-repeat=\"c in $ctrl.data.columns\"\r\n          ng-if=\"!c.hidden\" ng-click=\"$ctrl.orderBy(c)\">\r\n          <div class=\"{{ c.type | FormatHeader:c.aggregated }}\">\r\n            <span title=\"{{c.title}}\" data-toggle=\"tooltip\" data-placement=\"right\" ng-bind=\"c.label\" ng-class=\"{ 'grouped': c.grouped }\"></span>\r\n            <span ng-bind=\"c.aggregated\" ng-if=\"c.aggregated\" class=\"label label-primary aggregated\"></span>\r\n            <i class=\"fa fa-stack hidden-print\" ng-if=\"!c.aggregated\">\r\n              <i class=\"fa fa-sort-asc fa-stack-1x\" ng-class=\"{ 'active': $ctrl.order === ('-' + c.id) }\"></i>\r\n              <i class=\"fa fa-sort-desc fa-stack-1x\" ng-class=\"{ 'active': $ctrl.order === ('+' + c.id) }\"></i>\r\n            </i>\r\n          </div>\r\n        </th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr ng-repeat=\"i in $ctrl.data.rows | startFrom: (($ctrl.pag.currentPage-1) * $ctrl.pag.pageSize) |  limitTo:$ctrl.pag.pageSize\" \r\n      class=\"{{ i | FormatRow }}\" ng-click=\"$ctrl.rowClick(i)\" ng-style=\"$ctrl.rowStyle\">\r\n        <td ng-if=\"!c.hidden\" ng-repeat=\"c in $ctrl.data.columns\">\r\n          <span ng-bind-html=\"i[c.id] | FormatValue:c.type:c.aggregated\"></span>\r\n        </td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  <p>Total de registros: <span ng-bind=\"$ctrl.data.rows.length || 0\"></span></p>\r\n  <div ng-if=\"$ctrl.data.rows.length > $ctrl.pag.pageSize\" class=\"text-center\">\r\n    <pagination total-items=\"$ctrl.data.rows.length\" ng-model=\"$ctrl.pag.currentPage\" max-size=\"$ctrl.pag.maxSize\" \r\n                items-per-page=\"$ctrl.pag.pageSize\" boundary-links=\"true\" force-ellipses=\"true\"\r\n                first-text=\"Primeiros\" last-text=\"Últimos\" \r\n                previous-text=\"&laquo; Anteriores\" next-text=\"Próximos &raquo;\">\r\n    </pagination>\r\n  </div>\r\n</div>\r\n<div class=\"print-area atlas-loading\">\r\n  <div class=\"text-center\">\r\n    <p><i class=\"fa fa-spinner fa-spin fa-3x\"></i></p>\r\n    <p>Aguarde, carregando...</p>\r\n  </div>\r\n</div>";

},{}],17:[function(require,module,exports){
/* global angular */
'use strict'

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/display.html', require('./display.html'))
})

.controller('DisplayCtrl', function ($rootScope, $scope) {
  var self = this
  self.addClass = {}
  self.pag = { totalItems: 0, currentPage: 1, maxSize: 10, pageSize: 50 }
  self.rowStyle = { "cursor" : "default" }
  self.showMessage = true

  $rootScope.$on('DisplayOrder', function (ev, column) {
    self.order = '+' + column
  })

  $rootScope.$on('PrintReport', function (ev, printing) {
    if (printing === true) {
      self.pag.pageSize = self.data.rows.lenght
    }
    else {
      self.pag.pageSize = 50
    }
  })

  // Order rows by selected column
  self.orderBy = function (column) {
    if (column.aggregated) {
      return
    }

    var newOrder = '+' + column.id

    if (newOrder.localeCompare(self.order) === 0) {
      newOrder = '-' + column.id
    }

    self.order = newOrder
    $scope.$emit('AtlasOrderBy', self.order)
  }

  self.rowClick = function (row) {
    if ( self.data.rowCallback !== null ) {
      var value = row[self.data.rowCallback.columnParameter]

      if (value !== undefined){
        self.actionFn({id:value})
      }
    }
  }

  // Setup columns and their types
  function setupColumns () {
    self.columns = []
    if (!self.order && self.data && self.data.columns && self.data.rows) {
      // Set default order for the first column
      self.order = '+' + self.data.columns.filter(function (i) {return !i.hidden})[0].id
    }

    if  (self.data){
      if ( self.data.rowCallback !== null ) {
        if (self.data.columns.filter(function (i) {return i.id === self.data.rowCallback.columnParameter}).length){
          self.rowStyle = { "cursor" : "pointer" }
          if ( self.showMessage ){
            toastr.info( self.data.rowCallback.mensagem )
            self.showMessage = false
          }
        }
        else {
          self.rowStyle = { "cursor" : "default" }
        }
      }
    }

    setTimeout(function () {
       $('[data-toggle="tooltip"]').tooltip();
    }, 500);
  }

  // Watch for changes
  $scope.$watch(function () {
    return self.data
  }, function () {
    setupColumns()
  }, true)
})

.component('pivotDisplay', { // tag HTML: pivot-display
  templateUrl: '/atlas-engine/partials/display.html',
  controller: 'DisplayCtrl',
  bindings: {
    data: '=',
    actionFn: '&'
  }
})

},{"./display.html":16}],18:[function(require,module,exports){
module.exports = "<div class=\"row\" ng-click=\"$ctrl.hideDropDown(i.id)\">\r\n  <div class=\"col-md-4\">\r\n    <div class=\"panel-scroll\">\r\n      <p>Colunas disponíveis:</p>\r\n      <section id=\"columns\" class=\"field-container\">\r\n        <div id=\"{{ i.id }}\" class=\"editor-field hover-buttons\" ng-repeat=\"i in $ctrl.settings.columns\"\r\n          ng-mouseover=\"$ctrl.hover = i.id\" ng-mouseleave=\"$ctrl.hover = undefined\"\r\n          ng-class=\"{ 'input-group': $ctrl.hover === i.id }\">\r\n          <div class=\"form-control\"\r\n            title=\"{{ i.hidden ? 'Coluna removida, clique ao lado para adicioná-la' : 'Clique e arraste para grupos ou valores' }}\"\r\n            ng-bind=\"i.label\" ng-class=\"{ 'drag-forbidden': i.hidden }\">\r\n          </div>\r\n          <div class=\"input-group-btn\" ng-if=\"$ctrl.hover === i.id\">\r\n            <button type=\"button\" class=\"btn btn-success\" title=\"Adicionar coluna\"\r\n              ng-if=\"i.hidden\" ng-click=\"$ctrl.setVisibility(i.id, true)\">\r\n              <i class=\"fa fa-plus\"></i>\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger\" title=\"Remover coluna\"\r\n              ng-if=\"!i.hidden\" ng-click=\"$ctrl.setVisibility(i.id, false)\">\r\n              <i class=\"fa fa-remove\"></i>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </section>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-md-4\">\r\n    <p>Grupos:</p>\r\n    <section class=\"field-container\">\r\n      <div class=\"container-backtip\" ng-if=\"$ctrl.settings.groups.length === 0\">\r\n        <p><i class=\"fa fa-cubes fa-fw fa-3x\"></i></p>\r\n        <p>Arraste as colunas que deseja agrupar para cá.</p>\r\n      </div>\r\n      <div id=\"groups\" class=\"container-drop\">\r\n        <div id=\"{{ i.id }}\" class=\"form-control editor-field\"\r\n          ng-repeat=\"i in $ctrl.settings.groups\"\r\n          ng-class=\"{ 'drag-forbidden': $ctrl.viewChart && $index > 0 }\"\r\n          title=\"{{ $ctrl.viewChart && $index > 0 ? 'Gráficos podem exibir apenas um grupo' : 'Clique e arraste para colunas ou valores' }}\">\r\n          <span ng-bind=\"i.label\"></span>\r\n        </div>\r\n      </div>\r\n    </section>\r\n  </div>\r\n\r\n  <div class=\"col-md-4\">\r\n    <p>Valores:</p>\r\n    <section class=\"field-container\">\r\n      <div class=\"container-backtip\" ng-if=\"$ctrl.settings.values.length === 0\">\r\n        <p><i class=\"fa fa-plus fa-fw fa-3x\"></i></p>\r\n        <p>Arraste para cá as colunas que deseja somar, tirar média, ou calcular outros tipos de agregações.</p>\r\n      </div>\r\n      <div id=\"values\" class=\"container-drop\">\r\n        <div id=\"{{ i.id }}\" class=\"input-group editor-field\" ng-repeat=\"i in $ctrl.settings.values\">\r\n          <div class=\"form-control\" title=\"Clique e arraste para colunas ou grupos\"\r\n            ng-bind=\"i.label\">\r\n          </div>\r\n          <div class=\"input-group-btn\" title=\"Clique para mudar a forma de agregação\">\r\n            <button type=\"button\" class=\"btn btn-primary\"\r\n            ng-click=\"$ctrl.toggleDropDown(i.id)\" click-outsidee=\"$ctrl.showDropDown=undefined\">\r\n              <span ng-bind=\"$ctrl.operations[$ctrl.aggregations[i.id]]\"></span>\r\n              <span class=\"caret\"></span>\r\n            </button>\r\n            <ul class=\"dropdown-menu\" ng-show=\"$ctrl.showDropDown === i.id\" style=\"display: block;\" >\r\n                <li><a href=\"\" ng-repeat=\"(op, label) in $ctrl.operations | AllowedOperations:i.type\"\r\n                ng-click=\"$ctrl.aggregateBy(i.id, op)\" ng-bind=\"label\"></a></li>\r\n\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </section>\r\n  </div>\r\n</div>\r\n";

},{}],19:[function(require,module,exports){
/* global angular, _, $ */
'use strict'

var dragula = require('dragula')

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/editor.html', require('./editor.html'))
})

.controller('EditorCtrl', function ($rootScope, $scope, $timeout, EngineSvc, AggregationSvc) {
  var self = this

  self.operations = AggregationSvc.operations
  self.showDropDown = undefined
  self.hideDropDown = null

  self.orderType = ['asc']
  self.sortOrder = []

  // Column order has been changed
  $rootScope.$on('AtlasOrderBy', function (ev, column) {
    self.orderBy(ev, column)
    self.apply()
  })

  self.orderBy = function (ev, column) {
    // Order columns and groups according to the selected column
    self.sortOrder = []
    self.orderType = []

    // Check if it's a valid column order definition, starting with plus or minus sign
    if (column.length > 1 && '+-'.indexOf(column[0]) !== -1) {
      var type = column[0] === '+' ? 'asc' : 'desc'

      if (self.settings.groups.length > 0) {
        self.orderType = _.times(self.settings.groups.length, _.constant('asc'))
        self.orderType[_.findIndex(self.settings.groups, ['id', column.substring(1)])] = type

        self.sortOrder = _.chain(self.settings.groups)
          .clone()
          .map(function (i) {
            return i.id
          })
          .value()
      }
      else {
        self.sortOrder.push(column.substring(1))
        self.orderType.push(type)
      }
    }
    else
    {
      if (self.settings.groups.length > 0) {
        self.orderType = _.times(self.settings.groups.length, _.constant('asc'))

        self.sortOrder = _.chain(self.settings.groups)
          .clone()
          .map(function (i) {
            return i.id
          })
          .value()
      }
      else {
        self.sortOrder.push(self.settings.columns.filter(function (i) { return !i.hidden })[0].id)
        self.orderType.push('asc')
      }
    }
  }

  // Change aggregation type
  self.aggregateBy = function (columnId, type) {
    self.settings.aggregations[columnId] = type
    self.atlasDisplay(true)
    self.showDropDown = undefined
    self.apply()
  }

  self.atlasDisplay = function ( loading ) {
      if ( loading ) {
        $('.atlas-display').hide()
        $('.atlas-loading').show()
      }
      else {
        $('.atlas-loading').hide()
        $('.atlas-display').fadeIn(500)
      }
  }

  // Apply selected options
  self.apply = function () {
    EngineSvc.setInput(self.input)
      .setColumns(self.settings.columns)
      .groupRows(self.settings.groups)
      .aggregateValues(self.settings.values, self.settings.aggregations)
      .styleRows(self.settings.groups)
      .sortRows(self.sortOrder, self.orderType)
      .getOutput(function (data) {
        self.output = data
        $timeout(function () {
          self.atlasDisplay(false)
        })
      })
  }

  // Setup Dragula
  dragula([
    document.getElementById('columns'),
    document.getElementById('groups'),
    document.getElementById('values')
  ], {
    invalid: function (el, handle) {
      if (el.id) {
        var item = _.find(self.settings.columns, { id: el.id })
        if (item) {
          return item.hidden === true
        }
      }
      return false
    }
  })
  .on('drop', function (el, target, source) {
    self.atlasDisplay(true)
    self.settings[source.id] = getFieldsFromContainer(source.id)
    self.settings[target.id] = getFieldsFromContainer(target.id)
    $scope.$apply()

    var order = '+'
    if (self.settings.groups.length > 0) {
        order += self.settings.groups[0].id
    }
    else if (self.settings.values.length > 0) {
        order += self.settings.values[0].id
    }
    else if (self.settings.columns.length > 0) {
        order += self.settings.columns.filter(function (i) { return !i.hidden })[0].id
    }

    self.orderBy(null, order)
    $scope.$emit('DisplayOrder', order.substring(1))

    $timeout(function () {
      self.apply()
    })
  })

  // Setup columns and their respective default operations
  function setupColumns () {
    // Initialise settings if it isn't yet
    if (!self.settings) { self.settings = {} }
    if (!self.settings.columns) { self.settings.columns = [] }
    if (!self.settings.hide) { self.settings.hide = [] }
    if (!self.settings.groups) { self.settings.groups = [] }
    if (!self.settings.values) { self.settings.values = [] }
    if (!self.settings.aggregations) { self.settings.aggregations = {} }

    // Check if there's input
    if (!self.input || !self.input.columns || !self.input.rows) {
      return
    }

    // Copy columns from input
    self.settings.columns = _.chain(self.input.columns)
      .clone()
      .filter(function (obj) {
        var a = _.find(self.settings.groups, { id: obj.id })
        var b = _.find(self.settings.values, { id: obj.id })
        return (typeof a === 'undefined' && typeof b === 'undefined')
      })
      .map(function (col) {
        // Check if should hide this column
        col.hidden = (self.settings.hide.indexOf(col.id) !== -1)
        return col
      })
      .value()

    // Reset aggregations
    self.settings.aggregations = {}

    // Set a default operation for each column
    self.input.columns.forEach(function (i) {
      var operation = 'count'

      // Default operation is based in the value's type
      if ('integer|decimal|money'.indexOf(i.type) !== -1) {
        operation = 'sum'
      }

      self.settings.aggregations[i.id] = operation
    })

    if (self.sortOrder.length === 0) {
      self.orderBy(null, '')
    }
  }

  // Hide or show a column
  self.setVisibility = function (id, visible) {
    self.settings.hide = self.settings.hide.filter(function (i) {
      return i !== id
    })

    if (!visible) {
      self.settings.hide.push(id)
    }

    self.settings.columns.forEach(function (i) {
      if (i.id === id) {
        i.hidden = !visible
      }
    })

    self.input.columns.forEach(function (i) {
      if (i.id === id) {
        i.hidden = !visible
      }
    })

    if (self.settings.groups.length === 0 && self.settings.values.length === 0) {
      var visibles = self.settings.columns.filter(function (i) { return !i.hidden })
      var order = '+'

      if (!visible) {
        if (self.sortOrder[0] === id) {
          if (visibles.length > 0) {
            $scope.$emit('DisplayOrder', visibles[0].id)
            order += visibles[0].id
            self.orderBy(null, order)
          }
        }
      }
      else {
        if (visibles.length === 1) {
          $scope.$emit('DisplayOrder', visibles[0].id)
          order += visibles[0].id
          self.orderBy(null, order)
        }
      }
    }
  }

  // Get ids from a given container
  function getFieldsFromContainer (container) {
    var ret = []
    var parent = document.getElementById(container)

    if (parent) {
      var children = Array.prototype.slice.call(parent.childNodes)
      children.forEach(function (i) {
        if (i.className && i.className.indexOf('editor-field') !== -1) {
          var item = _.find(self.input.columns, { id: i.id })
          var json = _.clone(item)

          // Carry hidden tag from columns
          if (container === 'columns') {
            var other = _.find(self.settings.columns, { id: i.id })
            if (other) {
              json.hidden = other.hidden
            }
          }

          ret.push(json)
        }
      })
    }

    return ret
  }

  self.toggleDropDown = function (id) {
    self.hideDropDown = null
    self.showDropDown = (self.showDropDown === id) ? undefined : id 

    if (self.showDropDown !== undefined) {
        setTimeout(function () { self.hideDropDown = fnHideDropDown }, 0);
    }
  }
 
  var fnHideDropDown = function (id) {
    self.showDropDown = undefined
  }

  // Watch for changes
  // $scope.$watch(function () {
  //   return self.input
  // }, function () {
  //   setupColumns()
  //   self.apply()
  // }, true)

  // Initialisation
  setupColumns()
  self.apply()
})

.component('pivotEditor', { // tag HTML: pivot-editor
  templateUrl: '/atlas-engine/partials/editor.html',
  controller: 'EditorCtrl',
  transclude: true,
  bindings: {
    input: '=',
    output: '=',
    settings: '='
  }
})

},{"./editor.html":18,"dragula":8}],20:[function(require,module,exports){
module.exports = "<div class=\"{{ $ctrl.style }}\">\r\n  <ng-transclude></ng-transclude>\r\n</div>\r\n";

},{}],21:[function(require,module,exports){
/* global angular */

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/fab-container.html', require('./fab-container.html'))
})

.controller('FabContainerCtrl', function ($scope, $timeout, ExportSvc) {
  var self = this
  self.style = undefined

  if (self.float) {
    self.style = 'float ' + self.float
  }

  // Print request event
  $scope.$on('AtlasFabEvent.Print', function (ev) {
    $scope.$emit('PrintReport', true)

    $timeout(function () {
      window.print()
      $scope.$emit('PrintReport', false)
    })
  })

  // CSV export request event
  $scope.$on('AtlasFabEvent.ExportCSV', function (ev, data) {
    // Generate CSV content for downloading
    var file = ExportSvc.downloadCSV(data.columns, data.rows)

    // Make filename
    var title = 'Atlas Engine'
    var h1 = document.getElementsByTagName('h1')
    if (h1.length > 0) {
      title = h1[0].innerHTML
    }

    // Make a temporary link to download a named file
    var link = document.createElement('a')
    link.setAttribute('href', file)
    link.setAttribute('download', title + '.csv')
    link.setAttribute('style', 'display:none')
    document.body.appendChild(link)

    // Auto click the temporary link to download it and delete the link right away
    link.click()
  })
})

.component('fabContainer', { // tag HTML: fab-container
  templateUrl: '/atlas-engine/partials/fab-container.html',
  controller: 'FabContainerCtrl',
  transclude: true,
  bindings: {
    float: '@'
  }
})

},{"./fab-container.html":20}],22:[function(require,module,exports){
module.exports = "<span class=\"fab fa-stack fa-2x\" ng-click=\"$ctrl.onClick()\">\r\n  <i class=\"fa fa-circle fa-stack-2x {{ $ctrl.color }}\"></i>\r\n  <i class=\"fa fa-{{ $ctrl.icon }} fa-stack-1x fa-inverse\"></i>\r\n</span>\r\n";

},{}],23:[function(require,module,exports){
/* global angular */

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/fab.html', require('./fab.html'))
})

.controller('FabCtrl', function ($scope) {
  var self = this

  self.onClick = function () {
    // Toggle boolean variables
    if (typeof self.toggle === 'boolean') {
      self.toggle = true
    }

    self.ignoreOutClick = true

    // Emit events with or without data
    if (self.emit) {
      $scope.$emit('AtlasFabEvent.' + self.emit, self.data)
      return
    }
  }
})

.component('fab', { // tag HTML: fab
  templateUrl: '/atlas-engine/partials/fab.html',
  controller: 'FabCtrl',
  bindings: {
    icon: '@',
    color: '@',
    title: '@',
    emit: '@',
    data: '=',
    toggle: '=',
    ignoreOutClick: '='
  }
})

},{"./fab.html":22}],24:[function(require,module,exports){
module.exports = "<div class=\"panel panel-primary {{ $ctrl.style }}\" ng-show=\"$ctrl.visibility\">\r\n  <div class=\"panel-heading\">\r\n    <button type=\"button\" class=\"close\" ng-click=\"$ctrl.hide()\">&times;</button>\r\n    <span ng-bind=\"$ctrl.panelTitle\"></span>\r\n  </div>\r\n  <div class=\"panel-body\">\r\n    <ng-transclude></ng-transclude>\r\n  </div>\r\n  <div class=\"panel-footer align-right\">\r\n    <button ng-if=\"$ctrl.actionLabel\" type=\"button\" class=\"btn btn-primary\" ng-click=\"$ctrl.actionFn()\" ng-bind=\"$ctrl.actionLabel\">\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-default\" ng-click=\"$ctrl.hide()\">\r\n      Fechar\r\n    </button>\r\n  </div>\r\n</div>\r\n";

},{}],25:[function(require,module,exports){
/* global angular */

angular.module('AtlasEngine')

.run(function ($templateCache) {
  $templateCache.put('/atlas-engine/partials/panel.html', require('./panel.html'))
})

.controller('PanelCtrl', function ($scope) {
  var self = this
  self.style = undefined
  self.visibility = false

  // Hide current panel
  self.hide = function () {
    self.visibility = false
  }

  // Set floating style
  if (self.float) {
    self.style = 'float ' + self.float
  }
})

.component('panel', { // tag HTML: panel
  templateUrl: '/atlas-engine/partials/panel.html',
  controller: 'PanelCtrl',
  transclude: true,
  bindings: {
    panelTitle: '@',
    float: '@',
    visibility: '=',
    actionFn: '&',
    actionLabel: '@'
  }
})

},{"./panel.html":24}],26:[function(require,module,exports){
/* global angular, _ */
'use strict'

angular.module('AtlasEngine')

.filter('AllowedOperations', function () {
  return function (input, type) {
    var allowed = []

    switch (type) {
      case 'integer':
      case 'decimal':
      case 'money':
        allowed = [ 'average', 'sum', 'min', 'max' ]
        break

      case 'date':
      case 'datetime':
      case 'string':
        allowed = [ 'count', 'countDistinct' ]
        break

      default:
        break
    }

    var ret = _.pick(input, allowed)
    return ret
  }
})

},{}],27:[function(require,module,exports){
/* global angular */
'use strict'

angular.module('AtlasEngine')

.filter('FormatHeader', function () {
  return function (type, aggregated) {
    if (aggregated && 'integer|decimal|money'.indexOf(type) === -1) {
      type = 'integer'
    }

    switch (type) {
      case 'integer':
      case 'decimal':
      case 'money':
        return 'align-right'

      default:
        return ''
    }
  }
})

},{}],28:[function(require,module,exports){
/* global angular */
'use strict'

angular.module('AtlasEngine')

.filter('FormatRow', function () {
  return function (row) {
    if (!row.type) {
      return undefined
    }

    var ret = 'row-' + row.type

    if (row.type === 'total') {
      ret += '-' + row.level
    }

    return ret
  }
})

},{}],29:[function(require,module,exports){
/* global angular, moment */
'use strict'

var numeral = require('numeral')
var ptbr = require('numeral/languages/pt-br')
numeral.language('pt-br', ptbr)
numeral.language('pt-br')

angular.module('AtlasEngine')

.filter('FormatValueChart', function () {
  return function (input, type, aggregated) {
    if (typeof input === 'undefined') {
      return ''
    }

    if (aggregated && 'integer|decimal|money'.indexOf(type) === -1) {
      type = 'integer'
    }

    switch (type) {
      case 'integer':
        return parseInt(input)

      case 'decimal':
        return numeral(input).value()

      case 'money':
        return numeral(input).value()

      case 'date':
        return moment(input).toDate().toLocaleDateString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'datetime':
        return moment(input).toDate().toLocaleString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'boolean':
        return (input.toString().toLowerCase() === 'true') ? 'Sim' : 'Não'

      default:
        return input
    }
  }
})

},{"numeral":10,"numeral/languages/pt-br":9}],30:[function(require,module,exports){
/* global angular, moment */
'use strict'

var numeral = require('numeral')
var ptbr = require('numeral/languages/pt-br')
numeral.language('pt-br', ptbr)
numeral.language('pt-br')

angular.module('AtlasEngine')

.filter('FormatValueRaw', function () {
  return function (input, type, aggregated) {
    if (typeof input === 'undefined') {
      return ''
    }

    if (aggregated && 'integer|decimal|money'.indexOf(type) === -1) {
      type = 'integer'
    }

    switch (type) {
      case 'integer':
        return parseInt(input)

      case 'decimal':
        return numeral(input).format('0.00')

      case 'money':
        return numeral(input).format('0.00')

      case 'date':
        return moment(input).toDate().toLocaleDateString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'datetime':
        return moment(input).toDate().toLocaleString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'boolean':
        return (input.toString().toLowerCase() === 'true') ? 'Sim' : 'Não'

      default:
        return input
    }
  }
})

},{"numeral":10,"numeral/languages/pt-br":9}],31:[function(require,module,exports){
/* global angular, moment */
'use strict'

var numeral = require('numeral')
var ptbr = require('numeral/languages/pt-br')
numeral.language('pt-br', ptbr)
numeral.language('pt-br')

angular.module('AtlasEngine')

.filter('FormatValue', function () {
  return function (input, type, aggregated) {
    if (typeof input === 'undefined') {
      return ''
    }

    if (aggregated && 'integer|decimal|money'.indexOf(type) === -1) {
      type = 'integer'
    }

    switch (type) {
      case 'integer':
        return AlignRight(parseInt(input))

      case 'decimal':
        return AlignRight(numeral(input).format('0,0.00'))

      case 'money':
        return FormatMoney(numeral(input).format('0,0.00'))

      case 'date':
        return moment(input).toDate().toLocaleDateString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'datetime':
        return moment(input).toDate().toLocaleString('pt-BR', { timeZone: 'America/Sao_Paulo' })

      case 'boolean':
        return (input.toString().toLowerCase() === 'true') ? 'Sim' : 'Não'

      default:
        return input
    }
  }
})

function AlignRight (s) {
  return '<div class="align-right">' + s + '</div>'
}

function FormatMoney (s) {
  return '<div class="pull-left">R$</div>' + AlignRight(s)
}

},{"numeral":10,"numeral/languages/pt-br":9}],32:[function(require,module,exports){
/* global angular */
'use strict'

angular.module('AtlasEngine', [
  'ngSanitize'
])

// Components
require('./components/chart')
require('./components/chart-editor')
require('./components/display')
require('./components/editor')
require('./components/fab')
require('./components/fab-container')
require('./components/panel')

// Filters
require('./filters/allowed-operations')
require('./filters/format-header')
require('./filters/format-row')
require('./filters/format-value')
require('./filters/format-value-chart')
require('./filters/format-value-raw')

// Services
require('./services/aggregation')
require('./services/engine')
require('./services/export')

},{"./components/chart":15,"./components/chart-editor":13,"./components/display":17,"./components/editor":19,"./components/fab":23,"./components/fab-container":21,"./components/panel":25,"./filters/allowed-operations":26,"./filters/format-header":27,"./filters/format-row":28,"./filters/format-value":31,"./filters/format-value-chart":29,"./filters/format-value-raw":30,"./services/aggregation":33,"./services/engine":34,"./services/export":35}],33:[function(require,module,exports){
/* global angular, _ */
'use strict'

angular.module('AtlasEngine')

.service('AggregationSvc', function () {
  var self = this

  var parseNumeric = function (value) {
    var f = parseFloat(value)
    var i = parseInt(value)

    if (f !== i) {
      return f
    } else {
      return i
    }
  }

  this.operations = {
    count: 'Contagem',
    sum: 'Soma',
    average: 'Média',
    min: 'Mínimo',
    max: 'Máximo'
  }

  this.getDescription = function (op) {
    return self.operations[op] || op
  }

  this.count = function (values) {
    return values.length
  }

  this.sum = function (values) {
    return _.reduce(values, function (acc, n) {
      return parseNumeric(acc) + parseNumeric(n)
    })
  }

  this.average = function (values) {
    return _.reduce(values, function (acc, n) {
      return parseNumeric(acc) + parseNumeric(n)
    }) / values.length
  }

  this.min = function (values) {
    return _.chain(values)
      .map(function (i) {
        return parseNumeric(i)
      })
      .min()
      .value()
  }

  this.max = function (values) {
    return _.chain(values)
      .map(function (i) {
        return parseNumeric(i)
      })
      .max()
      .value()
  }
})

},{}],34:[function(require,module,exports){
/* global angular, _ */
'use strict'

angular.module('AtlasEngine')

.service('EngineSvc', function (AggregationSvc) {
  var self = this
  self.data = []

  // Feed engine with input data
  self.setInput = function (input) {
    self.data = _.clone(input)
    return self
  }

  // Set columns order
  self.setColumns = function (columns) {
    if (self.data) {
      self.data.columns = columns
    }

    return self
  }

  // Group rows by nesting each group as a children, grandchildren and so on
  var groupNested = function (rows, columns, idx) {
    if (!idx) { idx = 0 }

    // Group only one column at a time
    var sliced = columns.slice(idx, idx + 1)
    var groups = _.map(sliced, function (i) {
      return i.id
    })

    var ret = _.chain(rows)
      .groupBy(function (i) {
        var values = _.pick(i, groups)
        return JSON.stringify(values)
      })
      .map(function (children, key) {
        var unboxed = JSON.parse(key)
        unboxed.values = children

        // If there are more columns, recursively group them nested as this children
        if (columns.length > (idx + 1)) {
          unboxed.children = groupNested(children, columns, idx + 1)
        }

        return unboxed
      })
      .value()

    return ret
  }

  // Flatten nested groups for rendering
  var flattenRows = function (nestedRows) {
    var ret = []

    nestedRows.forEach(function (row) {
      // Flatten this row removing children references
      var copy = _.clone(row)
      delete copy.children
      ret.push(copy)

      // Flatten children rows recursively
      if (row.children) {
        var children = flattenRows(row.children)

        // Copy parent groups
        children.forEach(function (c) {
          _.assign(c, copy)
          ret.push(c)
        })
      }
    })

    return ret
  }

  // Group rows by a set of selected columns
  self.groupRows = function (groups) {
    if (groups && groups.length > 0) {
      // Grouping nested makes more hierarquical sense
      var nest = groupNested(self.data.rows, groups)

      // Flatten all rows because nested groups aren't good for rendering
      var flattened = flattenRows(nest)

      // Update data
      self.data.rows = flattened
      self.data.columns = _.map(groups, function (g) {
        g.grouped = true
        return g
      })
    }

    return self
  }

  // Set row type as detail or total, so it can be nicely styled
  self.styleRows = function (groups) {
    var groupNames = _.map(groups, 'id')

    // Iterate over each row
    if (self.data && self.data.rows) {
      self.data.rows = _.map(self.data.rows, function (row) {
        var keys = Object.keys(row).filter(function (i) {
          return groupNames.indexOf(i) !== -1
        })

        if (keys.length === groups.length) {
          row.type = 'detail'
        } else if (keys.length > 0) {
          row.type = 'total'
          row.level = keys.length
        } else {
          row.type = 'grandtotal'
        }

        return row
      })
    }

    return self
  }

  // Aggregate values for each group
  self.aggregateValues = function (fields, operations) {
    if (fields && fields.length > 0) {
      // Get field names
      var fieldNames = _.map(fields, function (f) {
        return f.id
      })

      if (self.data.rows.length > 0) {
        if (self.data.rows[0].values) {
          // Keep group names
          var groups = _.chain(self.data.columns)
          .clone()
          .map(function (i) {
            return i.id
          }).value()

          // Select all rows to aggregate the grand total
          var grandTotal = {}
          grandTotal.values = _.chain(self.data.rows)
          .map(function (row) {
            return row.values
          })
          .reduce(function (acc, n) {
            return _.concat(acc, n)
          })
          .uniqBy()
          .value()

          // Append grand total
          self.data.rows.push(grandTotal)

          // Iterate over each group
          self.data.rows = _.map(self.data.rows, function (row) {
          var keys = Object.keys(row).filter(function (i) {
            return groups.indexOf(i) !== -1
          })

          // Iterate over each aggregation field
          fieldNames.forEach(function (f) {
            var op = operations[f]

            // Pick values to be aggregated
            var values = _.chain(row.values)
              .filter(function (v) {
                var filter = _.pick(row, keys)
                var current = _.pick(v, keys)
                return _.isEqual(current, filter)
              })
              .map(function (v) {
                return v[f]
              })
              .value()

            // Set aggregation column value
            row[f] = AggregationSvc[op](values)
          })

          // The values aren't necessary anymore
          delete row.values

          return row
          })

          self.data.columns = _.concat(self.data.columns, _.map(fields, function (f) {
          f.aggregated = AggregationSvc.getDescription(operations[f.id])
          return f
          }))

          return self
        }
      }
        
      // No groups, aggregate everything
      var row = {}

      // Iterate over each aggregation field
      fieldNames.forEach(function (i) {
        var op = operations[i]
        // Pick values to be aggregated
        var values = _.map(self.data.rows, function (c) {
          return c[i]
        })
        // Set aggregation column value
        row[i] = AggregationSvc[op](values)
      })

      // Update rows and columns list
      self.data.rows = [row]
      self.data.columns = _.map(fields, function (f) {
        f.aggregated = AggregationSvc.getDescription(operations[f.id])
        return f
      })
    }

    return self
  }

  self.findIndexByKeyValue = function (arraytosearch, keys, valuetosearch) {
    var ret = -1;
    for (var i = 0; i < arraytosearch.length; i++) {
      for (var j = 0; j < keys.length; j++) {
        if (arraytosearch[i][keys[j]] == valuetosearch[j]) {
          if (j === (keys.length - 1)) {
            ret = i
            break
          }
        }
        else if (ret >= 0) {
          return ret
        }
        else {
          break
        }
      }
    }

    return ret
  }

  // Sort rows properly placing totals at the bottom
  self.sortRows = function (sortOrder, orderType) {
    var sortDetail = []
    var sortTotal = []
    var gt = undefined

    // Lodash will sort rows by a string representing each of them
    if (self.data && self.data.rows) {
      self.data.rows.forEach(function (i) {
        if (i['type'] == 'detail') {
          sortDetail.push(i)
        }
        else if (i['type'] == 'total') {
          sortTotal.push(i)
        }
        else if (i['type'] == 'grandtotal') {
          gt = i
        }
      })

      sortDetail = _.orderBy(sortDetail, sortOrder, orderType)

      sortTotal.forEach(function (row) {
        var loopstop = false
        var values = []
        var keys = []
        var index = 0

        sortOrder.forEach(function (i) {
          if (loopstop == false) {
            var value = row[i]
            if (value != undefined) {
              values.push(value)
              keys.push(i)
            }
            else {
              //break
              loopstop = true
            }
          }
        })

        index = self.findIndexByKeyValue(sortDetail, keys, values)

        sortDetail.splice(index + 1, 0, row)
      })

      if (gt !== undefined) {
        sortDetail.push(gt)
      }

      self.data.rows = sortDetail
    }

    return self
  }

  // Feed the data to output
  self.getOutput = function (cb) {
    cb(self.data)
    return self
  }
})

},{}],35:[function(require,module,exports){
/* global angular, _ */

angular.module('AtlasEngine')

.service('ExportSvc', function ($filter) {
  this.downloadCSV = function (columns, rows) {
    // Content type header for CSV files
    var ret = 'data:text/csv; charset=utf-8,'

    // Set columns labels
    ret += _.chain(columns)
      .map('label')
      .reduce(function (acc, n) {
        return acc + ',' + n
      })
      .value()

    // Set rows values
    rows.forEach(function (row) {
      var s = ''

      columns.forEach(function (col) {
        s += (s.length === 0 ? '\n' : ',')
        s += '"'
        s += row[col.id] ? $filter('FormatValueRaw')(row[col.id], col.type, col.aggregated) : ''
        s += '"'
      })

      ret += s
    })

    // Return file contents
    var file = encodeURI(ret)
    return file
  }
})

},{}]},{},[32]);
