﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdmax_photosweb.RestClient;

namespace tdmax_photosweb.Services
{
    public class UserService : IUserService
    {
        private readonly IRestClient m_Remote;
        const string API = "http://localhost:55754";

        public UserService( IRestClient p_Remote )
        {
            m_Remote = p_Remote;
        }

        public async Task<Entities.UserEntity> FindById( int p_Userid )
            => await m_Remote.GetObjectAsync<Entities.UserEntity>( API, $"users/{p_Userid}" );

        public async Task<Entities.UserEntity> FindByLogin( string p_Login )
            => await m_Remote.GetObjectAsync<Entities.UserEntity>( API, $"users/bylogin/{p_Login}" );
    }
}
