﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.Services
{
    public interface IUserService
    {
        Task<Entities.UserEntity> FindById( int p_Userid );
        Task<Entities.UserEntity> FindByLogin( string p_Login );
    }
}
