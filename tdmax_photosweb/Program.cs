﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace tdmax_photosweb
{
    public class Program
    {
        public static void Main( string[ ] args )
        {
            CreateWebHostBuilder( args ).Build( ).Run( );
        }

        public static IWebHostBuilder CreateWebHostBuilder( string[ ] args ) =>
            WebHost.CreateDefaultBuilder( args )
            .ConfigureAppConfiguration( ( webHostBuilderContext, configurationbuilder ) =>
            {
                var environment = webHostBuilderContext.HostingEnvironment;
                configurationbuilder
                        .AddJsonFile( "appSettings.json", optional: true );

                configurationbuilder.AddEnvironmentVariables( );
            } )
            .UseStartup<Startup>( );
    }
}
