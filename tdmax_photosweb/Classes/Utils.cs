﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using tdmax_photosweb.Classes.Authentication;

namespace tdmax_photosweb.Classes
{
    public static class Utils
    {
        public static string GetHex( byte[ ] arrayBytes )
        {
            StringBuilder ret = new StringBuilder( );
            ret.Append( "0x" );
            foreach ( byte b in arrayBytes )
            {
                ret.Append( b.ToString( "X2" ) );
            }
            return ret.ToString( );
        }
        public static byte[ ] GetBytes( string text )
        {
            if ( text.StartsWith( "0x" ) )
            {
                int tamanho = ( ( text.Length - 2 ) / 2 );
                byte[ ] arrayByte = new byte[ tamanho ];
                for ( int i = 2; i < text.Length; i += 2 )
                {
                    arrayByte[ ( ( i - 2 ) / 2 ) ] = byte.Parse( string.Format( "{0}{1}", text[ i ], text[ i + 1 ] ), NumberStyles.HexNumber );
                }
                return arrayByte;
            }
            return null;
        }
        public static string CriptySenha( string login, string senha )
        {
            return GetMD5( Encoding.UTF8.GetBytes( login + senha ) );
        }

        public static string GetMD5( string texto )
        {
            return GetMD5( Encoding.UTF8.GetBytes( texto ) );
        }
        public static string GetMD5( byte[ ] pDados )
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider( );
            return string.Join( string.Empty, md5.ComputeHash( pDados ).Select( item => item.ToString( "X2" ) ).ToArray( ) );
        }

        public static string DecryptJs( string criptoStr, AuthenticationInfo auth )
        {
            RijndaelManaged aes = new RijndaelManaged( );
            aes.Padding = PaddingMode.None;
            aes.Key = Encoding.ASCII.GetBytes( auth.Token.Key );

            // Create the streams used for decryption.
            MemoryStream msDecrypt = new MemoryStream( Utils.GetBytes( "0x" + criptoStr.Replace( "\n", "" ) ) );
            byte[ ] iv = new byte[ aes.IV.Length ];
            msDecrypt.Read( iv, 0, aes.IV.Length );
            aes.IV = iv;

            ICryptoTransform decryptor = aes.CreateDecryptor( aes.Key, aes.IV );
            CryptoStream csDecrypt = new CryptoStream( msDecrypt, decryptor, CryptoStreamMode.Read );
            StreamReader srDecrypt = new StreamReader( csDecrypt, Encoding.ASCII );

            return srDecrypt.ReadToEnd( ).Replace( "\0", "" );
        }

        //public static bool CheckTicket( this Controller controller, out object routeValues )
        //{
        //    return CheckTicket( controller, out routeValues, false );
        //}
        //public static bool CheckTicket( this Controller controller, out object routeValues, bool logoff )
        //{
        //    routeValues = null;

        //    HttpContext context = controller.HttpContext;
        //    AuthenticationInfo sessionInfo = new AuthenticationInfo( controller.HttpContext, AuthenticationToken.SessionIdToken );

        //    if ( sessionInfo.Token == null || !sessionInfo.Token.IsValid || logoff )
        //    {
        //        sessionInfo.Token.Reset( );

        //        context.Session.Clear( );
        //        CookieOptions option = new CookieOptions( );
        //        option.Expires = DateTime.Now;
        //        context.Response.Cookies.Append( AuthenticationToken.SessionIdToken, sessionInfo.ToString( ), option );

        //        routeValues = new
        //        {
        //            ReturnUrl = ( string.IsNullOrEmpty( context.Request.QueryString.Value ) || 
        //                            !context.Request.QueryString.Value.Contains( "logoff" ) ?
        //                            context.Request.GetDisplayUrl( ) :
        //                            context.Request.GetDisplayUrl( ).Replace( context.Request.QueryString.Value, "" ) )
        //        };
        //        return false;
        //    }
        //    return true;
        //}

        //public static CustomAuthorization.eAccessLevel GetNivelUsuario( HttpContext context )
        //{
        //    AuthenticationInfo auth = new AuthenticationInfo( context );
        //    if ( !auth.Token.IsAdmin )
        //    {
        //        return CustomAuthorization.eAccessLevel.None;
        //    }
        //    return CustomAuthorization.eAccessLevel.Manager;
        //}

        public static long GetVersao( string text )
        {
            string[ ] versao = text.Split( new char[ ] { '.' }, StringSplitOptions.RemoveEmptyEntries );
            short maior = ( versao.Length > 0 ) ? short.Parse( versao[ 0 ] ) : ( short ) 0;
            short menor = ( versao.Length > 1 ) ? short.Parse( versao[ 1 ] ) : ( short ) 0;
            short build = ( versao.Length > 2 ) ? short.Parse( versao[ 2 ] ) : ( short ) 0;
            short revisao = ( versao.Length > 3 ) ? short.Parse( versao[ 3 ] ) : ( short ) 0;

            return getBigInt( maior, 48 ) | getBigInt( menor, 32 ) | getBigInt( build, 16 ) | getBigInt( revisao, 0 );
        }
        private static long getBigInt( short num, int offset )
        {
            long l = 0;
            return ( l |= ( long ) num << offset );
        }

        public static string RemoveAccents( this string text )
        {
            StringBuilder sbReturn = new StringBuilder( );
            var arrayText = text.Normalize( NormalizationForm.FormD ).ToCharArray( );

            foreach ( char letter in arrayText )
            {
                if ( CharUnicodeInfo.GetUnicodeCategory( letter ) != UnicodeCategory.NonSpacingMark )
                    sbReturn.Append( letter );
            }
            return sbReturn.ToString( );
        }
        public static string Capitalize( this string text )
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase( text );
        }
    }
}
