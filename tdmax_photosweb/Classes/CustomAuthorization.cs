﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using tdmax_photosweb.Classes.Authentication;

namespace tdmax_photosweb.Classes
{
    [System.AttributeUsage( AttributeTargets.Method )]
    public class CustomAuthorization : AuthorizeAttribute
    {
        public enum eAccessLevel : byte
        {
            None = 0,
            Read = 1,
            Write = 2,
            ReadWrite = 3,
            Manager = 4
        }

        public string Pai { get; set; }
        public string Imagem { get; set; }
        public string Descricao { get; set; }
        public eAccessLevel NivelRequerido { get; set; }
        public bool VisivelMenu { get; set; }
        public bool HerdaNivelAcesso { get; set; }

        //protected override void HandleUnauthorizedRequest( AuthorizationContext filterContext )
        //{
        //    System.Web.Routing.RouteValueDictionary r = new System.Web.Routing.RouteValueDictionary( new
        //    {
        //        controller = "UserSecurity",
        //        action = "Login"
        //        /*, ReturnUrl = filterContext.HttpContext.Request.Url */
        //    } );

        //    filterContext.Result = new RedirectToRouteResult( r );

        //    filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

        //    //base.HandleUnauthorizedRequest(filterContext);
        //}

        //protected override bool AuthorizeCore( HttpContext httpContext )
        //{
        //    AuthenticationInfo auth = new AuthenticationInfo( httpContext );
        //    string recurso = GetRecurso( httpContext );
        //    //            this.Descricao = Localization.Current.GetResource( recurso, ( string.IsNullOrEmpty( this.Descricao ) ? recurso : this.Descricao ) );
        //    this.Descricao = this.Descricao;

        //    if ( !auth.Token.IsAdmin )
        //    {
        //        switch ( NivelRequerido )
        //        {
        //            case eAccessLevel.None:
        //            case eAccessLevel.Read:
        //            case eAccessLevel.Write:
        //            case eAccessLevel.Manager:
        //                return auth.Token.IsValid;
        //            default:
        //                return false;
        //        }
        //    }
        //    return auth.Token.IsValid;
        //}

        public static bool CanWrite( HttpContext httpContext )
        {
            AuthenticationInfo auth = new AuthenticationInfo( httpContext );
            if ( !auth.Token.IsAdmin )
            {
                return true;
            }
            return true;
        }

        //public static string GetRecurso( HttpContext httpContext )
        //{
        //    string controller;
        //    string action;
        //    MvcHandler handler = ( httpContext.CurrentHandler as System.Web.Mvc.MvcHandler );
        //    if ( handler != null )
        //    {
        //        controller = handler.RequestContext.RouteData.Values[ "controller" ].ToString( );
        //        action = handler.RequestContext.RouteData.Values[ "action" ].ToString( );

        //    }
        //    else
        //    {
        //        Regex regex = new Regex( "/[\\w]{1,}" );
        //        string entrada = ( httpContext as System.Web.HttpContextWrapper ).Request.FilePath;
        //        MatchCollection matches = regex.Matches( entrada );

        //        controller = matches[ 0 ].Value.Replace( "/", "" );
        //        action = matches[ 1 ].Value.Replace( "/", "" );
        //    }

        //    return string.Format( "{0}/{1}", controller, action );
        //}
    }
}
