﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading.Tasks;

namespace tdmax_photosweb.Classes.Authentication
{
    public class AuthenticationInfo : IIdentity
    {
        public AuthenticationToken Token { get; set; }

        public AuthenticationInfo( ) : this( null, null ) { }
        public AuthenticationInfo( HttpContext context ) : this( context, null ) { }
        public AuthenticationInfo( HttpContext context, string cookieName )
        {
            Token = new AuthenticationToken( );
            if ( context != null )
            {
                string key = cookieName ?? AuthenticationToken.SessionIdToken;
                string cookie = context.Request.Cookies[ key ];
                if ( cookie != null )
                {
                    Token = new AuthenticationToken( cookie );
                }
            }
        }
        public AuthenticationInfo( HttpRequestHeaders headers )
        {
            Token = new AuthenticationToken( );
            var cookie = headers.GetCookies( AuthenticationToken.SessionIdToken ).FirstOrDefault( );
            if ( cookie != null )
            {
                Token = new AuthenticationToken( cookie[ AuthenticationToken.SessionIdToken ].Value );
            }
        }

        public override string ToString( )
        {
            return this.Token.ToString( );
        }

        public static AuthenticationInfo GetAuthentication( HttpRequestMessage requestMessage )
        {
            var cookie = requestMessage.Headers.GetCookies( AuthenticationToken.SessionIdToken ).FirstOrDefault( );
            if ( cookie != null )
            {
                return new AuthenticationInfo( ) { Token = new AuthenticationToken( cookie[ AuthenticationToken.SessionIdToken ].Value ) };
            }
            else if ( requestMessage.Properties.ContainsKey( AuthenticationToken.SessionIdToken ) )
            {
                object auth = requestMessage.Properties[ AuthenticationToken.SessionIdToken ];
                return ( auth == null || !( auth is AuthenticationInfo ) ) ? new AuthenticationInfo( ) : ( AuthenticationInfo ) auth;
            }
            return new AuthenticationInfo( );
        }

        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "Basic"; }
        }

        public bool IsAuthenticated
        {
            get { return this.Token.IsValid; }
        }

        public string Name
        {
            get { return this.Token.Id.ToString( ); }
        }

        #endregion
    }
}
