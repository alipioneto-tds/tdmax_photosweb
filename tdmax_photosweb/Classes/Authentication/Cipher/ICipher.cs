﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.Classes.Authentication.Cipher
{
    public delegate void ProgressEventHandler( object o, ProgressEventArgs args );

    public interface ICipher
    {
        void Decrypt( string SourceFileName, string DestinationFileName, string Key );

        byte[ ] Decrypt( byte[ ] BytesInput, string Key );

        byte[ ] Encrypt( byte[ ] BytesInput, string Key );

        void Encrypt( string SourceFileName, string DestinationFileName, string Key );
    }

    public class ProgressEventArgs : EventArgs
    {
        public ProgressEventArgs( )
        { }

        public ProgressEventArgs( int i )
        { progress = i; }

        public ProgressEventArgs( int i, bool b )
        { progress = i; stop = b; }

        public int progress;
        public bool stop;
    }
}
