﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using tdmax_photosweb.Classes.Authentication.Cipher;

namespace tdmax_photosweb.Classes.Authentication
{
    public class AuthenticationToken
    {
        private static readonly string stKey = "$transdatasmart-webfotos!-aeskey";
        private static AppSettings AppSettings { get; set; }
        private HttpResponseMessage _response;

        public int Id { get; set; }
        public int Cid { get; set; }
        public string Key { get; set; }
        public string Application { get; private set; }
        public bool IsValid { get { return this.Id > 0; } }
        public bool IsAdmin { get; set; }
        public DateTime DataToken { get; private set; }

        public int GrupoOperadoraId { get; set; }

        public static string SessionIdToken => AppSettings.authSessionCookie;

        public AuthenticationToken( HttpResponseMessage Request )
        {
            this._response = Request;
        }

        public AuthenticationToken( )
        {
            this.Application = null;
        }

        public AuthenticationToken( AppSettings settings )
        {
            this.Application = null;
            AppSettings = settings;
        }

        public AuthenticationToken( string token )
        {
            parse( token );
        }

        public string GetSessionIdToken( )
        {
            return SessionIdToken;
        }

        public string GetTokenValue( )
        {
            return this.ToString( );
        }

        public CookieOptions GetCookieOptions( )
        {
            CookieOptions options = new CookieOptions( );
            options.Expires = DateTime.Now.AddMonths(1);
            options.Path = "/";
            options.IsEssential = true;
            options.HttpOnly = false;

            return options;
        }

        private void parse( string token )
        {
            Func<string, byte[ ]> strToByte = ( hex ) =>
            {
                byte[ ] arr = new byte[ hex.Length / 2 ];
                for ( int i = 0; i < hex.Length; i += 2 )
                    arr[ i / 2 ] = byte.Parse( string.Format( "{0}{1}", hex[ i ], hex[ i + 1 ] ), NumberStyles.HexNumber );

                return arr;
            };

            if ( string.IsNullOrEmpty( token ) ) return;

            try
            {
                string decryptText = Encoding.Unicode.GetString(
                                        new AESProvider( ).Decrypt( strToByte( token ), stKey )
                                    );
                string[ ] pairs = decryptText.Split( new char[ ] { ';' } );
                foreach ( string pair in pairs )
                {
                    string[ ] values = pair.Split( new char[ ] { '=' } );
                    if ( values.Length == 2 )
                    {
                        switch ( values[ 0 ] )
                        {
                            case "id":
                                this.Id = int.Parse( values[ 1 ] );
                                break;
                            case "cid":
                                this.Cid = int.Parse( values[ 1 ] );
                                break;
                            case "app":
                                this.Application = values[ 1 ];
                                break;
                            case "key":
                                this.Key = values[ 1 ];
                                break;
                            case "adm":
                                this.IsAdmin = bool.Parse( values[ 1 ] );
                                break;
                            case "dt":
                                this.DataToken = DateTime.Parse( values[ 1 ] );
                                break;
                            case "go":
                                this.GrupoOperadoraId = int.Parse( values[ 1 ] );
                                break;
                        }
                    }
                }
            }
            catch { }
        }

        public void Renew( )
        {
            if ( this.IsValid )
            {
                this.DataToken = DateTime.Now;
            }
        }
        public void Reset( )
        {
            this.Id = 0;
            this.Application = "";
            this.IsAdmin = false;
        }

        public override string ToString( )
        {
            byte[ ] arrayBytes = Encoding.Unicode.GetBytes(
                        string.Format(
                            "id={0};cid={1};app={2};key={3};adm={4};dt:{5};go={6}",
                            this.Id, this.Cid, this.Application, this.Key, this.IsAdmin, DateTime.Now, this.GrupoOperadoraId
                        )
                    );

            byte[ ] crypt = ( new AESProvider( ).Encrypt( arrayBytes, stKey ) );

            StringBuilder str = new StringBuilder( );
            foreach ( byte b in crypt )
                str.AppendFormat( "{0:X2}", b );

            return str.ToString( );
        }


    }
}
