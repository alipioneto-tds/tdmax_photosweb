﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public static class IRestClientHelper
    {
        /// <summary>
		/// Busca um objeto remoto e dispara exception em caso de erro
		/// </summary>
		public static T GetObject<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
            => p_Client.GetObjectAsync<T>( p_Api, p_RelativeUri, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Busca um objeto remoto e dispara exception em caso de erro
        /// </summary>
        public static async Task<T> GetObjectAsync<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        {
            var ret = await p_Client.GetAsync( p_Api, p_RelativeUri, p_Request );

            return ret.GetAsOrException<T>( );
        }

        /// <summary>
        /// Busca um objeto remote interno e dispara um exception em caso de erro
        /// </summary>
        public static T GetInternalObject<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
            => p_Client.GetInternalObjectAsync<T>( p_Api, p_RelativeUri, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Busca um objeto remote interno e dispara um exception em caso de erro
        /// </summary>
        public static async Task<T> GetInternalObjectAsync<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        {
            var ret = await p_Client.GetInternalAsync( p_Api, p_RelativeUri, p_Request );
            return ret.GetAsOrException<T>( );
        }

        /// <summary>
        /// Envia um objeto e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        public static void PostObject( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
            => p_Client.PostObjectAsync( p_Api, p_RelativeUri, p_Object, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
		/// Envia um objeto e valida o sucesso da operação, em caso de erro dispara exception
		/// </summary>
        public static T PostObject<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
            => p_Client.PostObjectAsync<T>( p_Api, p_RelativeUri, p_Object, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Envia um objeto e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        public static async Task PostObjectAsync( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
        {
            var ret = await p_Client.PostAsync( p_Api, p_RelativeUri, p_Object, p_Request );
            ret.EnsureSuccess( );
        }

        /// <summary>
		/// Envia um objeto e valida o sucesso da operação, em caso de erro dispara exception
		/// </summary>
		public static async Task<T> PostObjectAsync<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
        {
            var ret = await p_Client.PostAsync( p_Api, p_RelativeUri, p_Object, p_Request );
            return ret.GetAsOrException<T>( );
        }

        /// <summary>
        /// Envia um objeto e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        public static void PostInternalObject( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
            => p_Client.PostInternalObjectAsync( p_Api, p_RelativeUri, p_Object, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Envia um objeto internamente e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        public static async Task PostInternalObjectAsync( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
        {
            var ret = await p_Client.PostInternalAsync( p_Api, p_RelativeUri, p_Object, p_Request );
            ret.EnsureSuccess( );
        }

        /// <summary>
		/// Envia um objeto internamente e valida o sucesso da operação, em caso de erro dispara exception
		/// </summary>
        public static T PostInternalObject<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
            => p_Client.PostInternalObjectAsync<T>( p_Api, p_RelativeUri, p_Object, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
		/// Envia um objeto internamente e valida o sucesso da operação, em caso de erro dispara exception
		/// </summary>
        public static async Task<T> PostInternalObjectAsync<T>( this IRestClient p_Client, string p_Api, string p_RelativeUri, object p_Object, RestRequest p_Request = null )
        {
            var ret = await p_Client.PostInternalAsync( p_Api, p_RelativeUri, p_Object, p_Request );
            return ret.GetAsOrException<T>( );
        }

        /// <summary>
        /// Realiza operacao de delete e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        //public static void DeleteObject( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        //    => p_Client.DeleteObjectAsync( p_Api, p_RelativeUri, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Realiza operacao de delete e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        //public static async Task DeleteObjectAsync( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        //{
        //    var ret = await p_Client.DeleteAsync( p_Api, p_RelativeUri, p_Request );
        //    ret.EnsureSuccess( );
        //}

        /// <summary>
        /// Realiza operacao de delete e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        //public static void DeleteInternalObject( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        //    => p_Client.DeleteObjectAsync( p_Api, p_RelativeUri, p_Request ).GetAwaiter( ).GetResult( );

        /// <summary>
        /// Realiza operacao de delete e valida o sucesso da operação, em caso de erro dispara exception
        /// </summary>
        //public static async Task DeleteInternalObjectAsync( this IRestClient p_Client, string p_Api, string p_RelativeUri, RestRequest p_Request = null )
        //{
        //    var ret = await p_Client.DeleteInternalAsync( p_Api, p_RelativeUri, p_Request );
        //    ret.EnsureSuccess( );
        //}
    }
}
