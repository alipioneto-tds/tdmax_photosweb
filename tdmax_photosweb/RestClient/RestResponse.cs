﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public class RestResponse : IRestResponse
    {
        public HttpResponseMessage RawResponse { get; private set; }
        public string Content { get; private set; }
        public bool FailOver { get; private set; }
        public bool Success { get { return FailOver || m_IsSuccess( RawResponse.StatusCode ); } }

        private Func<HttpStatusCode, bool> m_IsSuccess;
        //private AtlasRestClientException m_Exception;
        private Exception m_Exception;
        //private IEnumerable<AtlasRestError> m_Error;

        public RestResponse(
            string p_Api,
            string p_RelativeUri,
            string p_method,
            object p_Request,
            HttpResponseMessage p_RawResponse,
            string p_Content,
            bool p_FailOver,
            Func<HttpStatusCode, bool> p_IsSuccess,
            object p_Object = null )
        {
            //m_Exception = new AtlasRestClientException(
            //    $"Method = [{p_method}], HttpCode = [{p_RawResponse.StatusCode}], Api = [{p_Api}], RelativeUri = [{p_RelativeUri}], Response = [{p_Content}]",
            //    p_Api,
            //    p_RelativeUri,
            //    p_Request,
            //    this,
            //    p_Object
            //);

            Content = p_Content;
            RawResponse = p_RawResponse;
            FailOver = p_FailOver;

            m_IsSuccess = p_IsSuccess;
        }

        public T GetAs<T>( )
        {
            return JsonConvert.DeserializeObject<T>( Content );
        }

        //public IEnumerable<AtlasRestError> GetError( )
        //{
        //    if ( null == m_Error )
        //    {
        //        try
        //        {
        //            m_Error = JsonConvert.DeserializeObject<IEnumerable<AtlasRestError>>( Content );
        //        }
        //        catch ( Exception )
        //        {

        //        }

        //        if ( null == m_Error )
        //        {
        //            m_Error = new List<AtlasRestError>( );
        //        }
        //    }

        //    return m_Error;
        //}

        public T GetAsOrException<T>( )
        {
            if ( Success )
            {
                T obj = GetAs<T>( );
                if ( null != obj )
                {
                    return obj;
                }
            }

            throw m_Exception;
        }

        public void EnsureSuccess( )
        {
            if ( !Success )
            {
                throw m_Exception;
            }
        }

        //public static implicit operator bool( AtlasRestResponse p_Resp )
        //{
        //    return p_Resp.Success;
        //}
    }
}
