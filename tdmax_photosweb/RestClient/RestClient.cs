﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public class RestClient : IRestClient
    {
        private readonly IUrlApiResolve mUrlResolve;

        private enum HttpType
        {
            POST, PUT, PATCH, GET, DELETE
        };

        private class EnqueueData
        {
            public string Api { get; set; }
            public string RelativeUri { get; set; }
            public string Source { get; set; }
            public string Type { get; set; }
            public bool Internal { get; set; }

            public IDictionary<string, string> Headers { get; set; }
            public IDictionary<string, string> QueryString { get; set; }
            public string BodyInBase64 { get; set; }
        }

        public bool? UseFailOver { get; set; }

        private string GetTypeDesc( HttpType type )
        {
            switch ( type )
            {
                case HttpType.GET:
                    return "GET";
                case HttpType.POST:
                    return "POST";
                case HttpType.PUT:
                    return "PUT";
                case HttpType.PATCH:
                    return "PATCH";
                case HttpType.DELETE:
                    return "DELETE";
                default:
                    throw new InvalidOperationException( );
            }
        }


        #region GET
        public IRestResponse Get( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null )
            => GetPerform( p_Api, p_Uri, false, p_AditionalInfo ).GetAwaiter( ).GetResult( );

        public async Task<IRestResponse> GetAsync( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null )
            => await GetPerform( p_Api, p_Uri, false, p_AditionalInfo );

        public IRestResponse GetInternal( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null )
            => GetPerform( p_Api, p_Uri, true, p_AditionalInfo ).GetAwaiter( ).GetResult( );

        public async Task<IRestResponse> GetInternalAsync( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null )
            => await GetPerform( p_Api, p_Uri, true, p_AditionalInfo );

        private async Task<IRestResponse> GetPerform( string p_Api, string p_Uri, bool p_Internal, RestRequest p_AditionalInfo = null )
            => await Perform( HttpType.GET, p_Api, p_Uri, null, p_Internal, p_AditionalInfo ?? new RestRequest( ) );
        #endregion

        #region POST
        public IRestResponse Post( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null )
            => PostPerform( p_Api, p_Uri, p_Object, false, p_AditionalInfo ).GetAwaiter( ).GetResult( );
        public async Task<IRestResponse> PostAsync( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null )
            => await PostPerform( p_Api, p_Uri, p_Object, false, p_AditionalInfo );
        public IRestResponse PostInternal( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null )
            => PostPerform( p_Api, p_Uri, p_Object, true, p_AditionalInfo ).GetAwaiter( ).GetResult( );
        public async Task<IRestResponse> PostInternalAsync( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null )
            => await PostPerform( p_Api, p_Uri, p_Object, true, p_AditionalInfo );
        private async Task<IRestResponse> PostPerform( string p_Api, string p_Uri, object p_Object, bool p_Internal, RestRequest p_AditionalInfo = null )
            => await Perform( HttpType.POST, p_Api, p_Uri, p_Object, p_Internal, p_AditionalInfo ?? new RestRequest( ) );
        #endregion

        private async Task<IRestResponse> Perform(
            HttpType p_Type,
            string p_Api,
            string p_Uri,
            object p_ContentObject,
            bool p_Internal,
            RestRequest p_Request )
        {
            if ( !string.IsNullOrEmpty( p_Api ) )
            {
                Uri uri = new Uri( p_Api).Append( p_Uri );

                string newUri;
                if ( p_Request.QueryString != null )
                {
                    newUri = uri.AddOrUpdateParameter( p_Request.QueryString ).ToString( );
                }
                else
                {
                    newUri = uri.AbsoluteUri;
                }

                return await PerformHelper(
                    p_Type,
                    newUri,
                    p_ContentObject,
                    p_Request,
                    p_Api,
                    p_Uri
                );
            }

            throw new Exception( "Invalid relative usage, empty API !" );
        }

        private async Task<IRestResponse> PerformHelper(
            HttpType p_Type,
            string p_AbsoluteUri,
            object p_ContentObject,
            RestRequest p_Request,
            string p_Api,
            string p_RelativeUri )
        {
            var client = new HttpClient( );

            HttpContent content = null;
            if ( p_ContentObject != null )
            {
                if ( p_ContentObject is HttpContent )
                {
                    content = p_ContentObject as HttpContent;
                }
                else
                {
                    content = new StringContent(
                        JsonConvert.SerializeObject( p_ContentObject ),
                        Encoding.UTF8,
                        "application/json"
                    );
                }
            }

            HttpResponseMessage response = null;
            switch ( p_Type )
            {
                case HttpType.GET:
                    response = await client.GetAsync( p_AbsoluteUri );
                    break;
                case HttpType.POST:
                    response = await client.PostAsync( p_AbsoluteUri, content );
                    break;
                case HttpType.PUT:
                    response = await client.PutAsync( p_AbsoluteUri, content );
                    break;
                case HttpType.PATCH:
                    response = await client.PatchAsync( p_AbsoluteUri, content );
                    break;
                case HttpType.DELETE:
                    response = await client.DeleteAsync( p_AbsoluteUri );
                    break;
            }

            string responseContent = await response.Content.ReadAsStringAsync( );

            bool inFailOver = false;
            if ( !p_Request.IsSuccess( response.StatusCode ) )
            {
                // ERROR
            }

            return new RestResponse(
                p_Api,
                p_RelativeUri,
                Enum.GetName( typeof( HttpType ), p_Type ),
                p_Request,
                response,
                responseContent,
                inFailOver,
                p_Request.IsSuccess,
                p_ContentObject
            );
        }
    }
}
