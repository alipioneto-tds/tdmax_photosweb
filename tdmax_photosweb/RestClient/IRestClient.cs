﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public interface IRestClient
    {
        bool? UseFailOver { get; set; }

        #region GET
        IRestResponse Get( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null );
        Task<IRestResponse> GetAsync( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null );
        IRestResponse GetInternal( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null );
        Task<IRestResponse> GetInternalAsync( string p_Api, string p_Uri, RestRequest p_AditionalInfo = null );
        #endregion

        #region POST
        IRestResponse Post( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null );
        Task<IRestResponse> PostAsync( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null );
        IRestResponse PostInternal( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null );
        Task<IRestResponse> PostInternalAsync( string p_Api, string p_Uri, object p_Object, RestRequest p_AditionalInfo = null );
        #endregion
    }
}
