﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public class RestRequest
    {
        public IDictionary<string, string> Headers { get; set; }
        public IDictionary<string, string> QueryString { get; set; }
        public Func<HttpStatusCode, bool> IsSuccess { get; set; } = ( x ) => x == HttpStatusCode.OK;
        public bool? UseFailOver { get; set; }
    }
}
