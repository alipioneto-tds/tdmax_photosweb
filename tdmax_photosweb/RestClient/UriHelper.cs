﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace tdmax_photosweb.RestClient
{
    public static class UriHelper
    {
        public static Uri AddOrUpdateParameter( this Uri url, IDictionary<string, string> values )
        {
            var uriBuilder = new UriBuilder( url );
            var query = HttpUtility.ParseQueryString( uriBuilder.Query );

            foreach ( var v in values )
            {
                if ( query.AllKeys.Contains( v.Key ) )
                {
                    query[ v.Key ] = v.Value;
                }
                else
                {
                    query.Add( v.Key, v.Value );
                }
            }

            uriBuilder.Query = query.ToString( );
            return uriBuilder.Uri;
        }

        /// <summary>
        /// Append path to existing uri
        /// </summary>
        public static Uri Append( this Uri uri, params string[ ] paths )
        {
            return new Uri(
                paths.Aggregate(
                    uri.AbsoluteUri,
                    ( current, path ) => string.Format( "{0}/{1}", current.TrimEnd( '/' ), path.TrimStart( '/' ) )
                )
            );
        }
    }
}
