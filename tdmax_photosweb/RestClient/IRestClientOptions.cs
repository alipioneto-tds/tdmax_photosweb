﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public interface IRestClientOptions
    {
        string ApiKey { get; set; }
        string AccessToken { get; set; }
        void AddCustomApiMap( string p_Api, string p_Endpoint );
        string FindCustomApiMap( string p_Api );
    }
}
