﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public interface IUrlApiResolve
    {
        Uri Resolve( string api, string uri, bool isInternal );
    }
}
