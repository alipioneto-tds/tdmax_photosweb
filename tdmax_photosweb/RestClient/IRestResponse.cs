﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public interface IRestResponse
    {
        string Content { get; }
        bool FailOver { get; }
        HttpResponseMessage RawResponse { get; }
        bool Success { get; }

        void EnsureSuccess( );
        T GetAs<T>( );
        T GetAsOrException<T>( );
        //IEnumerable<AtlasRestError> GetError( );
    }
}
