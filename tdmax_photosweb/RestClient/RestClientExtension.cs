﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public static class RestClientExtension
    {
        public static IServiceCollection AddRestClient( this IServiceCollection services, 
            Action<IRestClientOptions, HttpContext> action = null )
        {
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IRestClientOptions, RestClientOptions>( k => {
                var contextAccessor = k.GetService( typeof( IHttpContextAccessor ) ) as IHttpContextAccessor;

                RestClientOptions options = new RestClientOptions( );
                action?.Invoke( options, contextAccessor?.HttpContext );
                return options;
            } );
            services.AddTransient<IUrlApiResolve, UrlApiResolve>( );
            services.AddTransient<IRestClient, RestClient>( );
            //services.AddTransient<IRestClientFactory, RestClientFactory>( );
            return services;
        }
    }
}
