﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public class UrlApiResolve : IUrlApiResolve
    {
        public Uri Resolve( string api, string uri, bool isInternal )
        {
            string a = @"localhost:55754/";

            Uri uriResolved = new Uri( a ).Append(
                isInternal ? string.Format( $"{api}.internal" ) : api,
                uri
            );

            return uriResolved;
        }
    }
}
