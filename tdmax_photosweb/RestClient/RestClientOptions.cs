﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.RestClient
{
    public class RestClientOptions : IRestClientOptions
    {
        private Dictionary<string, string> m_CustomApiEndpoints;

        public string ApiKey { get; set; } = null;
        public string AccessToken { get; set; } = null;

        /// <summary>
        /// Customize api endpoint routing
        /// </summary>
        public void AddCustomApiMap( string p_Api, string p_Endpoint )
        {
            if ( null == m_CustomApiEndpoints )
            {
                m_CustomApiEndpoints = new Dictionary<string, string>( );
            }

            m_CustomApiEndpoints.Add( p_Api.ToLower( ).Trim( ), p_Endpoint );
        }

        /// <summary>
        /// Check for custom api endpoint routing
        /// </summary>
        public string FindCustomApiMap( string p_Api )
        {
            string ret = null;
            if ( null != m_CustomApiEndpoints )
            {
                string apiName = p_Api.Replace( "/", string.Empty ).ToLower( ).Trim( );
                m_CustomApiEndpoints.TryGetValue( apiName, out ret );
            }
            return ret;
        }
    }
}
