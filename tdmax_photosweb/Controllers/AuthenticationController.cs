﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using tdmax_photosweb.Classes;
using tdmax_photosweb.Classes.Authentication;
using tdmax_photosweb.Entities;
using tdmax_photosweb.Services;

namespace tdmax_photosweb.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IUserService m_UserService;
        private readonly AppSettings _mySettings;

        public AuthenticationController( IOptions<AppSettings> settings, IUserService p_User )
        {
            _mySettings = settings.Value;
            m_UserService = p_User;
        }

        [HttpGet]
        public EmptyResult SetKey( string key )
        {
            AuthenticationToken token = new AuthenticationToken( _mySettings ) { Key = key };
            Response.Cookies.Append( token.GetSessionIdToken( ), token.GetTokenValue(), token.GetCookieOptions( ) );

            return new EmptyResult( );
        }

        public class SetAuthenticationModel
        {
            public string criptoStr;
        }

        [HttpPost]
        public async Task<JsonResult> SetAuthentication( [FromBody]SetAuthenticationModel model )
        {
            string erro = "";
            try
            {
                AuthenticationInfo auth = new AuthenticationInfo( HttpContext );
                if ( !string.IsNullOrEmpty( auth.Token.Key ) )
                {
                    string plaintext = Utils.DecryptJs( model.criptoStr, auth );
                    string[ ] pairs = plaintext.Split( new char[ ] { '|' } );

                    string user = getValue( "user", pairs );
                    string passwd = getValue( "passwd", pairs );

                    Regex regex = new Regex( "^\\d+$" );

                    UserEntity userEntity = ( regex.IsMatch( user ) ) ?
                            ( await m_UserService.FindById( int.Parse( user ) ) ) :
                            ( await m_UserService.FindByLogin( user ) );

                    if ( userEntity.Login != null )
                    {
                        if ( userEntity.IsActive )
                        {
                            //if ( usuario.GrupoOperadoraID > 0 || usuario.PerfilID == ( byte ) ePerfilWebFotos.AdminTdMax )
                            if (true)
                            {
                                if ( userEntity.Password.ToUpper() == Utils.CriptySenha( userEntity.Login, passwd).ToUpper())
                                {
                                    auth.Token.Id = userEntity.UserId;
                                    auth.Token.Cid = userEntity.RegisterId;
                                    auth.Token.GrupoOperadoraId = 1;// userEntity.GrupoOperadoraID;

                                    Response.Cookies.Append( auth.Token.GetSessionIdToken( ), 
                                        auth.Token.GetTokenValue( ), auth.Token.GetCookieOptions( ) );

                                    return Json( new
                                    {
                                        Id = auth.Token.Id,
                                        Cid = auth.Token.Cid,
                                        Name = userEntity.Name,
                                        PerfilID = 1,// usuario.PerfilID,
                                        GrupoOperadoraID = 1,//usuario.GrupoOperadoraID,
                                        Ticket = auth.ToString( ),
                                        IsValid = auth.Token.IsValid,
                                        PerfilDescricao = "Admin TDMax",//usuario.PerfilDescricao
                                    } );
                                }
                                //else
                                //{
                                //    Controle c = new Controle( 2, 11, 1 );
                                //    string requestUrl = c.GetValor( ).ToString( );
                                //    usuario.AuthenticationError( requestUrl );
                                //}
                            }
                            else
                            {
                                erro = "A autenticação falhou! Usuário sem grupo." ;
                            }
                        }
                        else
                        {
                            erro = "O usuário encontra-se desativado";
                        }
                    }
                    else
                    {
                        erro = "A autenticação falhou! Login inválido.";
                    }
                }
            }
            catch ( Exception e )
            {
                erro = e.Message;
            }

            return Json( new
            {
                Id = 0,
                Name = "",
                Ticket = "",
                IsValid = false,
                Err = erro
            });
        }

        [HttpPost]
        public async Task<JsonResult> GetAuthentication( )
        {
            AuthenticationInfo auth = new AuthenticationInfo( HttpContext );
            if ( auth.Token == null )
            {
                auth.Token = new AuthenticationToken( );
            }

            UserEntity usr = ( auth.Token.Id == 0 ) ? new UserEntity( ) :
                             ( await m_UserService.FindById( auth.Token.Id ) );

            return Json( new
            {
                Id = auth.Token.Id,
                Cid = usr.RegisterId,
                Name = usr.Name,
                PerfilID = 1,//usr.PerfilID,
                GrupoOperadoraID = 1,//usr.GrupoOperadoraID,
                Ticket = auth.ToString( ),
                IsValid = auth.Token.IsValid,
                Token = ( auth.Token.Id > 0 ) ? auth.Token.ToString( ) : "",
                PerfilDescricao = "Admin TDMax",//usr.PerfilDescricao,
                Login = usr.Login
            } );
        }

        private string getValue( string value, string[ ] pairs )
        {
            foreach ( string pair in pairs )
            {
                string[ ] values = pair.Split( new char[ ] { '=' } );
                if ( values.Length == 2 && values[ 0 ] == value )
                {
                    return values[ 1 ];
                }
            }
            return "";
        }
    }
}