﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tdmax_photosweb.Controllers
{
    [Route( "[controller]" )]
    public class ServerController : Controller
    {
        [HttpGet]
        public object GetServerInfo( )
        {
            string uri = Request.GetDisplayUrl( );

            string path = "FotosPath",//ConfigurationManager.AppSettings[ "FotosPath" ],
                    server = "FotosServer";//ConfigurationManager.AppSettings[ "FotosServer" ];

            return new
            {
                appId = 1000,//ConfigurationManager.AppSettings[ "appId" ],
                appName = "TDMax WebFotos",//ConfigurationManager.AppSettings[ "appName" ],
                version = "20.10.0.0",//ConfigurationManager.AppSettings[ "version" ],
                login = "~/UserSecurity/Login",
                url = Request.Scheme + Request.Host,
                fotosCadastro = "http://localhost:55754/Users",//ConfigurationManager.AppSettings[ "FotosCadPath" ],
                fotosAcesso = "http://localhost:55754/Transactions",
                webApi = Request.Scheme + Request.Host + "/api/",
                lang = System.Globalization.CultureInfo.CurrentCulture.Name
            };
        }
    }
}
