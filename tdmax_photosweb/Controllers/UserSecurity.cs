﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdmax_photosweb.Classes.Authentication;

namespace tdmax_photosweb.Controllers
{
    public class UserSecurity : Controller
    {
        public ActionResult Logout( )
        {
            HttpContext.Session.Clear( );
            HttpContext.Response.Cookies.Delete( "ticket" );
            HttpContext.Response.Cookies.Delete( AuthenticationToken.SessionIdToken );

            return RedirectToAction( "Login", "UserSecurity" );
        }
    }
}
