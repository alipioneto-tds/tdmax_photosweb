﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tdmax_photosweb.Models;

namespace tdmax_photosweb.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index( )
        {
            if ( Request.Headers[ "Referer" ].Count == 0 )
            {
                return Redirect( "~/Account/Login" );
            }

            return View( );
        }

        public IActionResult Privacy( )
        {
            return View( );
        }

        [ResponseCache( Duration = 0, Location = ResponseCacheLocation.None, NoStore = true )]
        public IActionResult Error( )
        {
            return View( new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier } );
        }
    }
}
