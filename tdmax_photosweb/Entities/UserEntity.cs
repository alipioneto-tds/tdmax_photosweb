﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tdmax_photosweb.Entities
{
    public class UserEntity
    {
        public int RegisterId { get; set; }

        public int UserId { get; set; }

        public bool IsActive { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }

        public string Name { get; set; }
        public string Login { get; set; }
    }
}
